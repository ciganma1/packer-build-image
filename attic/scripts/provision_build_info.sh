#!/bin/env bash
set -x
set -v


BUILD_NAME=$1
TIMESTAMP=$2
PACKER_BUILD_NAME=$1

case ${BUILD_NAME} in
  centos7-vagrant|centos7-ami )
              IMAGE_NAME="nibr baseos nxc centos 7 x86_64 (nxc) ${TIMESTAMP}"
              ;;
  centos6-vagrant|centos6-ami )
              IMAGE_NAME="nibr baseos nxc centos 6 x86_64 (nxc) ${TIMESTAMP}"
              ;;
  centos6-informatics-ami )
              IMAGE_NAME="nibr stack nxc_informatics centos 6 x86_64 (nxc informatics) ${TIMESTAMP}"
              ;;
  centos7-informatics-ami )
              IMAGE_NAME="nibr stack nxc_informatics centos 7 x86_64 (nxc informatics) ${TIMESTAMP}"
              ;;
  rhel6-ami )
             IMAGE_NAME="nibr baseos nxc redhat 6 x86_64 (nxc) ${TIMESTAMP}"
              ;;
  rhel7-ami )
             IMAGE_NAME="nibr baseos nxc redhat 7 x86_64 (nxc) ${TIMESTAMP}"
             ;;
     *)
             IMAGE_NAME="nibr baseos nxc unknown x86_64 (nxc) ${TIMESTAMP}"
             ;;
esac



[[ -d /etc/acs/nxfacts ]] && mkdir -p /etc/acs/nxfacts

# generate a basic image  information
if [[  -f /etc/acs/nxfacts/build.json ]]; then
   pushd /etc/acs/nxfacts
   # merge existing build facts from other scripts to a new one
   cat build.json | jq -r --arg PACKER_BUILD_NAME "$PACKER_BUILD_NAME" --arg IMAGE_NAME "$IMAGE_NAME" --arg  TIMESTAMP "$TIMESTAMP" '. +  { "type": $PACKER_BUILD_NAME , "name": $IMAGE_NAME, "timestamp": $TIMESTAMP  }' | jq -s 'sort_by(.)[]'  > build.json.new
   # replace existing one with our merge
   [[ $? == 0 ]] && mv build.json.new build.json
   rm -f build.json.new
   popd
else
   # if no build.json exist generate a new one
cat > /etc/acs/nxfacts/build.json <<EOF
{
  "type": "${PACKER_BUILD_NAME}",
  "name":  "${IMAGE_NAME}",
  "timestamp": "${TIMESTAMP}"
}
EOF
fi
