# This kickstart file allows unattended installation of a complete,
# qualified Linux environment for CentOS 7.X 

# where the installation files are
url --url http://yum.global.nibr.novartis.net/iso/centos-7.5-x86_64

# The network settings are set here
%include /tmp/kick-network

# The root password can be common, as it's not used anyway and will be overwritten.
rootpw packer

install
text
#key --skip
reboot

# Locale and timezone
lang en_US.UTF-8
timezone UTC --ntpservers time1-chbs.nibr.novartis.net,time1-usca.nibr.novartis.net

# Keyboard Layout
keyboard us

# Don't install X11
skipx

# Security
auth --enableshadow --enablemd5
firewall --disabled
selinux --disabled

# Partitionning is done here
%include /tmp/kick-partition

# extra packages (needed for vmware-tools)
%packages
%include /tmp/kick-package-list
%end

%pre
#!/bin/bash

function log_error()
{
 echo "ERROR: $1" >> /tmp/kick-error
}


######## EXT4 - DISABLE 64bit (not supported by NetBackup)
sed -i '/.*features =.*/ s/,64bit//g' /etc/mke2fs.conf


######## NETWORK SETTINGS DETECTION
# we extract the network settings (current ip,netmask,gateway,dns)
if [ $(ip addr | grep -w inet| grep -cv '127.0.0.1') -ne 1 ]; then
  log_error "ONE interface (only) must have an IP."
  log_error "It's not completely reliable to determine the IP to use, so we fail"
  log_error "Test returns:"
  log_error "$(ip addr | grep -w inet| grep -cv '127.0.0.1')"
  
  exit 1
fi

interface=$(ip route | grep ^default | head -n 1 | awk '{print $5}')
if [ -z "$interface" ]; then
  log_error "Couldn't determine the network interface name"
  
  exit 1
fi

ip_addr=$(ip addr|grep -w 'inet'|grep -v '127.0.0.1'|awk '{print $2}')
ip=$(echo $ip_addr|cut -d/ -f1)
mask=$(ipcalc $ip_addr -m|cut -d= -f2)
gateway=$(ip route | grep ^default | head -n 1 | awk '{print $3}')
nameserver=$(cat /etc/resolv.conf|grep nameserver|head -n 1|awk '{print $2}')


######## HOSTNAME
system_name=$(hostname -f)
# we make sure we have a FQDN
system_name="`echo $system_name | awk '{print tolower($0)}'`"
short_name="$(echo "$system_name" | awk -F. '{print $1}')"

if [ "$system_name" == "$short_name" ]; then
  log_error "You need to give a FQDN. ${system_name} does not appear to be one."
  
fi

# we generate the hosts file
echo "127.0.0.1 localhost localhost.localdomain" > /tmp/hosts
echo "${ip} ${system_name} ${short_name}" >> /tmp/hosts


######## WRITES NETWORK SETTINGS TO KICKSTART FILE
echo "network --device $interface --bootproto static --noipv6 --ip $ip --netmask $mask --gateway $gateway --nameserver $nameserver --hostname $short_name" > /tmp/kick-network


######## AUTOMATIC PARTITIONING
is_vm=false
[ $(dmidecode | grep -ci vmware) -gt 0 ] && is_vm=true

if [ $is_vm == true ]; then
  # It's a VM: we must have 3 disks.
  # Swap goes on sdb (required for Vmware over NFS).
  # The partitions should be aligned to 1M boundaries for optimal
  # performance, that's why they're created manually with fdisk
  # instead of doing it via the usual kickstart commands.
  # The latter are only used to associate the partitions with
  # the right mount points.

  testsdc="$(fdisk -l /dev/sdc | grep -i 'units')"
  if [ -z "$testsdc" ]; then
    log_error "The machine is a VM. It must have three disks."
    log_error "Couldn't find /dev/sdc. Aborting."
    
    exit 1
  fi

  echo "clearpart --none --drives=sda,sdb,sdc" >> /tmp/kick-partition
  echo "bootloader --location=mbr --driveorder=sda,sdb,sdc" >> /tmp/kick-partition
  echo "part /boot --fstype ext4 --onpart=/dev/sda1" >> /tmp/kick-partition
  echo "part pv.root --onpart=/dev/sda2" >> /tmp/kick-partition
  echo "volgroup vg_root pv.root" >> /tmp/kick-partition
  echo "logvol / --fstype ext4 --name=lv_root --vgname=vg_root --size=10240" >> /tmp/kick-partition
  echo "logvol /var --fstype ext4 --name=lv_var --vgname=vg_root --size=3072" >> /tmp/kick-partition
  echo "logvol /tmp --fstype ext4 --name=lv_tmp --vgname=vg_root --size=2048" >> /tmp/kick-partition
  echo "logvol /admhome --fstype ext4 --name=lv_admhome --vgname=vg_root --size=1024" >> /tmp/kick-partition
  echo "part pv.swap --onpart=/dev/sdb1" >> /tmp/kick-partition
  echo "volgroup vg_swap pv.swap" >> /tmp/kick-partition
  echo "logvol swap --fstype swap --name=lv_swap --vgname=vg_swap --size=2048" >> /tmp/kick-partition
  echo "part pv.apps --onpart=/dev/sdc1" >> /tmp/kick-partition
  echo "volgroup vg_apps pv.apps" >> /tmp/kick-partition

  # we create partitions with fdisk to have them aligned
  # sda1 (512M) for /boot, sda2 (the rest of the disk) for LVM
  echo -e "n\np\n1\n\n+512M\na\n1\nn\np\n2\n\n\nt\n2\n8e\nw\n" | fdisk -c -u /dev/sda

  # we create sdb1 (the whole disk size) for vg_swap
  echo -e "n\np\n1\n\n\nt\n8e\nw\n" | fdisk -c -u /dev/sdb

  # we create sdc1 (the whole disk size) for vg_apps
  echo -e "n\np\n1\n\n\nt\n8e\nw\n" | fdisk -c -u /dev/sdc

else
  # It's a physical machine.
  # If acs-boot-disk parameter is available, it takes highest precedence
  acs_boot_disk="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-boot-disk' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
  if [ ! -z "$acs_boot_disk" ]; then
    disk="$acs_boot_disk"
  else
    # We use the first disk ONLY (/dev/cciss/c0d0 or /dev/sda)
    # /boot on first partition (512M). LVM on the rest of the disk
    # swap is also on LVM
    testdisk="$(fdisk -l /dev/cciss/c0d0 | grep -i 'units')"
    if [ ! -z "$testdisk" ]; then # /dev/cciss/c0d0 is present
      disk="cciss/c0d0"
    else
      # if booted from a USB key, sda is the key. We check for that
      if [ $(blkid | grep '/dev/sda' | grep -ci 'label=\"cobbler\"') -ne 0 ]; then
        # /dev/sda is a bootable cobbler usb key. We make sure /dev/sdb is present
        testdisk="$(fdisk -l /dev/sdb | grep -i 'units')"
        if [ ! -z "$testdisk" ]; then # /dev/sdb is present
          disk="sdb"
        else
          log_error "ERROR: /dev/sda is the USB key, and /dev/sdb is NOT present."
          
          exit 1
        fi
      else
        # /dev/sda is our target disk. It must be present
        testdisk="$(fdisk -l /dev/sda | grep -i 'units')"
        if [ ! -z "$testdisk" ]; then # /dev/sda is present
          disk="sda"
        else
          # neither /dev/cciss/c0d0 nor /dev/sda are present so we fail.
          log_error "ERROR: neither /dev/cciss/c0d0 nor /dev/sda are present."
          log_error "ERROR: The 'acs-boot-disk' boot option has not been set either, so we fail"
          
          exit 1
        fi
      fi
    fi
  fi

  # we partition the first disk only. Any other disk remains untouched
  echo "zerombr" >> /tmp/kick-partition
  echo "ignoredisk --only-use=$disk" >> /tmp/kick-partition
  echo "clearpart --all --drives=$disk --initlabel" >> /tmp/kick-partition
  echo "bootloader --location=mbr --driveorder=$disk" >> /tmp/kick-partition
  echo "part /boot --fstype ext4 --size=512 --asprimary --ondisk=$disk" >> /tmp/kick-partition
  echo "part pv.root --size=1 --grow --ondisk=$disk" >> /tmp/kick-partition
  echo "volgroup vg_root pv.root" >> /tmp/kick-partition
  echo "logvol /        --fstype ext4 --name=lv_root    --vgname=vg_root --size=10240" >> /tmp/kick-partition
  echo "logvol /var     --fstype ext4 --name=lv_var     --vgname=vg_root --size=3072" >> /tmp/kick-partition
  echo "logvol /tmp     --fstype ext4 --name=lv_tmp     --vgname=vg_root --size=2048" >> /tmp/kick-partition
  echo "logvol /admhome --fstype ext4 --name=lv_admhome --vgname=vg_root --size=1024" >> /tmp/kick-partition
  echo "logvol swap     --fstype swap --name=lv_swap    --vgname=vg_root --size=2048" >> /tmp/kick-partition
fi


######## VMWARE TOOLS
is_vm=false
[ $(dmidecode | grep -ci vmware) -gt 0 ] && is_vm=true

if [ $is_vm == true ]; then
cat > /tmp/kick-package-list << EOF
perl
net-tools
open-vm-tools
EOF
else
  touch /tmp/kick-package-list
fi

%end


########################################################################
# %post-script
########################################################################
%post --nochroot
# we put the hosts file in place
mv -f /tmp/hosts /mnt/sysimage/etc/hosts
# we keep the modified mke2fs.conf file
mv -f /etc/mke2fs.conf /mnt/sysimage/etc/

%end

%post
exec >& /root/acs-kick-post.log

function acs_log { echo ; echo "###### ACS-LOG: $(echo $1 | tr 'a-z' 'A-Z')"; }

is_vm=false
[ $(dmidecode | grep -ci vmware) -gt 0 ] && is_vm=true

#################################
# UID/GID CHANGE
#################################
# Rationale: the default uid/gid range has changed in rhel7. We keep the old ranges
acs_log 'UID/GID CHANGE'

NEW_UID_MIN=500
NEW_GID_MIN=500
NEW_SYS_UID_MAX=499
NEW_SYS_GID_MAX=499

# Some system users and groups are created during the installation, when rpms are installed.
# Those users/groups have the wrong uids/gids. We change them.
CUR_UID=$NEW_SYS_UID_MAX
CUR_GID=$NEW_SYS_GID_MAX

for LINE in $(cat /etc/group|awk -v NEW_SYS_GID_MAX=$NEW_SYS_GID_MAX -F: 'BEGIN { OFS = ":" } ($3>NEW_SYS_GID_MAX) { print $1,$3 }'); do
  GROUPNAME=`echo $LINE|cut -d: -f1`
  OLDGID=`echo $LINE|cut -d: -f2`
  groupmod -g $CUR_GID $GROUPNAME
  find / -path /proc -prune -o -group $OLDGID -exec chown :$CUR_GID {} \;
  CUR_GID=$[CUR_GID-1]
done

for LINE in $(cat /etc/passwd|awk -v NEW_SYS_UID_MAX=$NEW_SYS_UID_MAX -F: 'BEGIN { OFS = ":" } ($3>NEW_SYS_UID_MAX) { print $1,$3 }'); do
  USERNAME=`echo $LINE|cut -d: -f1`
  OLDUID=`echo $LINE|cut -d: -f2`
  usermod -u $CUR_UID -g $USERNAME $USERNAME
  find / -path /proc -prune -o -user $OLDUID -exec chown $CUR_UID {} \;
  CUR_UID=$[CUR_UID-1]
done

### /etc/login.defs needs to be changed, for the old ranges to apply for future users/groups
cat > /etc/login.defs << EOF
MAIL_DIR	/var/spool/mail
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_MIN_LEN	5
PASS_WARN_AGE	7
UID_MIN         $NEW_UID_MIN
UID_MAX         60000
SYS_UID_MIN     201
SYS_UID_MAX     $NEW_SYS_UID_MAX
GID_MIN         $NEW_GID_MIN
GID_MAX         60000
SYS_GID_MIN     201
SYS_GID_MAX     $NEW_SYS_GID_MAX
CREATE_HOME	yes
UMASK           077
USERGROUPS_ENAB yes
ENCRYPT_METHOD MD5
MD5_CRYPT_ENAB yes
EOF


#################################
# SSH KEY
#################################
# Put the ssh key used for post deployment script
acs_log 'ssh key'
mkdir -p /root/.ssh
cat > /root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2jnP1juuEebI3sixbphvVrsr4NHpG7yE8xkkuv4+yqlsGqvan1fsaVmlAFEvBvdE9dPKYbxGpt+bbvNgdconXPjSYwQcP5vYSd5OpJH17etQBLlAKcHIKrdTKpbX3/jTWjnEyzuP7kaT4W0Tn7n+e532qa6Ynk0cLDj9Bx1Vt55TLNgWpHHgrVGg7LPqDjNMVli09f3cv92d5dKAExYgvysAIwsGucqzNOCG10mMmFgDuE+l7B3FwMPVpdpFKOapCx6nW1kDibkbFz39EY1JL5Nek+OR7r3TCc4hcmliE6YI7YmJWvKnnIYQwtZfYtKEsumryjUI1kxvpL1GccAF6Q== post@install
EOF
chmod -R o-rwx /root/.ssh


#################################
# Services
#################################
# we disable all the services
for serv in $(systemctl list-unit-files -t service|grep enabled|awk '{print $1}');do acs_log "disable service $serv (systemctl)"; systemctl disable $serv; done
for serv in $(/sbin/chkconfig --list 2>/dev/null| grep '3:on'| awk '{print $1}'); do acs_log "disable service $serv (sysV)"; /sbin/chkconfig $serv off 2>/dev/null; done

# and activate only the ones we want
ENABLED_SERVICES="dbus-org.freedesktop.NetworkManager dbus-org.freedesktop.nm-dispatcher getty@ irqbalance lvm2-monitor microcode NetworkManager-dispatcher NetworkManager rsyslog sshd systemd-readahead-collect systemd-readahead-drop systemd-readahead-replay"
if [ $is_vm == true ]; then ENABLED_SERVICES="${ENABLED_SERVICES} vmtoolsd"; fi

for serv in $ENABLED_SERVICES ;do acs_log "enable service $serv (systemctl)"; systemctl enable ${serv}.service; done

#################################
# AUTO-PUPPETIZATION SCRIPTS
#################################

# take care of acs-patching-tag boot flag
acs_patching_tag="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-patching-tag' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
echo Value of parameter acs-patching-tag is: $acs_patching_tag
if [ $acs_patching_tag ] ; then
  tag=$acs_patching_tag
else
  tag=dev
fi

# We push the definition of additional repositories
cat >> /etc/yum.conf << 'EOF'
[acs-local]
name = 'ACS local repository'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/acs-local
proxy = _none_
gpgcheck = 0
enabled = 1

[centos-os]
name = 'CentOS os (v. 7 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/centos-os
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
proxy = _none_
enabled = 1

[centos-updates]
name = 'CentOS updates (v. 7 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/centos-updates
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
proxy = _none_
enabled = 1

[epel]
name = 'Extra Packages for Enterprise Linux (v. 7 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/epel
gpgcheck = 0
proxy = _none_
enabled = 1

[puppetlabs-dependencies]
name = 'Puppet Labs Dependencies (v. 7 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/puppetlabs-dependencies
gpgcheck = 0
proxy = _none_
enabled = 1

[puppetlabs-products]
name = 'Puppet Labs Products (v. 7 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/puppetlabs-products
gpgcheck = 0
proxy = _none_
enabled = 1

[rsyslog-8]
name = 'ACS adiscon rsyslog v8 mirror'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/7Server-$basearch/rsyslog-8
gpgcheck = 0
proxy = _none_
enabled = 1
EOF

# Replace replace_with_tag with actual tag
sed -i "s|replace_with_tag|$tag|g" /etc/yum.conf

# we install few packages
rm -f /etc/yum.repos.d/*
yum clean all
yum -y install acs-puppetize-me acs-network-tools

# we update all packages before reboot
acs_patching_type="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-patching-type' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
echo Value of parameter acs-patching-type is: $acs_patching_type
if [ "$acs_patching_type" == "security" ] ; then
  yum update --security -y
  yum update rsyslog -y
elif [ "$acs_patching_type" == "none" ] ; then
  echo Not undergoing patching because acs_patching_type is set to none
else
  yum update -y
fi

#################################
# Build version number
#################################
# We store the build version number and time
mkdir -p /etc/acs/nxfacts/
cat > /etc/acs/nxfacts/build.json << EOF
{
  "cmdline": "`cat /proc/cmdline`",
  "method": "kickstart",
  "target": "server",
  "time": `date +%s`,
  "version": 4
}
EOF

chkconfig acs-puppetize-me-init off
%end
