#!/bin/bash --
# This is to work around Packer's issues with reboots during a build; see https://github.com/hashicorp/packer/issues/4684#issuecomment-331182735
# If they ever add a reboot provisioner, we should probably switch to that instead

echo "====> Shutting down the SSHD service and rebooting..."
service sshd stop
nohup shutdown -r now < /dev/null > /dev/null 2>&1 &
exit 0
