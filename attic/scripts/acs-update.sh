#!/bin/bash

/usr/local/libexec/acs/acs-nxfacts-refresh-local

tag=`/usr/local/bin/acs-nxfacts get cloud tags | jq .PatchAtBoot |tr -d '"'`

if [ $tag = 'full' ]; then
   /usr/local/bin/acs-update-me -t full
elif [ $tag = 'security' ]; then
   /usr/local/bin/acs-update-me -t security
elif [ $tag = 'false' ] || [ -z "$tag" ]; then
   echo "Not updating"
else
   /usr/local/bin/acs-update-me
fi

