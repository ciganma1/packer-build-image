#!/usr/bin/env bash
set -x -v
BUILD=$1
LOG=$2

PACKER_LOG=1 /usr/local/bin/packer build  -color=false -only=${BUILD}  -force <(jq -n -f json/nx-builds-commented-mnc-proto1.json) > /tmp/${LOG}
