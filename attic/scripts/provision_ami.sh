#!/bin/bash --


### Add customizations specific to AWS builds


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Install cloud formation helpers
# Troubleshooting - exclude some builds
#if [[ ! $PACKER_BUILD_NAME =~ (centos70-|rhel71-) ]]; then
# Set proxy and install dependencies
export http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011
export https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011
cd /usr/bin
echo "Installing pre-reqs for CloudFormation"
pip install --upgrade pip==9.0.3
pip install pystache
pip install argparse
#pip install python-daemon
pip install requests
# Pull the latest sources and build it, since there aren't working RPMs for all OSes
cd /opt
echo "Downloading CloudFormation source"
curl -O https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz
tar -xvpf aws-cfn-bootstrap-latest.tar.gz
cd aws-cfn-bootstrap-1.4/
echo "Building CloudFormation from source"
python setup.py build
python setup.py install
echo "Linking CloudFormation scripts"
ln -s /usr/init/redhat/cfn-hup /etc/init.d/cfn-hup
chmod 775 /usr/init/redhat/cfn-hup
cd /opt
mkdir aws
cd aws
mkdir bin
ln -s /usr/bin/cfn-hup /opt/aws/bin/cfn-hup
ln -s /usr/bin/cfn-init /opt/aws/bin/cfn-init
ln -s /usr/bin/cfn-signal /opt/aws/bin/cfn-signal
ln -s /usr/bin/cfn-elect-cmd-leader /opt/aws/bin/cfn-elect-cmd-leader
ln -s /usr/bin/cfn-get-metadata /opt/aws/bin/cfn-get-metadata
ln -s /usr/bin/cfn-send-cmd-event /opt/aws/bin/cfn-send-cmd-event
ln -s /usr/bin/cfn-send-cmd-result /opt/aws/bin/cfn-send-cmd-result
#fi


## Install awscli
echo "Installing AWS CLI"
pip install awscli
echo "Enabling AWS CLI tab-completion for bash"
cat > /etc/profile.d/aws_completer.sh << EOF
complete -C '/usr/bin/aws_completer' aws
EOF


## Create ec2-user and grant sudo
mkdir -p /home.ec2
echo "Creating ec2-user and adding to sudoers"
useradd --home /home.ec2/ec2-user ec2-user
echo 'ec2-user ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers


## Create /var/log/syslog to avoid errors when operating as root
# i.e. tail: cannot open `/var/log/syslog' for reading: No such file or directory
# Need to look into the actual cause of this later; might be AWS monitoring, or possibly something changed by cloud-init
touch /var/log/syslog


## Install dracut-config-generic, which configures it to build generic initramfs images instead of host-only
# This is needed for support of C5/M5 instance types on CentOS 7 (it's already the default behavior on CentOS 6)
# Existing images must also be recreated, but that will be done in the dracut-modules-growroot section anyway
major_release=$(cat /etc/system-release | sed -r 's/(Red Hat Enterprise Linux Server|CentOS Linux|CentOS) release ([67]).*/\2/')
if [[ $major_release -eq 7 ]]; then
  echo "Installing dracut-config-generic to build generic initramfs images"
  yum -y install dracut-config-generic
fi


## Increase NVMe timeout to max
# As recommended in http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/nvme-ebs-volumes.html#timeout-nvme-ebs-volumes and https://aws.amazon.com/ec2/faqs/#Amazon_Elastic_Block_Store_(EBS)
# 255 is max on current CentOS 6/7 kernels, as can be seen with `modinfo nvme` on 6 or `modinfo nvme_core on 7; io_timeout's type is byte
major_release=$(cat /etc/system-release | sed -r 's/(Red Hat Enterprise Linux Server|CentOS Linux|CentOS) release ([67]).*/\2/')
if [[ $major_release -eq 6 ]]; then
  echo "Increasing NVMe timeout to 255"
  grubby --update-kernel=ALL --args="nvme.io_timeout=255"
fi
if [[ $major_release -eq 7 ]]; then
  echo "Increasing NVMe timeout to 255"
  grubby --update-kernel=ALL --args="nvme_core.io_timeout=255"
fi


## Install dracut-modules-growroot, which along with cloud-init provides for auto-resizing of root partition
# RHEL/CentOS 6 only; 7 already has this functionality
major_release=$(cat /etc/system-release | sed -r 's/(Red Hat Enterprise Linux Server|CentOS Linux|CentOS) release ([67]).*/\2/')
if [[ $major_release -le 6 ]]; then
  echo "Installing dracut-modules-growroot kernel module"
  yum -y install dracut-modules-growroot

  # the script provided by the RPM does not work for / on LVM. We also need to trigger a reboot when we change the disk partitions. We push custom a custom script
  cat > /usr/share/dracut/modules.d/50growroot/growroot.sh << 'EOF'
#!/bin/sh

# Environment variables that this script relies upon:
# - root

_info() {
  echo "growroot: $*"
}

_warning() {
  echo "growroot Warning: $*" >&2
}

# This will drop us into an emergency shell
_fatal() {
  echo "growroot Fatal: $*" >&2
  exit 1
}

_growroot() {
  # Compute root-device
  if [ -z "${root##*mapper*}" ]
  then
    set -- "${root##*mapper/}"
    VOLGRP=${1%-*}
    ROOTVOL=${1#*-}
    rootdev=$(readlink -f $(lvm pvs --noheadings | awk '/'${VOLGRP}'/{print $1}'))
    _info "'/' is hosted on an LVM2 volume: setting \$rootdev to ${rootdev}"
  else
    # Remove 'block:' prefix and find the root device
    rootdev=$(readlink -f "${root#block:}")
  fi

  # root arg was nulled at some point...
  if [ -z "${rootdev}" ] ; then
    _warning "unable to find root device"
    return
  fi

  # If the basename of the root device (ie 'xvda1', 'sda1', 'vda') exists
  # in /sys/block/ then it is a block device, not a partition
  if [ -e "/sys/block/${rootdev##*/}" ] ; then
    _info "${rootdev} is not a partition"
    return
  fi

  # Check if the root device is a partition (name ends with a digit)
  if [ "${rootdev%[0-9]}" = "${rootdev}" ] ; then
    _warning "${rootdev} is not a partition"
    return
  fi

  # Remove all numbers from the end of rootdev to get the rootdisk and
  # partition number
  rootdisk=${rootdev}
  while [ "${rootdisk%[0-9]}" != "${rootdisk}" ] ; do
    rootdisk=${rootdisk%[0-9]}
  done
  partnum=${rootdev#${rootdisk}}

  # Check if we need to strip a trailing 'p' from the rootdisk name (for
  # device names like /dev/mmcblk0p)
  tmp=${rootdisk%[0-9]p}
  if [ "${#tmp}" != "${#rootdisk}" ] ; then
    rootdisk=${rootdisk%p}
  fi

  # Do a growpart dry run and exit if it fails or doesn't have anything
  # to do
  if ! out=$(growpart --dry-run "${rootdisk}" "${partnum}") ; then
    _info "${out}"
    return
  fi

  # Wait for any of the initial udev events to finish otherwise growpart
  # might fail
  udevadm settle --timeout=30

  # Resize the root partition
  if out=$(growpart --update off "${rootdisk}" "${partnum}" 2>&1) ; then
    _info "${out}"
  else
    _warning "${out}"
    _warning "growpart failed"
  fi

  # Wait for the partition re-read events to complete so that the root
  # partition is available for mounting
  udevadm settle --timeout=30

  # Add the root partition if it didn't come back on its own
  if ! [ -e "${rootdev}" ] ; then
    partx --add --nr "${partnum}" "${rootdisk}" || \
      _warning "failed to add root device ${rootdev}"
    udevadm settle --timeout=30
  fi

  _info "we need to trigger a reboot, to see the change"
  echo b >/proc/sysrq-trigger
}

_growroot

# vi: ts=4 noexpandtab
EOF

  # Add it to initramfs images, so it can do its thing at boot
  echo "Adding dracut-modules-growroot kernel module to initramfs images"
  rpm -qa kernel | sed 's/^kernel-//'  | xargs -I {} dracut -f /boot/initramfs-{}.img {}
fi


## Add information to motd
echo "Adding info to motd"
#AMI ID: 
#AMI date: 
#Major changes: 
#Full release notes in /etc/nx-core_release_notes
#
cat >> /etc/motd << 'EOF'
This AMI should meet the pre-CIS security baseline, similar to what is
implemented for on-premise servers. However it is still recommended to
apply Chef nx_core to ensure you have the most up-to-date security
settings. For more information, see
http://web.global.nibr.novartis.net/apps/confluence/display/CD/AMI+Creation+and+Deployment

EOF


## Add service to populate information in motd
# echo "Adding service to populate motd at boot"


## Create Nexpose user, set up login access and sudo
nexpose_user=sys_r7lx1_aws
nexpose_uid=782
nexpose_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbOdHZTbRrhsa7nhXrk7pYtSkWz0Et3SjHqNNMyUBV3LiPvOkBeSOMVlUOSs7tmywYgaRXWWvsdvySn9v0sWP3HjFKduGrazbdny/udALojqMH6Vo9bRarJ2michfjtncPrmlDzIfgBg5CKV/w5+iWZqTC1ufDo6MChXOnqv+n6NHUEIc62ZDI6lHQD4ldVsaQ1lFzfTFfQ809/PUBgbCmtrTOtnW7bqb9DY04uEeVt7gUK9xnVjtru6n1vyO/2PQ/flBrzyMTM/4DT53pKtd8xL6oh6SK+HhTN+hpnradBg9DOoVpHHf54X8RIHbzqYTLBOtlo7jfma8CUis/u3J1 ${nexpose_user}"
nexpose_home="/var/lib/${nexpose_user}"

# Create user
useradd --uid ${nexpose_uid} --user-group --home-dir "${nexpose_home}" --create-home ${nexpose_user}

# Set up SSH login via key
mkdir --parents --verbose --mode=0700 "${nexpose_home}/.ssh"
echo "${nexpose_key}" >> "${nexpose_home}/.ssh/authorized_keys"
chmod 600 "${nexpose_home}/.ssh/authorized_keys"
chown --recursive ${nexpose_user}:${nexpose_user} "${nexpose_home}/.ssh"

# Set up sudo access
echo "${nexpose_user} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers



## Create file for Chef to populate with instance metadata
mkdir -p /etc/chef/ohai/hints
echo "Creating /etc/chef/ohai/hints/ec2.json"
touch /etc/chef/ohai/hints/ec2.json


## Build facts
# We store some basic facts about the build
mkdir -p /etc/acs/nxfacts
echo "Writing build facts"
cat > /etc/acs/nxfacts/build.json << EOF
{
  "cmdline": "$(cat /proc/cmdline)",
  "method": "packer",
  "target": "ami",
  "time": $(date +%s),
  "version": 3
}
EOF
