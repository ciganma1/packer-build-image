#!/bin/bash --


### Add cloud-init on AWS builds (and others?)


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Install cloud-init
echo "Installing cloud-init"
yum -y install cloud-init
# Configure it
#echo "Configuring cloud-init"
#sed -E -i /etc/cloud/cloud.cfg -e 's/^([[:space:]]*name: ).*/\1ec2-user/' -e '1 i cloud_type: auto'

## Fix cloud-init output logging
# This should be in Packer 1.2.0, but there's no timeline for when that will be available
echo "Fixing cloud-init output logging"
if ! /bin/grep -q 'output: {' /etc/cloud/cloud.cfg.d/05_logging.cfg; then echo 'output: { all: "| tee -a /var/log/cloud-init-output.log" }' >> /etc/cloud/cloud.cfg.d/05_logging.cfg; fi
