# This kickstart file allows unattended installation of a complete,
# qualified Linux environment for Redhat Enterprise Linux 6.X

# where the installation files are (os_release is defined in the Cobbler profile)
url --url http://yum.global.nibr.novartis.net/iso/rhel-server-6.9-x86_64

# The network settings are set here
%include /tmp/kick-network

# The root password can be common, as it's not used anyway and will be overwritten.
rootpw packer

# Login/pass to use to connect to the server while the installation is running
sshpw --username=root $1$1b5CjY/X$vAG9k1a74LSeboyJr54U01 --iscrypted

install
text
key --skip
reboot

# Locale and timezone
lang en_US.UTF-8
timezone UTC

# Keyboard Layout
keyboard us

# Don't install X11
skipx

# Security
auth --enableshadow --enablemd5
firewall --disabled
selinux --disabled

# Partitionning is done here
%include /tmp/kick-partition

%packages

%pre
#!/bin/bash

function log_error()
{
 echo "ERROR: $1" >> /tmp/kick-error
}


######## NETWORK SETTINGS DETECTION
# we extract the network settings (current ip,netmask,gateway,dns)
if [ $(ifconfig | grep 'inet addr'| grep -cv '127.0.0.1') -ne 1 ]; then
  log_error "ONE interface (only) must have an IP."
  log_error "It's not completely reliable to determine the IP to use, so we fail"
  log_error "Test returns:"
  log_error "$(ifconfig | grep 'inet addr'| grep -cv '127.0.0.1')"
  exit 1
fi

interface=$(route -n | grep -E '^0.0.0.0' | awk '{print $NF'})
if [ -z "$interface" ]; then
  log_error "Couldn't determine the network interface name"
  exit 1
fi

ip=$(ifconfig|grep 'inet addr'|grep -v '127.0.0.1'|awk -F'inet addr:' '{print $2}'|awk '{print $1}')
mask=$(ifconfig|grep 'inet addr'|grep -v '127.0.0.1'|awk -F'Mask:' '{print $2}'|awk '{print $1}')
gateway=$(route -n|grep -E '^0.0.0.0'|awk '{print $2}')
nameserver=$(cat /etc/resolv.conf|grep nameserver|head -n 1|awk '{print $2}')


######## HOSTNAME
system_name=$(hostname -f)
# we make sure we have a FQDN
system_name="`echo $system_name | awk '{print tolower($0)}'`"
short_name="$(echo "$system_name" | awk -F. '{print $1}')"

if [ "$system_name" == "$short_name" ]; then
  log_error "You need to give a FQDN. ${system_name} does not appear to be one."
fi

# we generate the hosts file
echo "127.0.0.1 localhost localhost.localdomain" > /tmp/hosts
echo "${ip} ${system_name} ${short_name}" >> /tmp/hosts


######## WRITES NETWORK SETTINGS TO KICKSTART FILE
echo "network --device $interface --bootproto static --noipv6 --ip $ip --netmask $mask --gateway $gateway --nameserver $nameserver --hostname $short_name" > /tmp/kick-network


######## AUTOMATIC PARTITIONING
is_vm=false
[ $(dmidecode | grep -ci vmware) -gt 0 ] && is_vm=true

if [ $is_vm == true ]; then
  # It's a VM: we must have 3 disks.
  # Swap goes on sdb (required for Vmware over NFS).
  # The partitions should be aligned to 1M boundaries for optimal
  # performance, that's why they're created manually with fdisk
  # instead of doing it via the usual kickstart commands.
  # The latter are only used to associate the partitions with
  # the right mount points.

  testsdc="$(fdisk -l /dev/sdc | grep -i 'units')"
  if [ -z "$testsdc" ]; then
    log_error "The machine is a VM. It must have three disks."
    log_error "Couldn't find /dev/sdc. Aborting."
    exit 1
  fi

  echo "clearpart --none --drives=sda,sdb,sdc" >> /tmp/kick-partition
  echo "bootloader --location=mbr --driveorder=sda,sdb,sdc" >> /tmp/kick-partition
  echo "part /boot  --onpart=/dev/sda1" >> /tmp/kick-partition
  echo "part pv.root --onpart=/dev/sda2" >> /tmp/kick-partition
  echo "volgroup vg_root pv.root" >> /tmp/kick-partition
  echo "logvol / --fstype ext4 --name=lv_root --vgname=vg_root --size=10240" >> /tmp/kick-partition
  echo "logvol /var --fstype ext4 --name=lv_var --vgname=vg_root --size=3072" >> /tmp/kick-partition
  echo "logvol /tmp --fstype ext4 --name=lv_tmp --vgname=vg_root --size=2048" >> /tmp/kick-partition
  echo "logvol /admhome --fstype ext4 --name=lv_admhome --vgname=vg_root --size=1024" >> /tmp/kick-partition
  echo "part pv.swap --onpart=/dev/sdb1" >> /tmp/kick-partition
  echo "volgroup vg_swap pv.swap" >> /tmp/kick-partition
  echo "logvol swap --fstype swap --name=lv_swap --vgname=vg_swap --size=2048" >> /tmp/kick-partition
  echo "part pv.apps --onpart=/dev/sdc1" >> /tmp/kick-partition
  echo "volgroup vg_apps pv.apps" >> /tmp/kick-partition

  # we create partitions with fdisk to have them aligned
  # sda1 (512M) for /boot, sda2 (the rest of the disk) for LVM
  echo -e "n\np\n1\n\n+512M\na\n1\nn\np\n2\n\n\nt\n2\n8e\nw\n" | fdisk -c -u /dev/sda

  # we create sdb1 (the whole disk size) for vg_swap
  echo -e "n\np\n1\n\n\nt\n8e\nw\n" | fdisk -c -u /dev/sdb

  # we create sdc1 (the whole disk size) for vg_apps
  echo -e "n\np\n1\n\n\nt\n8e\nw\n" | fdisk -c -u /dev/sdc

else
  # It's a physical machine.
  # If acs-boot-disk parameter is available, it takes highest precedence
  acs_boot_disk="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-boot-disk' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
  if [ ! -z "$acs_boot_disk" ]; then
    disk="$acs_boot_disk"
  else
    # We use the first disk ONLY (/dev/cciss/c0d0 or /dev/sda)
    # /boot on first partition (512M). LVM on the rest of the disk
    # swap is also on LVM
    testdisk="$(fdisk -l /dev/cciss/c0d0 | grep -i 'units')"
    if [ ! -z "$testdisk" ]; then # /dev/cciss/c0d0 is present
      disk="cciss/c0d0"
    else
      # if booted from a USB key, sda is the key. We check for that
      if [ $(blkid | grep '/dev/sda' | grep -ci 'label=\"cobbler\"') -ne 0 ]; then
        # /dev/sda is a bootable cobbler usb key. We make sure /dev/sdb is present
        testdisk="$(fdisk -l /dev/sdb | grep -i 'units')"
        if [ ! -z "$testdisk" ]; then # /dev/sdb is present
          disk="sdb"
        else
          log_error "ERROR: /dev/sda is the USB key, and /dev/sdb is NOT present."
          exit 1
        fi
      else
        # /dev/sda is our target disk. It must be present
        testdisk="$(fdisk -l /dev/sda | grep -i 'units')"
        if [ ! -z "$testdisk" ]; then # /dev/sda is present
          disk="sda"
        else
          # neither /dev/cciss/c0d0 nor /dev/sda are present so we fail.
          log_error "ERROR: neither /dev/cciss/c0d0 nor /dev/sda are present."
          log_error "ERROR: The 'acs-boot-disk' boot option has not been set either, so we fail"
          exit 1
        fi
      fi
    fi
  fi

  # we partition the first disk only. Any other disk remains untouched
  echo "zerombr" >> /tmp/kick-partition
  echo "ignoredisk --only-use=$disk" >> /tmp/kick-partition
  echo "clearpart --all --drives=$disk --initlabel" >> /tmp/kick-partition
  echo "bootloader --location=mbr --driveorder=$disk" >> /tmp/kick-partition
  echo "part /boot --fstype ext4 --size=512 --asprimary --ondisk=$disk" >> /tmp/kick-partition
  echo "part pv.root --size=1 --grow --ondisk=$disk" >> /tmp/kick-partition
  echo "volgroup vg_root pv.root" >> /tmp/kick-partition
  echo "logvol /        --fstype ext4 --name=lv_root    --vgname=vg_root --size=10240" >> /tmp/kick-partition
  echo "logvol /var     --fstype ext4 --name=lv_var     --vgname=vg_root --size=3072" >> /tmp/kick-partition
  echo "logvol /tmp     --fstype ext4 --name=lv_tmp     --vgname=vg_root --size=2048" >> /tmp/kick-partition
  echo "logvol /admhome --fstype ext4 --name=lv_admhome --vgname=vg_root --size=1024" >> /tmp/kick-partition
  echo "logvol swap     --fstype swap --name=lv_swap    --vgname=vg_root --size=2048" >> /tmp/kick-partition
fi

%end


########################################################################
# %post-script
########################################################################
%post --nochroot
# we put the hosts file in place
mv -f /tmp/hosts /mnt/sysimage/etc/hosts
%end

%post
exec >& /root/acs-kick-post.log

#################################
# SSH KEY
#################################
# Put the ssh key used for post deployment script
mkdir -p /root/.ssh
cat > /root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2jnP1juuEebI3sixbphvVrsr4NHpG7yE8xkkuv4+yqlsGqvan1fsaVmlAFEvBvdE9dPKYbxGpt+bbvNgdconXPjSYwQcP5vYSd5OpJH17etQBLlAKcHIKrdTKpbX3/jTWjnEyzuP7kaT4W0Tn7n+e532qa6Ynk0cLDj9Bx1Vt55TLNgWpHHgrVGg7LPqDjNMVli09f3cv92d5dKAExYgvysAIwsGucqzNOCG10mMmFgDuE+l7B3FwMPVpdpFKOapCx6nW1kDibkbFz39EY1JL5Nek+OR7r3TCc4hcmliE6YI7YmJWvKnnIYQwtZfYtKEsumryjUI1kxvpL1GccAF6Q== post@install
EOF
chmod -R o-rwx /root/.ssh

#################################
# Services
#################################
# we disable all the services
for serv in $(chkconfig --list|awk '{print $1}');do chkconfig $serv off; done

# and activate only the ones we want
ENABLED_SERVICES="haldaemon lvm2-monitor messagebus network sshd udev-post"
for serv in $ENABLED_SERVICES ;do chkconfig $serv on; done

#################################
# AUTO-PUPPETIZATION SCRIPTS
#################################

# take care of acs-patching-tag boot flag
acs_patching_tag="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-patching-tag' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
echo Value of parameter acs-patching-tag is: $acs_patching_tag
if [ $acs_patching_tag ] ; then
  tag=$acs_patching_tag
else
  tag=dev
fi

# We push the definition of additional repositories
cat >> /etc/yum.conf << 'EOF'

[acs-local]
name = 'ACS local repository'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/acs-local
proxy = _none_
gpgcheck = 0
enabled = 1

[epel]
name = 'Extra Packages for Enterprise Linux (v. 6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/epel
gpgcheck = 0
proxy = _none_
enabled = 1

[optional]
name = 'Red Hat Enterprise Linux Server Optional Software (v. 6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/optional
gpgcheck = 0
proxy = _none_
enabled = 1

[puppetlabs-products]
name = 'Puppet Labs Products (v. 6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/puppetlabs-products
gpgcheck = 0
proxy = _none_
enabled = 1

[puppetlabs-dependencies]
name = 'Puppet Labs Dependencies (v. 6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/puppetlabs-dependencies
gpgcheck = 0
proxy = _none_
enabled = 1

[rhn-tools]
name = 'Red Hat Network Tools for RHEL Server (v.6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/rhn-tools
gpgcheck = 0
proxy = _none_
enabled = 1

[rsyslog-8]
name = 'ACS adiscon rsyslog v8 mirror'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/rsyslog-8
gpgcheck = 0
proxy = _none_
enabled = 1

[updates]
name = 'Red Hat Enterprise Linux Server (v. 6 for $basearch)'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/updates
proxy = _none_
gpgcheck = 0
enabled = 1

[vmtools]
name = 'ACS vmware mirror'
baseurl = http://yum.global.nibr.novartis.net/yum/tags/replace_with_tag/6Server-$basearch/vmtools
gpgcheck = 0
proxy = _none_
enabled = 1
EOF

# Replace replace_with_tag with actual tag
sed -i "s|replace_with_tag|$tag|g" /etc/yum.conf

# we install few packages
rm -f /etc/yum.repos.d/*
yum clean all
yum -y install acs-puppetize-me acs-network-tools

#################################
# VMware Tools
#################################
is_vm=false
[ $(dmidecode | grep -ci vmware) -gt 0 ] && is_vm=true

if [ $is_vm == true ]; then
  yum -y install vmware-tools-esx-kmods vmware-tools-esx-nox
fi

# we update all packages before reboot
acs_patching_type="$(for param in $(cat /proc/cmdline); do if [ $(echo $param|awk -F= '{print $1}') == 'acs-patching-type' ]; then echo $param | awk -F= '{print $2}'; fi; done)"
echo Value of parameter acs-patching-type is: $acs_patching_type
if [ "$acs_patching_type" == "security" ] ; then
  yum update --security -y
  yum update rsyslog -y
elif [ "$acs_patching_type" == "none" ] ; then
  echo Not undergoing patching because acs_patching_type is set to none
else
  yum update -y
fi

#################################
# Build version number
#################################
# We store the build version number and time
mkdir -p /etc/acs/nxfacts/
cat > /etc/acs/nxfacts/build.json << EOF
{
  "cmdline": "`cat /proc/cmdline`",
  "method": "kickstart",
  "target": "server",
  "time": `date +%s`,
  "version": 4
}
EOF

chkconfig acs-puppetize-me-init off
%end
