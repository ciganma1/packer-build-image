#!/bin/bash --


### Configuration specific to SciComp builds


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail

lvextend -L+1G -r /dev/vg_root/lv_var

case "$PACKER_BUILD_NAME" in
centos6*)
  # not all packages declared as dependencies of scicomp-all are available on centos6
  # we compute the list of packages really available, and install only those
  current_version=$(yum list scicomp-all | grep ^scicomp-all | awk '{print $2}'|cut -d: -f2)
  curl http://yum.global.nibr.novartis.net/yum/tags/prod/6Server-x86_64/acs-local/scicomp-all-${current_version}.x86_64.rpm > /tmp/scicomp-all.rpm
  rpm -qRp /tmp/scicomp-all.rpm | grep -vw rpmlib |sed -e 's/(x86-64)/\.x86_64/g' -e 's/(x86-32)/\.i686/g' -e 's/[ ]*$//g' |sort > /tmp/packages-scicomp-all
  yum list all |awk '{print $1}' | sed 's/\.noarch//g' |sort > /tmp/packages-available
  comm -12 /tmp/packages-scicomp-all /tmp/packages-available > /tmp/packages-to-install
  echo "Installing all the packages we can, from scicomp-all. Going to take a long time"
  yum -y install $(cat /tmp/packages-to-install)
  ;;
rhel6*)
  echo "Installing the scicomp-all package. Going to take a long time"
  yum -y --enablerepo=* install scicomp-all
  ;;
*)
  echo 'NOT SUPPORTED. ABORT'
  exit 1
  ;;
esac

yum clean packages

## Reinstall awscli
# One of the informatics packages breaks it
echo "Reinstalling AWS CLI"
https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011/ pip install awscli pip==9.0.3


## Add informatics sudo rights
echo "Adding informatics sudo privileges"
cat >> /etc/sudoers << "EOF"

# Standard rules taken from srv_scicomp Puppet class
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/tomcat, NOPASSWD:/sbin/service tomcat *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/jboss, NOPASSWD:/sbin/service jboss *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/httpd, NOPASSWD:/sbin/service httpd *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/tomcat*, NOPASSWD:/sbin/service tomcat* *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/mw_*, NOPASSWD:/sbin/service mw_* *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/etc/init.d/weblogic, NOPASSWD:/sbin/service weblogic *
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - tomcat
%admins_nibr_mms_srv_gbl_a ALL=(tomcat) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - tomcat6
%admins_nibr_mms_srv_gbl_a ALL=(tomcat6) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - tomcat7
%admins_nibr_mms_srv_gbl_a ALL=(tomcat7) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - apache
%admins_nibr_mms_srv_gbl_a ALL=(apache) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - sys_mwapp1
%admins_nibr_mms_srv_gbl_a ALL=(sys_mwapp1) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - mms-chef
%admins_nibr_mms_srv_gbl_a ALL=(mms-chef) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - sys_dmuser
%admins_nibr_mms_srv_gbl_a ALL=(sys_dmuser) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - sys_mdr
%admins_nibr_mms_srv_gbl_a ALL=(sys_mdr) NOPASSWD:ALL
%admins_nibr_mms_srv_gbl_a ALL=(ALL) NOPASSWD:/bin/su - weblogic
%admins_nibr_mms_srv_gbl_a ALL=(weblogic) NOPASSWD:ALL
EOF


## Add informatics mounts
echo "Adding informatics mounts"
# Need to split this for USCA vs CHBS
mkdir -p /clscratch /cm/shared /da /db /dlab /labdata /usr/prog
cat >> /etc/fstab << "EOF"
isiusca01.na.novartis.net:/ifs/usca/clscratch /clscratch nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca05.na.novartis.net:/ifs/usca/usca-cmshare /cm/shared nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca09.nibr.novartis.net:/ifs/usca/da /da nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca06.nibr.novartis.net:/ifs/usca/db /db nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca01.na.novartis.net:/ifs/usca/labdata /dlab nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca01.na.novartis.net:/ifs/usca/labdata /labdata nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
isiusca05.na.novartis.net:/ifs/usca/prog2 /usr/prog nfs nodev,proto=tcp,timeo=600,vers=3,hard,intr 0 0
EOF
#mkdir -p /clscratch /cm/shared /da /db /dlab /usr/prog
#cat >> /etc/fstab << "EOF"
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/clscratch /clscratch nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/cmshare /cm/shared nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/da /da nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/db /db nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/dlab /dlab nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#chbsisi-c.storechbs01.eu.novartis.net:/ifs/chbs/prog2 /usr/prog nfs nodev,rw,proto=tcp,timeo=600,vers=3,hard,intr 0 0
#EOF

## Add Centrify?



## Build facts
# We store some basic facts about the build
#echo "Adding build facts"
#mkdir -p /etc/acs/nxfacts
#cat > /etc/acs/nxfacts/build.json << EOF
#{
#  "cmdline": "$(cat /proc/cmdline)",
#  "method": "packer",
#  "target": "ami",
#  "time": $(date +%s),
#  "version": 3
#}
#EOF


