#!/bin/bash --
### Script to reset Cybereason's configuration and force it to create a new unique ID

os_release_major="$(/usr/local/bin/acs-nxfacts get core os_release_major)"

# Stop the service, if it's running
echo 'Stopping Cybereason service'
case ${os_release_major} in
  6) initctl stop cybereason-sensor ;;
  7) systemctl stop cybereason-sensor.service ;;
  *) echo "ERROR: This script is intended for use only on CentOS/RHEL 6/7."; exit 1 ;;
esac

# Set it to active and remove any previous unique identifier
echo 'Fixing Cybereason configuration'
sed --in-place --expression 's/^[::space::]*state[::space::]*=[::space::]*INACTIVE[::space::]*$/state=ACTIVE_NORMAL/' --expression '/^server.channelName[::space::]*=/d' /opt/cybereason/sensor/etc/sensor.conf
# Delete any remaining files from a previous instance
rm --recursive --force /opt/cybereason/sensor/PublishData
rm --recursive --force /opt/cybereason/sensor/Logs

# Start the service back up, which will also generate a new unique ID
echo 'Starting Cybereason service'
case ${os_release_major} in
  6) initctl start cybereason-sensor ;;
  7) systemctl start cybereason-sensor.service ;;
esac

