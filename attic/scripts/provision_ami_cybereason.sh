#!/bin/bash --

## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail

# Install Cybereason
echo 'Installing Cybereason'
yum install --assumeyes --disablerepo="*" /root/cybereason-sensor-17.5.322.0-1.x86_64_nibr_nibr-4-t.cybereason.net_443_INACTIVE_rpm.rpm

# Remove installer
echo 'Removing Cybereason installer'
rm --force --verbose /root/cybereason-sensor-17.5.322.0-1.x86_64_nibr_nibr-4-t.cybereason.net_443_INACTIVE_rpm.rpm

# Link Cybereason reset script so it runs for each new instance
echo 'Linking Cybereason reset script to cloud-init per-instance directory'
mkdir --parents --verbose /var/lib/cloud/scripts/per-instance
ln --symbolic --verbose /usr/local/bin/acs-reset-cybereason.sh /var/lib/cloud/scripts/per-instance/acs-reset-cybereason.sh
