#!/bin/bash --


### Add kernel modules for enhanced networking


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail

## Install newer ixgbevf kernel module, required for enhanced networking on AWS
# This enables enhanced networking on C3, C4, D2, I2, R3, and M4 (excluding m4.16xlarge) instance types
# Refer to http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/enhanced-networking.html#supported_instances for more information

# Build from source
# Could create an RPM, but then we'd have to roll a new one anytime we wanted to upgrade this, and the extra minute or so of build time is insignificant anyway
# Downloaded from https://sourceforge.net/projects/e1000/files/ixgbevf%20stable/
export ixgbevf_version="4.3.5"

pushd /root
echo "Extracting ixgbevf version ${ixgbevf_version} kernel module source"
tar -xvf "ixgbevf-${ixgbevf_version}.tar.gz"
pushd "ixgbevf-${ixgbevf_version}/src"
echo "Building ixgbevf version ${ixgbevf_version} kernel module from source"
make install

# Cleanup
popd
echo "Removing ixgbevf source files"
rm -rf /root/ixgbevf-${ixgbevf_version}.tar.gz /root/ixgbevf-${ixgbevf_version}/


## Install Amazon ENA kernel module
# Required for enhanced networking on F1, G3, I3, P2, R4, X1, and m4.16xlarge instance types
export http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011 https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011
export ena_version="1.3.5"
export ena_commit=857b4b8d81a1b3ca2a4af29695acda57cabee9cc
export unpack_directory="$(mktemp --tmpdir -d)"
export kernel_version="$(uname -r)"

# Enable Amazon ENA
# Create an archive file locally from git first, in order to pin the version until we're ready to upgrade
# Pulled that trick and the general ENA install workflow from https://github.com/claranet/centos7-ami/blob/master/build.sh
mkdir -p "${unpack_directory}/ena"
echo "Retrieving ENA module source files"
git clone "https://github.com/amzn/amzn-drivers.git" "${unpack_directory}/ena"
cd "${unpack_directory}/ena"
echo "Installing ENA module ${ena_version} (commit ${ena_commit})"
git archive --prefix ena-${ena_version}/ ${ena_commit} | tar xC /usr/src
cat > /usr/src/ena-${ena_version}/dkms.conf << EOF
PACKAGE_NAME="ena"
PACKAGE_VERSION="${ena_version}"
CLEAN="make -C kernel/linux/ena clean"
MAKE="make -C kernel/linux/ena/ BUILD_KERNEL=\${kernelver}"
BUILT_MODULE_NAME[0]="ena"
BUILT_MODULE_LOCATION="kernel/linux/ena"
DEST_MODULE_LOCATION[0]="/updates"
DEST_MODULE_NAME[0]="ena"
AUTOINSTALL="yes"
EOF
echo "Adding ENA module to kernel"
dkms add -m ena -v "${ena_version}"
dkms dkms build -m ena -v "${ena_version}" -k "${kernel_version}"
dkms install -m ena -v "${ena_version}" -k "${kernel_version}"
# Remove the temporary install files
rm -rf "${unpack_directory}"


## Recreate initramfs images with new kernel modules
#dracut --force
echo "Recreating initramfs images with ixgbevf and ENA kernel modules"
rpm -qa kernel | sed 's/^kernel-//' | xargs -I "{}" dracut --force /boot/initramfs-{}.img {}


