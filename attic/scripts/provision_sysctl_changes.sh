#!/bin/bash --

# Apply changes from NXCR-160
if [[  -f  /etc/sysctl.conf  ]]; then

echo "Applying sysctl changes"
cat >> /etc/sysctl.conf << EOF_sysctl
# NXCR-160
net.ipv4.conf.all.rp_filter = 1
EOF_sysctl

fi

# Run changes and regenerate initramfs
if [[ $? == 0 ]]; then
   sysctl -p 
   echo "Generating new initramfs /boot/initramfs-$(uname -r)"
   rpm -qa kernel | sed 's/^kernel-//' | xargs -I "{}" dracut --force --verbose /boot/initramfs-{}.img {}
else
   echo "Failed to apply systctl values"
fi 
