#!/usr/bin/env bash


# Description:
# Script generates a JSON file with markup language to post a page on Confluence using REST API
#
#


print_help(){
cat <<HELP
$0  [-n|--name] <IMAGE_NAME> [ [-l|--local] [-C|--cid] <CONFLUENCE_PAGE_ID>  [-j|--jira] <JIRA_ISSUE> [-d|--debug] [-h|--help] [-s|--space]  <CONFLUENCE_SPACE_KEY>  ]"

[-A|--auth]    <CONFLUENCE_ACCESS>      HTTP basic authentication base64 encoded
[-C|--cid]     <CONFLUENCE_PAGE_ID>     Confluence document ID ( Can be found by running API call http://web.global.nibr.novartis.net/apps/confluence/rest/api/content?spaceKey=<SPACE> )
[-d|--debug]                            Debug
[-h|--help]                             Prints help
[-l|--local]                            Run on local machine
                                        NOTE: Can't be used with [ -n|--name ] switch
[-j|--jira]     <JIRA_ISSUE>            Jira issue number ( e.g. PE-11856 )
                                        NOTE: The parameter fetches Jira issue and extracts referenced tasks used to generate
                                               changelog with release updates
[-n|--name]     <IMAGE_NAME>            Name of an existing image ( e.g. 'rhel7-vagrant-20180810-1533899044' )
[-s|--space]    <CONFLUENCE_SPACE_KEY>  Confluence key space (e.g. ~sys_confli1 )
[-S|--server]   <CONFLUENCE_SERVER>     Conluence server (e.g. web.global.nibr.novartis.net )


HELP
}

debug(){
  set -x
  set -v
  CURL_DEBUG='-v'
}


LOCAL=0

#---------------------------------------------------------------------------
#                             Options
#---------------------------------------------------------------------------
# Based on a following article http://mywiki.wooledge.org/BashFAQ/035
while :; do
    case $1 in
        -A|--auth)
            if [ "$2" ]; then
                CONFLUENCE_ACCESS=$2
                shift
            else
                die 'ERROR: "-A|--auth" requires a non-empty option argument.'
            fi
            ;;
        -C|--cid)
            if [ "$2" ]; then
                CONFLUENCE_PAGE_ID=$2
                shift
            else
                die 'ERROR: "-C|--cid" requires a non-empty option argument.'
            fi
            ;;
        -j|--jira)
            if [ "$2" ]; then
                JIRA_ISSUE=$2
                shift
            else
                die 'ERROR: "-j|--jira" requires a non-empty option argument.'
            fi
            ;;
        -h|-\?|--help)
            print_help    # Display a usage synopsis.
            exit
            ;;
        -d|--debug)
            debug
            ;;
        -n|--name)
            if [ "$2" ]; then
                IMAGE_NAME=$2
                shift
            else
                die 'ERROR: "-n|--name" requires a non-empty option argument.'
                exit
            fi
            ;;
        -s|--space)
            if [ "$2" ]; then
                CONFLUENCE_SPACE_KEY=$2
                shift
            else
                die 'ERROR: "-s|--space" requires a non-empty option argument.'
            fi
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            #print_help
            break
            exit 1
    esac
    shift
done

#---------------------------------------------------------------------------
#                              Checks
#---------------------------------------------------------------------------

[[ -z  ${IMAGE_NAME} ]] && { echo "ERROR! IMAGE_NAME can't be empty" ; exit 1 ; }

### code to generate image information
PAYLOAD=''
PACKAGES=''


# image name
# jq ".builds[].files[].name" manifest-centos7-ami-20180820-1534751111.json
# "nibr baseos nxc centos 7 x86_64 (nxc) 20180820-1534751111.ova"
#

if [[ -n ${IMAGE_NAME}  &&  -f "../build-info/${IMAGE_NAME}/manifest-${IMAGE_NAME}.json" ]]; then
    NAME=$( jq ".builds[].files[].name" ../build-info/${IMAGE_NAME}/manifest-${IMAGE_NAME}.json | sed -e 's/\"//g' )
    NAME=${NAME%%.*}
else
    echo "ERROR! Can't locate manifest file [ ../build-info/${IMAGE_NAME}/manifest-${IMAGE_NAME}.json ]."
    print_help
    exit 1
fi

# extract version
IMAGE_VERSION=${IMAGE_NAME##*[![:digit:]-]?}


#[[ -z ${IMAGE_NAME} ]] && { echo "ERROR! Image name is missing. " ; exit 1 ; } || { exit 0; }

# Conflience page title
PAGE_TITLE="${NAME}"

PACKAGES_TABLE_HEAD="||NAME||SUMMARY||VERSION||RELEASE||\n"

#---------------------------------------------------------------------------
#                              Settings
#---------------------------------------------------------------------------

# Confluence settings
#CONFLUENCE_PARENT_ID='118882315'
#CONFLUENCE_PARENT_ID='117164668'  ### PROD
if [[ -n ${CONFLUENCE_PAGE_ID} ]]; then
    CONFLUENCE_PARENT_ID=${CONFLUENCE_PAGE_ID}
else
    CONFLUENCE_PARENT_ID='125118587' ### PROD
fi


CONFLUENCE_SPACE_KEY='~sys_confli1'
## PROD
CONFLUENCE_SERVER='web.global.nibr.novartis.net/apps/confluence' ### PROD
## Test
#CONFLUENCE_SERVER='confluence.tst.nibr.novartis.net'
### Dev
###CONFLUENCE_SERVER='web-test.global.nibr.novartis.net/apps/confluence'


# jira settings
JIRA_ACCESS=${CONFLUENCE_ACCESS}
JIRA_SERVER="jirasw.prd.nibr.novartis.net"


# checks
[[ -z  ${CONFLUENCE_PARENT_ID} ]] && { echo "ERROR! Confluence parent ID is missing" ; exit 1 ; }
[[ -z  ${CONFLUENCE_ACCESS} ]] && { echo "ERROR! Confluence basic auth is missing" ; exit 1 ; }
[[ -z  ${CONFLUENCE_SPACE_KEY} ]] && { echo "ERROR! Confluence space key is missing" ; exit 1 ; }
[[ -z  ${CONFLUENCE_SERVER} ]] && { echo "ERROR! Confluence server is missing" ; exit 1 ; }



#---------------------------------------------------------------------------
#                              Main
#---------------------------------------------------------------------------


# check if confluence server is alive
#[[ $( ping  -c 1 ${CONFLUENCE_SERVER}  &>/dev/null ) -ne 0 ]] && { echo "ERROR! Confluence server ${CONFLUENCE_SERVER} is not reachable" ; exit 1 ;}

## jira
#
# https://jirasw.prd.nibr.novartis.net/rest/api/latest/issue/${ISSUE_NUMBER}
#

if [[ -n ${JIRA_ISSUE} ]];then

  CHANGELOG="h2. Changelog\\n"
  CHANGELOG+=$( curl ${CURL_DEBUG} -s -k -H "Authorization: Basic ${JIRA_ACCESS}" -H 'Content-Type: application/json' -H 'Accept: application/json' https://${JIRA_SERVER}/rest/api/latest/issue/${JIRA_ISSUE} | jq ".fields.issuelinks[].outwardIssue.fields.summary" jira_issue.json  | sed -e 's/AWS AMIs: //g' -e 's/^\"/\* /g' -e 's/\"$/\\n/g'  -e 's/null//g'  )

  # check changelog
  if [[ ${CHANGELOG} == 'null' ]];then
      echo "ERROR! JIRA API call didn't return anyhing"
      exit 1
  fi

fi


# check does our endpoint exist
CHECK_ENDPOINT=$( curl ${CURL_DEBUG} -k -s  -H "Authorization: Basic ${CONFLUENCE_ACCESS}" -H 'Content-Type: application/json' -H 'Accept: application/json' https://${CONFLUENCE_SERVER}/rest/api/content/${CONFLUENCE_PARENT_ID} | jq ".data.successful"   )

if [[ -z ${CHECK_ENDPOINT} || ${CHECK_ENDPOINT} = "false"  ]]; then
   echo "FAILED! Failed to reach Confluence endpoint [ https://${CONFLUENCE_SERVER}/rest/api/content/${CONFLUENCE_PARENT_ID} ]. Please check if exists."
   exit 1
fi


if [[ -f "../build-info/${IMAGE_NAME}/${IMAGE_NAME}-packages_list.txt" ]]; then
    # generate rpm list
    PACKAGES_LIST=$( sort "../build-info/${IMAGE_NAME}/${IMAGE_NAME}-packages_list.txt"  |  perl -ne 's/\n/\\n/; print $_' | sed -e 's/"//g ')
else
    echo "ERROR! Can't find file [ ../build-info/${IMAGE_NAME}/${IMAGE_NAME}-packages_list.txt ]"
    exit 1
fi

# generate repository list if available
if [[ -f  "../build-info/${IMAGE_NAME}/${IMAGE_NAME}-repo_list.txt" ]];then

    #
    REPOSITORY_LIST=$(perl -ne "if (/[\w\s-]+:\s\'([\w\s\(\)].*)\'/) {  s/[\w\s-]+:\s//g ; s/^'/|/g ; s/'$/|\\\n/ ; s/\n//g ; print $1}" ../build-info/${IMAGE_NAME}/${IMAGE_NAME}-repo_list.txt)
fi

# get aws cli version if available
if [[ -f "../build-info/${IMAGE_NAME}/${IMAGE_NAME}-awscli_version.txt" ]];then
    AWSCLI="h2. AWS tools\n ||AWSCLI|$( cat ../build-info/${IMAGE_NAME}/${IMAGE_NAME}-awscli_version.txt )|\n"
fi


#page header
PAGE_HEADER=$( cat << HEADER
h2. Image details\\n \
||NAME|${NAME}|\\n \
||VERSION|${IMAGE_VERSION}|\\n
HEADER
)


[[ -n ${REPOSITORY_LIST} ]] && REPOSITORY_INFO=$( cat << REPO_INFO
\\n \
h2. Yum repositories\\n \
||ENABLED REPOSITORIES||\\n \
${REPOSITORY_LIST}
REPO_INFO
)


# page footer
PACKAGE_HEADER=$( cat << PACKAGE_HEADER
\\n \
h2. RPM packages\\n
PACKAGE_HEADER
)

# generate payload data
PAYLOAD=${PAGE_HEADER}
[[ -n ${CHANGELOG} ]] && PAYLOAD+=${CHANGELOG}
[[ -n ${AWSCLI} ]] && PAYLOAD+=${AWSCLI}
[[ -n ${REPOSITORY_INFO} ]] && PAYLOAD+=${REPOSITORY_INFO}
[[ -n ${PACKAGE_HEADER} ]] && PAYLOAD+=${PACKAGE_HEADER}
[[ -n ${PACKAGES_TABLE_HEAD} ]] && PAYLOAD+=${PACKAGES_TABLE_HEAD}
[[ -n ${PACKAGES_LIST} ]] && PAYLOAD+=${PACKAGES_LIST}



JSON=$( cat << JSON
{
  "type": "page",
  "ancestors": [{ "type": "page", "id": "${CONFLUENCE_PARENT_ID}" }],
  "title": "${PAGE_TITLE}",
  "space": { "key": "${CONFLUENCE_SPACE_KEY}" },
  "body": { "storage": { "value": "${PAYLOAD}", "representation": "wiki" } }
}
JSON
)


[[ -n ${JSON} ]]&& printf "%s"  "${JSON}" >  ../build-info/${IMAGE_NAME}/${IMAGE_NAME}-json_payload.json


RET_RESPONSE=$(curl ${CURL_DEBUG} -s  -k -H "Authorization: Basic ${CONFLUENCE_ACCESS}" -H 'Content-Type: application/json' -H 'Accept: application/json' -d @../build-info/${IMAGE_NAME}/${IMAGE_NAME}-json_payload.json https://${CONFLUENCE_SERVER}/rest/api/content | jq '.statusCode, ._links.base + ._links.webui' | perl -ne 's/\n$/;/; print $_' | sed 's/"//g'   )
RET_CODE=$(echo ${RET_RESPONSE} | cut -d";" -f 1)
RET_URL=$(echo ${RET_RESPONSE} | cut -d";" -f 2)



# check response code if any
[[ $( echo ${RET_CODE##RESPONSE:})  -eq  "400" || $( echo ${RET_CODE##RESPONSE:})  -gt "400"   ]] && { echo "ERROR: I got HTTP response [ ${RET_CODE##RESPONSE:} ]" ; exit 1 ;}
# print link to the document if any
[[ -n ${RET_URL} ]] && echo "Link to release notes: [ ${RET_URL} ]"
