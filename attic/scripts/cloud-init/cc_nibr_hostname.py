# vi: ts=4 expandtab
#
#    Copyright (C) 2009-2010 Canonical Ltd.
#    Copyright (C) 2012 Hewlett-Packard Development Company, L.P.
#
#    Author: Scott Moser <scott.moser@canonical.com>
#    Author: Juerg Haefliger <juerg.haefliger@hp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3, as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
from cloudinit.settings import PER_INSTANCE, PER_ALWAYS
from cloudinit import util

# Modules are expected to have the following attributes.
# 1. A required 'handle' method which takes the following params.
#    a) The name will not be this files name, but instead
#    the name specified in configuration (which is the name
#    which will be used to find this module).
#    b) A configuration object that is the result of the merging
#    of cloud configs configuration with legacy configuration
#    as well as any datasource provided configuration
#    c) A cloud object that can be used to access various
#    datasource and paths for the given distro and data provided
#    by the various datasource instance types.
#    d) A argument list that may or may not be empty to this module.
#    Typically those are from module configuration where the module
#    is defined with some extra configuration that will eventually
#    be translated from yaml into arguments to this module.
# 2. A optional 'frequency' that defines how often this module should be ran.
#    Typically one of PER_INSTANCE, PER_ALWAYS, PER_ONCE. If not
#    provided PER_INSTANCE will be assumed.
#    See settings.py for these constants.
# 3. A optional 'distros' array/set/tuple that defines the known distros
#    this module will work with (if not all of them). This is used to write
#    a warning out if a module is being ran on a untested distribution for
#    informational purposes. If non existent all distros are assumed and
#    no warning occurs.

frequency = PER_INSTANCE
# for testing:
#frequency = PER_ALWAYS

_ORIG_HOSTNAME_FILE='/etc/nibr_hostname.orig'

def handle(name, cfg, cloud, log, args):
    if util.get_cfg_option_bool(cfg, "nibr_hostname_disable", False):
        log.debug(("NIBR:%s Configuration option 'nibr_hostname_disable' is set,"
                   " not setting the hostname"), name)
        return
    
    if util.get_cfg_option_bool(cfg, "preserve_hostname", False):
        log.debug(("NIBR:%s Configuration option 'preserve_hostname' is set,"
                   " not setting the hostname"), name)
        return
    
    instance_id = cloud.get_instance_id()
    if len(instance_id) < 6:
        log.debug(("NIBR:%s instance_id %s is too short for a hostname in module %s"),
                  name, instance_id )
        return
    
    
    (hostname, fqdn) = util.get_hostname_fqdn(cfg, cloud)
    
    # save a copy of the original hostname for.. reasons?
    try:
        if not os.path.exists(_ORIG_HOSTNAME_FILE):
            with open(_ORIG_HOSTNAME_FILE, 'w') as origfile:
                origfile.write(hostname)
    except Exception:
        util.logexc(log, "NIBR: Failed to write original hostname(%s) to %s, continuing anyway.",
                    hostname,
                    _ORIG_HOSTNAME_FILE)
        
    dotidx = fqdn.find('.')
    if dotidx == -1:
        try:
            log.warning("NIBR:%s Setting just the hostname, fqdn(%s) of this machine looks strange..",name,fqdn)
            cloud.distro.set_hostname(instance_id)
        except Exception:
            util.logexc(log, "NIBR:%s Failed to set the hostname to %s (fqdn was %s)",
                        name,
                        instance_id,
                        fqdn)
            raise
    else:
        try:
            domain = fqdn[dotidx:]
            fqdn = "%s%s"%(instance_id, domain)
            
            log.warning("NIBR:%s Setting the hostname to instance_id (%s)",
                        name,
                        fqdn)
            cloud.distro.set_hostname(instance_id, fqdn)
            
        except Exception:
            util.logexc(log, "NIBR:%s Failed to set the hostname to %s (%s)", 
                        name,
                        instance_id,
                        fqdn)
            raise



# NIBR TODO: If DHCP is still broken, or if we use MDNS/MDHCP:
# Here is where we would register
# our A + PTR records with DNS:
# this is a dummy example, best to break out a library for this.
#curl POST infoblox.prd.nibr.novartis.net
#   data= { 'ip':       cloud.cfg.default_ip,
#           'name':     '%s.cloud.nibr.novartis.net'%(instance_id)
#           'with_ptr': 'true',
#           'issuer':   'sys_nxcr_amidns'}
#
# think about how to sweep the cloud.nibr.novartis.net domain and clean these up though.
