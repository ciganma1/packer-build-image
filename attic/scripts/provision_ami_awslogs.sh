#!/bin/bash --


### Add awslogs CloudWatch agent on AMI builds


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail

# Version

os_version=$(/usr/local/bin/acs-nxfacts get core os_release_major)
## Create a working directory
echo "Creating temp directory for awslogs install"
export installer_directory="$(mktemp --tmpdir -d)"
pushd "${installer_directory}"

## Download the install sources
# If needed, the static locations of the sources initially used for this are:
# https://s3.amazonaws.com/aws-cloudwatch/downloads/1.4.1/awslogs-agent-setup.py
# https://s3.amazonaws.com/aws-cloudwatch/downloads/1.4.1/AgentDependencies.tar.gz
echo "Downloading the awslogs install sources"
https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011 curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011 curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/AgentDependencies.tar.gz | tar xz

## Patch the setup script
# By default, virtualenv downloads the latest versions of pip, setuptools, and wheel when it creates an environment, rather than using the ones it is bundled with
# This causes a problem on CentOS/RHEL 6, because they are still on python 2.6, and wheel has dropped support for python 2.6 starting with wheel 0.30.0
# Virtualenv has a --no-download flag to tell it not to try to pull newer versions from PyPI, which should have been used in the setup script, as the code in question is only used for standalone installations where the dependencies have all been pre-downloaded
# So we add the necessary flag using sed. As long as the general format of the call to virtualenv remains the same, this should work. If it changes, hopefully because Amazon fixed the problem themselves, then the initial pattern won't match and sed won't change anything
echo "Patching the awslogs setup script"
sed -i 's/%s %s --python %s %s/%s %s --python %s %s --no-download/' awslogs-agent-setup.py

## Create an empty config file
echo "Creating a blank awslogs config"
touch awslogs.conf

## Run the install
echo "Installing awslogs CloudWatch agent"
python ./awslogs-agent-setup.py --region us-east-1 --dependency-path ./AgentDependencies --non-interactive --configfile=./awslogs.conf

## Make sure we ended up with the right versions of everything
# pip itself should be <7.0, and wheel should be ==0.29.0
echo "awslogs installed. pip versions are:"
/var/awslogs/bin/pip --version
/var/awslogs/bin/pip freeze

## Stop and disable the service, since not everyone uses it and it doesn't have a real config
# Currently the setup script starts the service, but doesn't enable it at boot. We explicitly disable it anyway, just in case Amazon changes something unexpectedly. As they tend to do.
echo "Stopping awslogs and making sure it doesn't start at boot"
if [[ ${os_version} =~ "6" ]];then
service awslogs stop
chkconfig awslogs off
chkconfig --list awslogs
fi

if [[ ${os_version} =~ "7" ]];then
systemctl stop  awslogs
systemctl disable  awslogs
fi

## Clean up the pid file, in case it already crashed before we could stop it
# This tends to happen with the bogus config
echo "Removing stale awslogs pid file, if necessary"
rm --force --verbose /var/awslogs/state/awslogs.pid

## Move the setup script, in case someone wants to re-run it with --only-generate-config
# Though it's probably more likely that people who use it will just add a real config file and enable/start the service
echo "Moving awslogs setup script to /var/awslogs"
mv awslogs-agent-setup.py /var/awslogs/

## Remove the install sources
echo "Removing awslogs install sources"
popd
rm --recursive --force --verbose "${installer_directory}"
