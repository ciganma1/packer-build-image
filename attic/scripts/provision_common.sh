#!/bin/bash --


### Do configuration common to all builds


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Gather information needed for branching later
major_release=$(cat /etc/system-release | sed -r 's/(Red Hat Enterprise Linux Server|CentOS Linux|CentOS) release ([67]).*/\2/')


## Change syslog format
sed -r -i"" 's/^[[:space:]]*\$ActionFileDefaultTemplate[[:space:]]+.*$/$ActionFileDefaultTemplate RSYSLOG_SyslogProtocol23Format/' /etc/rsyslog.conf 


## Don't require tty for sudo, or kitchen whines like a little baby
echo "Removing tty requirement from /etc/sudoers"
sed -i -r 's/^(Defaults[[:space:]]+requiretty.*)/#\1/' /etc/sudoers
sed -i -r 's/^(Defaults[[:space:]]+!visiblepw.*)/#\1/' /etc/sudoers


## Turn off SSH DNS lookups
# This is necessary for the vagrant build, and it makes more sense to have it be consistent
# Security baseline should also take care of this, but doing it here in case the baseline is removed from the build again at some point
echo "Disabling SSH DNS lookups"
echo "UseDNS no" >> /etc/ssh/sshd_config


## Install extra software
# kernel-devel and kernel-headers are needed for guest additions on vagrant builds and enhanced networking on AMIs
# jq is useful for working with json
# python-pip is needed for the ami build, and is widely enough used to merit common install
# dkms is needed for vbox guest additions and ENA
# cloud-utils-growpart is needed to expand the LVM partition
# acs-update for OS patching
# acs-nxfacts for fact management
# screen, because it should be on every server
# gcc, iputils, nc, ruby, wget, net-tools, bind-utils, ntp, ntpdate, man, man-pages, and nfs-utils have all been requested to be on the build
#   many of these are probably already in @core/@base, but it doesn't hurt to explicitly list them here
# yum-plugin-priorities and yum-plugin-versionlock are lengthy installs, so better to pre-install them
# git is needed to enable ENA support
echo "Installing pre-reqs and basic software"
yum -y install kernel-devel kernel-headers jq python-pip dkms cloud-utils-growpart acs-update acs-nxfacts screen gcc iputils nc ruby wget net-tools bind-utils ntp ntpdate man man-pages nfs-utils yum-plugin-priorities yum-plugin-versionlock git nscd


## Install OS-specific packages
# RHEL/CentOS 7 only
if [[ "${major_release}" == "7" ]]; then
  # Dowgrade kernel. Newer ones have issues with NVMe SSDs used on i3 instances not showing up consistently
#  yum -y install kernel-3.10.0-327.36.3.el7 kernel-devel-3.10.0-327.36.3.el7 kernel-headers-3.10.0-327.36.3.el7
  :
fi
if [[ "${major_release}" == "6" ]]; then
  :
fi


## Disable all services, then reactivate the ones we want
echo "Disabling unwanted services"
# Branch based on major OS version
case "$major_release" in
  6) # RHEL/CentOS 6
     for serv in $(chkconfig --list|awk '{print $1}'); do echo "Disabling service ${serv} (chkconfig)"; chkconfig ${serv} off; done
     # From on-premise kickstart: haldaemon lvm2-monitor messagebus network sshd udev-post
     # By request, enabled on by default until Chef handles this: crond ntpd
     # Added for consistency with RHEL/CentOS 7: rsyslog
     ENABLED_SERVICES="rsyslog crond haldaemon lvm2-monitor messagebus network sshd udev-post ntpd nscd"
     for serv in $ENABLED_SERVICES; do echo "Enabling service ${serv} (chkconfig)"; chkconfig ${serv} on; done
  ;;
  7) # RHEL/CentOS 7
     for serv in $(systemctl list-unit-files -t service | grep enabled | awk '{print $1}' | grep -Ev 'autovt@.service|getty@.service|dbus-org.freedesktop.NetworkManager.service|dbus-org.freedesktop.nm-dispatcher.service'); do echo "disable service $serv (systemctl)"; systemctl disable $serv; done
     for serv in $(/sbin/chkconfig --list 2>/dev/null| grep '3:on'| awk '{print $1}'); do echo "Disabling service ${serv} (chkconfig)"; /sbin/chkconfig ${serv} off 2>/dev/null; done
     # From on-premise kickstart: irqbalance lvm2-monitor microcode NetworkManager-dispatcher NetworkManager rsyslog sshd systemd-readahead-collect systemd-readahead-drop systemd-readahead-replay
     # Additional services required to import properly to AWS: kdump libstoragemgmt mdmonitor sysstat tuned network
     # By request, enabled by default until Chef handles this: crond ntpd
     ENABLED_SERVICES="crond kdump libstoragemgmt mdmonitor sysstat tuned network irqbalance lvm2-monitor microcode NetworkManager-dispatcher NetworkManager rsyslog sshd systemd-readahead-collect systemd-readahead-drop systemd-readahead-replay ntpd nscd"
     for serv in $ENABLED_SERVICES; do echo "Enabling service ${serv} (systemctl)"; systemctl enable ${serv}.service; done
  ;;
esac


## Wait for the network to be up, primarily to address DNS issues in EC2
echo "Adding NETWORKWAIT=yes to /etc/sysconfig/network"
echo 'NETWORKWAIT=yes' >> /etc/sysconfig/network


## Enable dmesg timestamps
echo "Enabling dmesg timestamps"
grubby --update-kernel=ALL --args="printk.time=1"


## Add noexec to /dev/shm
if [[ "${major_release}" == "7" ]]; then
  # On RHEL/CentOS 7, add an entry for /dev/shm with the default options + noexec
  echo "Adding entry for /dev/shm with noexec option to /etc/fstab"
  echo 'tmpfs /dev/shm tmpfs rw,nosuid,nodev,noexec 0 0' >> /etc/fstab
fi
if [[ "${major_release}" == "6" ]]; then
  # On RHEL/CentOS 6, add noexec to the existing entry
  echo "Adding noexec option to /dev/shm entry in /etc/fstab"
  sed --in-place --regexp-extended 's|^([[:space:]]*tmpfs[[:space:]]+/dev/shm[[:space:]]+tmpfs[[:space:]]+[^[:space:]]*)(.*$)|\1,noexec\2|' /etc/fstab
fi


## Add Novartis and QuoVadis cert chains
# Already copied into place by an earlier step in the Packer build
echo "Installing Novartis and QuoVadis CAs"
if [[ "${major_release}" == "7" ]]; then
  # RHEL/CentOS 7
  update-ca-trust extract
fi
if [[ "${major_release}" == "6" ]]; then
  # RHEL/CentOS 6
  update-ca-trust enable
  update-ca-trust extract
fi
# Pull some of the auto-generated comments from the resulting cert bundle
# Could be useful for debugging if something goes wrong
echo "Novartis and QuoVadis CAs:"
grep --ignore-case --extended-regexp 'quovadis|novartis' /etc/ssl/certs/ca-bundle.crt


## Make sure myhostname is in /etc/nsswitch.conf
# This is missing in CentOS 6, and has recently been removed from CentOS 7
# Standardizing it to what we use on-premise
# More investigation may be needed if it is not on the resulting AMI
echo "Fixing hosts entry in nsswitch.conf"
sed --in-place --regexp-extended 's/^([[:space:]]*hosts:[[:space:]]*).*$/\1files dns myhostname/' /etc/nsswitch.conf

# Disable IPv6 in grub
grubby --update-kernel=ALL --args="ipv6.disable=1"

# Remove IPv6 addresses from /etc/hosts
sed --in-place --regexp-extended '/[[:space:]]*\:\:1[[:space:]]*/d' /etc/hosts
