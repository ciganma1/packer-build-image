#!/bin/bash --


### Implement items from the security baseline
# Mostly pulled verbatim from Jon's build


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail



cd /etc/sysconfig/


## Sec 3.1.2
sed 's/PROMPT=yes/PROMPT=no/' init | sed 's/SINGLE=\/sbin\/sushell/SINGLE=\/sbin\/sulogin/' > /tmp/tmp-init
cp /tmp/tmp-init init
echo "~:S:wait:/sbin/sulogin" >> /etc/inittab


## Sec 3.1.3
cat /etc/fstab | awk '$0 ~ /ext/ { sub("defaults", "nodev,defaults"); print } $0 !~ /ext/ {print $0}' > /tmp/tmp-fstab
cp /tmp/tmp-fstab /etc/fstab


## CIS L1 baseline 1.7.1.1, 1.7.1.2 , 1.7.1.3

login_legalese="Use of this network is restricted to authorized users only. User activity may be monitored and/or recorded. Anyone using this network expressly consents to such monitoring and/or recording. BE ADVISED: if possible criminal activity is detected, these records, along with certain personal information, may be provided to law enforcement officials."

# Old legalese from pre-CIS Sec 3.1.6
#Use of this network is restricted to authorized users only. User activity may be monitored and/or recorded. Anyone using this network expressly consents to such monitoring and/or recording. BE ADVISED: if possible criminal activity is detected, these records, along with certain personal information, may be provided to law enforcement officials.

echo "${login_legalese}" > /etc/issue
echo "${login_legalese}" > /etc/issue.net
echo "${login_legalese}" > /etc/ssh/banner
echo "Banner /etc/ssh/banner" >> /etc/ssh/sshd_config


motd_legalese=" +-----------------------------------------------------------------------------+
 | Programs and Networks are only to be used according to the Corporate        |
 | Guidelines (see IGM.BSL.WS.V01)                                             |
 +-----------------------------------------------------------------------------+" > /etc/motd

echo -e "${motd_legalese}\n" >> /etc/motd

## To support IPv6 disabling - NXCR-194
echo "AddressFamily inet" >> /etc/ssh/sshd_config

## Sec 3.1.7
echo "IgnoreRhosts yes" >> /etc/ssh/sshd_config


## Sec 3.1.8
echo "HostbasedAuthentication no" >> /etc/ssh/sshd_config


## Sec 3.1.9
## Not happening. Chef needs this...
## echo "PermitRootLogin no" >> /etc/ssh/sshd_config


## Sec 3.1.9
echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config


## Sec 3.2.1
echo "MaxAuthTries 4" >> /etc/ssh/sshd_config

#### sshd_config tweaks
sed -i.bak /etc/ssh/sshd_config -e 's/^PasswordAuthentication no/PasswordAuthentication yes/'
sed -i.bak /etc/ssh/sshd_config -e 's/^.*UseDNS yes/UseDNS no/'


## Sec 3.2.3
echo "LOGIN_RETRIES 10" >> /etc/local.defs


## Sec 3.2.7
echo "PASS_MAX_DAYS 60" >> /etc/local.defs
echo "PASS_MIN_DAYS 7" >> /etc/local.defs
echo "PASS_MIN_LEN 14" >> /etc/local.defs
echo "PASS_WARN_AGE 7" >> /etc/local.defs

chmod 600 /etc/ssh/sshd_config

# /usr/sbin/usermod -L SYSACCT
# /usr/sbin/usermod -s /sbin/nologin SYSACCT

