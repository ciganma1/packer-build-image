#!/bin/bash

/usr/bin/yum clean all && /usr/bin/yum -y --disablerepo="*" --enablerepo=acs-local update "acs-*" 

/usr/local/libexec/acs/acs-nxfacts-refresh-local
days_var=`/usr/local/bin/acs-nxfacts get cloud tags | jq .acsupdateperiod |tr -d '"'`
days=${days_var:-7}
last_update_time=`/usr/local/bin/acs-nxfacts get patching last_run`

if [ -z "$last_update_time" ]; then
   lasttime=$(date --date="1950-01-01" +"%s")
else
   lasttime=$(date --date="$last_update_time" +"%s")
fi

expected_update_time=$((lasttime + days*24*60*60))
today_time=`date +%s`
if [[ $today_time -ge $expected_update_time ]]; then
   /usr/local/bin/acs-update-me -n -t security
else
   echo "Not updating security updates because $days days not completed after last update"
fi
