 /*

Description:
-------------

Jenkins declarative pipeline generated Kitchen skeleton files, runs runs kitchen create , kitchen converge with nx_core cookbooks, verify ( without inspec profiles , not ready yet).
After succesful tests it sends notification via email

Parameters:
-------------

VAGRANT_NAME:

- name of Vagrant image


VAGRANT_IMAGE:

- path to an existing Vagrant image


*/


/*/////////////////////////////////////////////////////////////////
   Settings
*//////////////////////////////////////////////////////////////////

// default bitbucket settings
BITBUCKET_CRED_ID = 'BITBUCKET_AUTOMATION_ID'
BITBUCKET_USER_ID = 'nibr_linux_servers'
BITBUCKET_SLUG = 'novartisnibr'



// default environment settings
env.OS_BUILD_TYPE = 'VAGRANT'
env.NODE = 'packer-bm'
env.CUSTOM_WORKSPACE = '/apps/nx_build'


// default proxy settings
env.HTTP_PROXY="http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"
env.HTTPS_PROXY="https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"


// default role cookbook
ROLE_COOKBOOK = 'nxcore_kitchen_run'
ROLE_PROJECT_NAME = 'role_nxcore_kitchen_run'

// git projects
GIT_NX_CORE_PROJECTS = ["augeas","nx_concat","nx_core","nx_core_yum","nx_core_ulimits","nx_core_sysctl","nx_core_sudo","nx_core_ssh","nx_core_snmp","nx_core_rsyslog","nx_core_resolv","nx_core_patching","nx_core_ntp","nx_core_nscd","nx_core_network","nx_core_mountfs","nx_core_mail","nx_core_local_accounts","nx_core_kdump","nx_core_fusioninventory","nx_core_data","nx_core_boot","nx_core_backup","nx_core_auth","nx_core_auditd","nx_core_sysctl","nx_core_ulimits","nx_core_yum","nx_compliance"]


// default build info answer
ENV_CONFLUENCE_ANSWER = ["yes","no"]

// fallback email recipient in case of failed build
env.INFO_EMAIL = 'marek.ciganek@novartis.com'

// set if upload to repository is enabled
UPLOAD_TO_REPO = ''


/*/////////////////////////////////////////////////////////////////
  Functions
*//////////////////////////////////////////////////////////////////


def send_email_notification(rcp,stat){
    def recipient       = rcp
    def build_status    = stat
    def mail_body       = ""

        mail_body       = """
Build details:\n\n\n
JOB NAME: ${env.JOB_NAME}
BUILD ID: ${env.BUILD_ID}
NODE NAME: ${env.NODE_NAME}
URL: ${env.BUILD_URL}
RUN DISPLAY: ${env.RUN_DISPLAY_URL}
"""
if (  VAGRANT_IMAGE != null &&  VAGRANT_NAME != null){

        mail_body   += """


Vagrant details:\n\n\n
VAGRANT IMAGE: ${env.VAGRANT_IMAGE}
VAGRANT NAME: ${env.VAGRANT_NAME}
"""
    if ( build_status == 'Successful') { mail_body = "${mail_body}" }

        if ( recipient.size() == 0 ) { return }

        mail(to: recipient,
            subject: "${env.JOB_NAME} BUILD: ${env.BUILD_ID} ${build_status}",
            body: mail_body
            )
    }

}
/*/////////////////////////////////////////////////////////////////
  Pipeline section
*//////////////////////////////////////////////////////////////////

pipeline {

    //run only on a dedicated build host
    agent {
        node {
            label NODE
            customWorkspace CUSTOM_WORKSPACE
        }
    }
    // we terminate the build after 90 minutes
    options {
        timeout(time: 30, unit: 'MINUTES')
        timestamps()
    }


    parameters {

        string(
                name: "VAGRANT_NAME",
                description: "Vagrant VM  hostname")

        string(
                name: "VAGRANT_IMAGE",
                description: "Vagrant VM image name")

    }

    stages {

        /*
            Stage expects input parameters
        */

        stage('Preparation') {
            steps {
                script {

                        // validate input parameter
                        if ( params.VAGRANT_NAME != '' ){
                            VAGRANT_NAME = params.VAGRANT_NAME
                        }else{
                            error("VAGRANT_NAME parameter can't be empty")
                        }

                        // validate input parameter
                        if ( params.VAGRANT_IMAGE != '' ){
                            VAGRANT_IMAGE = params.VAGRANT_IMAGE
                        }else{
                            error("VAGRANT_IMAGE parameter can't be empty")
                        }

                        // Check if image really exists
                        if ( !fileExists(VAGRANT_IMAGE) ){
                            error("Unable to locate: [ ${VAGRANT_IMAGE} ]")
                        }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage clones nx_core repos
        */

        stage('Clone repository'){

            steps {

                // clone repo
                //timeout(time: 30, unit: 'SECONDS'){
                    script {
                            def output_git_commands  = ''
                            try {
                                // clone all necessary projects from git repository
                                GIT_NX_CORE_PROJECTS.each { project ->
                                    //dir("kitchen/${VAGRANT_NAME}/nx_core/${project}" ){
                                    dir("kitchen/${VAGRANT_NAME}/nx_core" ){
                                     //   retry(5){

                                                withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                                                output_git_commands = sh(returnStdout: true,script: """
                                                   if [[ ! -d ${project} ]];then
                                                        git  clone https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${BITBUCKET_SLUG}/${project}.git
                                                   else
                                                        cd  ${project}
                                                        git pull
                                                        cd ..
                                                   fi
                                                """)
                                                //git credentialsId: BITBUCKET_CRED_ID , url: "https://${BITBUCKET_USER_ID}@bitbucket.org/" + BITBUCKET_SLUG  + "/" + project + ".git" ,  changelog: false , poll: false
                                                }
                                       // }
                                            if ( output_git_commands =~ /Error|ERROR|Failed|Not found|No such file or directory/ ){
                                                error("Packer build failed")
                                            }else{
                                                currentBuild.result = 'SUCCESS'
                                            }
                                    }
                                }

                            }catch(Exception ex){
                                error("ERROR! Unable to clone repositories")
                            }

                           /* dir("kitchen/${VAGRANT_NAME}/nx_core") {
                                    sh (returnStdout: true, script: """rm -rf *@tmp""")
                            }*/
                 //   }
              } // end script
            } // end steps
        } // end stage

        /*
            Stage generates Chef role cookbook for kitchen
        */

        stage('Prepare test role'){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {

                script {

                    def sshd_output_command = ''
                    dir("kitchen/${VAGRANT_NAME}") {
                        sshd_output_command = sh (returnStdout: true, script: """[[ ! -d databags ]] && mkdir databags || exit 0""")
                    }

                    dir("kitchen/${VAGRANT_NAME}/nx_core") {

                        sshd_output_command = sh (returnStdout: true, script: """
                            set -x -v

                            [[ ! -d ${ROLE_PROJECT_NAME} ]] && mkdir  ${ROLE_PROJECT_NAME} || { rm -rf  ${ROLE_PROJECT_NAME} ; mkdir  ${ROLE_PROJECT_NAME} ; }
                            [[ ! -d ${ROLE_PROJECT_NAME}/recipes ]]  && mkdir ${ROLE_PROJECT_NAME}/recipes || exit 0
                            #[[ ! -d ${ROLE_PROJECT_NAME}/tests ]]    && mkdir ${ROLE_PROJECT_NAME}/tests || exit 0
                            [[ -f nx_core_data/recipes/data_site_chbs.rb  ]] && cp nx_core_data/recipes/data_site_chbs.rb  nx_core_data/recipes/data_site_kitchen.rb
                            [[ ! -f /tmp/${ROLE_PROJECT_NAME} ]] && { mkfifo /tmp/${ROLE_PROJECT_NAME} ; chmod 600 /tmp/${ROLE_PROJECT_NAME} ; } || exit 0

                            touch ${ROLE_PROJECT_NAME}/README.md

cat > ${ROLE_PROJECT_NAME}/Berksfile << EOF_berksfile
source 'https://10.147.137.206'
source 'https://supermarket.chef.io'
#source 'https://uch-supermarket.prd.nibr.novartis.net'
#source 'https://supermarket.chef.io'

metadata

group :core do
  def dependencies(path)
    full_entry = File.join(File.dirname(__FILE__), path)
    File.directory?(full_entry) and File.exist?(File.join(full_entry, 'metadata.rb'))
  end
  Dir.glob('../*').each do |path|
    dependencies path
    cookbook File.basename(path),
             :path => path
  end
end
EOF_berksfile


cat > ${ROLE_PROJECT_NAME}/metadata.rb << EOF_metadata
name             '${ROLE_PROJECT_NAME}'
maintainer       'NX'
maintainer_email 'NX'
license          'All rights reserved'
description      'Installs/Configures ${ROLE_PROJECT_NAME}, a role-cookbook for testing.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'nx_core'
EOF_metadata

cat > ${ROLE_PROJECT_NAME}/recipes/default.rb << EOF_recipe
#
# Cookbook Name:: ${ROLE_PROJECT_NAME}
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nx_core'
EOF_recipe


cat > ${ROLE_PROJECT_NAME}/.kitchen.yml <<EOF_kitchen
---
driver:
  name: vagrant
  customize:
    memory: 2048
    cableconnected1: 'on'

#driver_config:
#  http_proxy: '${HTTPS_PROXY}'
#  https_proxy: '${HTTPS_PROXY}'

provisioner:
  name: chef_zero
  client_rb:
     'ohai.disabled_plugins = [:Passwd] #':

  #data_bags_path: 'kitchen/databags/'
  root_path: '/var/tmp/kitchen'
  require_chef_omnibus: 12.17.44
  http_proxy:   ${HTTP_PROXY}
  https_proxy:  ${HTTPS_PROXY}


platforms:
  - name: ${VAGRANT_NAME}
    driver:
      box: ${VAGRANT_NAME}
      box_url: ${VAGRANT_IMAGE}

suites:
  - name: ${VAGRANT_NAME}
    run_list:
      - recipe[${ROLE_PROJECT_NAME}]
    attributes:
      site: 'kitchen'
      env: 'dev'
      project: '${ROLE_COOKBOOK}'
      category: 'default'

verifier:
  name: inspec
  inspec_tests:
       - tests/NIBR-custom
EOF_kitchen

            cat > ${ROLE_PROJECT_NAME}/nx_metadata.json <<EOF_json
{
  "update": {
    "approvers": [],
    "notifications": {
      "email": true,
      "email_addresses": [],
      "slack": true,
      "slack_channel": ""
    }
  }
}
EOF_json

#[[ -d /tmp/tests ]] && cp -r /tmp/tests  ${ROLE_PROJECT_NAME}
[[ -d  nx_compliance ]] && mv nx_compliance   ${ROLE_PROJECT_NAME}/tests
""")

                        if ( sshd_output_command =~ /Error|ERROR|Failed|Not found|No such file or directory/ ){
                            error("Packer build failed")
                        }else{

                            currentBuild.result = 'SUCCESS'
                            print "Kitchen prepare $currentBuild.result"

                        }

                    } //end  dir
                } // end script
            } // end steps
        } // end stage

        /*
            Stage creates a test VM
        */
        stage('Kitchen create'){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {
                // clone repo
                script {

                    try {

                        dir("kitchen/${VAGRANT_NAME}/nx_core") {

                            sh (returnStdout: false, script:
                                "#!/bin/bash\n" +
                                "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                "cd ${ROLE_PROJECT_NAME}\n" +
                                "/usr/bin/kitchen create --no-color"
                            )
                        }

                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen create ]")
                    }
                } // end script
            } // end steps
        } // end stage

        /*
            Stage applies nx_core cookbooks
        */
        stage('Kitchen converge'){
            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }
            steps {
                    // clone repo
                script {

                    try {
                        // sometimes converge fails
              

                            dir("kitchen/${VAGRANT_NAME}/nx_core") {
                                sh (returnStdout: false, script:
                                    "#!/bin/bash\n" +
                                    "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                    "cd ${ROLE_PROJECT_NAME}\n" +
                                    "/usr/bin/kitchen converge --no-color"
                                )
                            }
                        
                            currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        //error("ERROR! Failed stage [ Kitchen converge ]")
                        println("ERROR! Failed stage [ Kitchen converge ]")
                    }finally{

                            dir("kitchen/${VAGRANT_NAME}/nx_core") {
                                sh (returnStdout: false, script:
                                    "#!/bin/bash\n" +
                                    "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                    "cd ${ROLE_PROJECT_NAME}\n" +
                                    "/usr/bin/kitchen converge --no-color"
                                )
                            }
                    }
                } // end script
            } // end steps
        } // end stage
        /*
            Stage creates a test VM
        */
/*
        stage('Kitchen reboot'){
            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }
            steps {
                    // clone repo
                script {

                    try {

                        dir("kitchen/${VAGRANT_NAME}/nx_core") {

                            def sshd_output_command = sh (returnStdout: true, script:
                                 sh (returnStdout: true, script:
                                    "#!/bin/bash\n" +
                                    "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                    "cd ${ROLE_PROJECT_NAME}\n" +
                                    "kitchen exec  ${VAGRANT_NAME} -c \"sudo reboot;exit\" 2>/dev/null"
                                    )
                                sleep 60
                            }

                        if ( sshd_output_command =~ /Error|ERROR|Failed|Not found|No such file or directory/ ){
                            error("Packer build failed")
                        }else{
                            currentBuild.result = 'SUCCESS'
                        }

                        }catch(Exception ex){
                            error("ERROR! Failed stage [ Kitchen create ]")
                    }
                }
            }
*/
        /*
            Stage verifies the VM against security baseline
        */
        stage('Kitchen verify'){
            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }
            steps {
                    // clone repo
                script {

                    try {

                        dir("kitchen/${VAGRANT_NAME}/nx_core") {

                            sh (returnStdout: false, script:
                                "#!/bin/bash\n" +
                                "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                "cd ${ROLE_PROJECT_NAME}\n" //+
                                // "/usr/bin/kitchen verify --no-color"
                            )

                        }

                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen verify ]")
                    }
                } // end script
            } // end steps
        } // end stage


        /*
            Stage removes VM
        */

        stage('Kitchen destroy'){
            when {
                    expression { currentBuild.result == 'SUCCESS' ||  currentBuild.result == 'FAILED ' }
            }
            steps {
                    // clone repo
                script {

                    try {

                        dir("kitchen/${VAGRANT_NAME}/nx_core") {

                            sh (returnStdout: false, script:
                                "#!/bin/bash\n" +
                                "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                "cd ${ROLE_PROJECT_NAME}\n" +
                                "/usr/bin/kitchen destroy --no-color"
                            )

                        }
                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen create ]")
                    }
                } // end script
            } // end steps
        } // end stage


        /*
            Stage uploads original image to the repository
        */

        stage('Upload to repo'){
            when {
                    expression { UPLOAD_TO_REPO == 'ENABLED'}
            }
            steps {

                    script {
                        dir("kitchen") {
                                sh (returnStdout: true, script: """echo UPLOAD""")
                        }
                } // end script
            } // end steps
        } // end stage

        /*
            Stage generates and publishes release notes on Confluence
        */

        stage('Publish release notes'){
            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }
            steps {
                    // clone repo
                script {
                    def PUBLISH_RELEASE_NOTES = 'no'
                    def sshd_output_command  = ''
                    
                    try {
                        // We setup default timeout prompt for user input we run with preset variables otherwise
                        timeout(time: 180, unit: 'SECONDS'){

                            //user can choose on input which build combination to run
                            def userInput = input message: 'Do you want to publish release notes?', submitterParameter: 'submit_by', ok: 'Continue',
                                parameters: [
                                                choice(choices:ENV_CONFLUENCE_ANSWER.join('\n') , description: 'Do you want to publish Release Notes to confluence?', name: 'publish_release_notes')
                                            ]

                                PUBLISH_RELEASE_NOTES = userInput['publish_release_notes']

                                // try to publish release information on Confluence
                                if ( PUBLISH_RELEASE_NOTES == 'yes' ){

                                    if ( fileExists("publish_release.sh") ){
                                        sshd_output_command =  sh (returnStdout: true, script: "./publish_release.sh -n ${VAGRANT_NAME} -C 125118623")
                                    }else{
                                        error("ERROR: Unable to find [ ${env.CUSTOM_WORKSPACE}/publish_release.sh ]")
                                    }
                                }

                            } //end timeout

                            print "DEBUG: ${sshd_output_command}"
                      

                    }catch(Exception e){
                        //Do nothing
                        //error("ERROR! Failed to publish release notes for [${VAGRANT_NAME}]")
                    }finally{

                        // Default parameters to run the pipeline with
                        if ( PUBLISH_RELEASE_NOTES == null  ) { PUBLISH_RELEASE_NOTES = 'no' }

                        // we record the name of the Jenkins user who submits this request, and the time
                        SUBMIT_BY = 'Autorun'
                        SUBMIT_TIME = new Date().toString()
                        currentBuild.result = 'SUCCESS' 

                    } // end finally
                } // end script
            } // end steps
        } // end stage

        /*
            Stage cleans up
        */

        stage('Cleanup'){
            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }
            steps {
                    // clone repo
                script {

                    try {
                        
                       if (fileExists("${env.CUSTOM_WORKSPACE}/kitchen/${VAGRANT_NAME}/nx_core/.kitchen.yml")){
                            dir("kitchen/${VAGRANT_NAME}/nx_core") {
                                sh (returnStdout: false, script:
                                    "#!/bin/bash\n" +
                                    "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                    "cd ${ROLE_PROJECT_NAME}\n" +
                                    "/usr/bin/kitchen destroy --no-color"
                                )
                           }
                       }else{
                           println "WARNING: Unable to find [ ${env.CUSTOM_WORKSPACE}/${VAGRANT_NAME}/nx_core/.kitchen.yml ]"
                           println "         Skipping Kitchen destroy"
                       }


                        dir("kitchen/${VAGRANT_NAME}") {
                            println "INFO: Removing directory  [ ${env.CUSTOM_WORKSPACE}/kitchen/${VAGRANT_NAME} ]"
                            sh (returnStdout: false, script:
                                "#!/bin/bash\n"  +
                                "[[ -d ${env.CUSTOM_WORKSPACE}/kitchen/${VAGRANT_NAME} ]] && rm -rf ${env.CUSTOM_WORKSPACE}/kitchen/${VAGRANT_NAME} || exit 0"
                            )
                        }
                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen create ]")
                    }
              } // end script
            } // end steps
        } // end stage

   } //end STAGES


        /*
            Post actions
        */
        post('Post actions') {
        /*always {
            deleteDir()
        }*/

        success{
           send_email_notification(INFO_EMAIL,'Successful')
        }

        unstable{
           send_email_notification(INFO_EMAIL,'Unstable')
        }

        failure {
            
            send_email_notification(INFO_EMAIL,'Failed')
            script{
                try{
                    if (fileExists("${env.CUSTOM_WORKSPACE}/kitchen/${VAGRANT_NAME}/nx_core/.kitchen.yml")){
                        dir("kitchen/${VAGRANT_NAME}/nx_core") {
                            sh (returnStdout: false, script:
                                "#!/bin/bash\n" +
                                "export ${HTTP_PROXY}  ${HTTPS_PROXY} \n" +
                                "cd ${ROLE_PROJECT_NAME}\n" +
                                "/usr/bin/kitchen destroy --no-color"
                                )
                        }
                    }

                    dir("kitchen"){
                        sh (returnStdout: false, script:
                            "#!/bin/bash\n" //+
                            // "rm -rf ${VAGRANT_NAME}*"
                        )
                    }
                    
                }catch(Exception ex){
                    error("ERROR! Failed post action [ Kitchen destroy ]")
                }
            }
        }

    }
} //end  pipeline
