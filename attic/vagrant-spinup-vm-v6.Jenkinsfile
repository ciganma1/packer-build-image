env.BUILD_WORKING_DIR = 'packer_build'
env.PACKER_DIR = 'packer'
BITBUCKET_CRED_ID = '4b6e07f0-c030-4c84-a184-5b7a66ebc7b2'
OS_BUILD_TYPE = 'VAGRANT'

env.HTTP_PROXY="http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"
env.HTTPS_PROXY="https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"

ROLE_COOKBOOK = 'nxcore_kitchen_run'

GIT_NX_CORE_PROJECTS = ["nx_concat","nx_core","nx_core_yum","nx_core_ulimits","nx_core_sysctl","nx_core_sudo","nx_core_ssh","nx_core_snmp","nx_core_rsyslog","nx_core_resolv","nx_core_patching","nx_core_ntp","nx_core_nscd","nx_core_network","nx_core_mountfs","nx_core_mail","nx_core_local_accounts","nx_core_kdump","nx_core_fusioninventory","nx_core_data","nx_core_boot","nx_core_backup","nx_core_auth","nx_core_auditd","nx_core_sysctl","nx_core_ulimits","nx_core_yum"]

BITBUCKET_SLUG = 'novartisnibr'
pipeline {

        //run only on a dedicated build host
        agent { node { label 'packer' } }

        parameters {
                string(
                        name: "VAGRANT_IMAGE",
                        description: "Vagrant VM image name")
                string(
                        name: "VAGRANT_NAME",
                        description: "Vagrant VM  hostname")
        }

        stages {
          stage('Preparation') {
                steps {
                        script {
                        VAGRANT_IMAGE  = params.VAGRANT_IMAGE
                        VAGRANT_NAME   = params.VAGRANT_NAME
                        ROLE_PROJECT_NAME = 'role_nxcore_kitchen_run'
                        }
                }
        }
        
        /*
            Stage clones nx_core repos
        */
        stage('Clone repository'){
            steps {
                    // clone repo
                    script {
                        GIT_NX_CORE_PROJECTS.each { project ->
                            dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core/${project}" ){
                                git credentialsId: BITBUCKET_CRED_ID , url: "https://ciganma1@bitbucket.org/" + BITBUCKET_SLUG  + "/" + project + ".git" ,  changelog: false , poll: false
                            }
                        }
                        dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                            sh (returnStdout: true, script: """rm -rf *@tmp""")
                        }
                }
            }
        }
   /*
        Stage generates Chef role cookbook for kitchen
    */

        stage('Prepare test role'){
            steps {
                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen") {
                    sh (returnStdout: true, script: """[[ ! -d databags ]] && mkdir databags || exit 0""")
                }

                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                    script {
                        sh (returnStdout: true, script: """
                            set -x -v 
                            [[ ! -d ${ROLE_PROJECT_NAME} ]] && mkdir  ${ROLE_PROJECT_NAME} || exit 0
                            [[ ! -d ${ROLE_PROJECT_NAME}/recipes ]]  && mkdir ${ROLE_PROJECT_NAME}/recipes || exit 0
                            [[ ! -d ${ROLE_PROJECT_NAME}/tests ]]    && mkdir ${ROLE_PROJECT_NAME}/tests || exit 0 
                            [[ -f nx_core_data/recipes/data_site_chbs.rb  ]] && cp nx_core_data/recipes/data_site_chbs.rb  nx_core_data/recipes/data_site_kitchen.rb 
                            [[ ! -f /tmp/${ROLE_PROJECT_NAME} ]] && { mkfifo /tmp/${ROLE_PROJECT_NAME} ; chmod 600 /tmp/${ROLE_PROJECT_NAME} ; } || exit 0

                            touch ${ROLE_PROJECT_NAME}/README.md

cat > ${ROLE_PROJECT_NAME}/Berksfile << EOF_berksfile
source 'https://10.147.137.206'
source 'https://supermarket.chef.io'
#source 'https://uch-supermarket.prd.nibr.novartis.net'
#source 'https://supermarket.chef.io'

metadata

group :core do
  def dependencies(path)
    full_entry = File.join(File.dirname(__FILE__), path)
    File.directory?(full_entry) and File.exist?(File.join(full_entry, 'metadata.rb'))
  end
  Dir.glob('../*').each do |path|
    dependencies path
    cookbook File.basename(path),
             :path => path
  end
end
EOF_berksfile


cat > ${ROLE_PROJECT_NAME}/metadata.rb << EOF_metadata
name             '${ROLE_PROJECT_NAME}'
maintainer       'NX'
maintainer_email 'NX'
license          'All rights reserved'
description      'Installs/Configures ${ROLE_PROJECT_NAME}, a role-cookbook for testing.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends           'nx_core'
EOF_metadata

cat > ${ROLE_PROJECT_NAME}/recipes/default.rb << EOF_recipe
#
# Cookbook Name:: ${ROLE_PROJECT_NAME}
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nx_core'
EOF_recipe


cat > ${ROLE_PROJECT_NAME}/.kitchen.yml <<EOF_kitchen
---
driver:
  name: vagrant
  customize:
    memory: 2048
    cableconnected1: 'on'

driver_config:
  http_proxy: '${HTTPS_PROXY}'
  https_proxy: '${HTTPS_PROXY}'

provisioner:
  name: chef_zero
  client_rb:
     'ohai.disabled_plugins = [:Passwd] #':

  data_bags_path: '${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/databags/'
  root_path: '/var/tmp/kitchen'
  require_chef_omnibus: 12.17.44

platforms:
  - name: ${VAGRANT_NAME}
    driver:
      box: ${VAGRANT_NAME}
      box_url: ${VAGRANT_IMAGE}

suites:
  - name: default
    run_list:
      - recipe[${ROLE_PROJECT_NAME}]
    attributes:
      site: 'kitchen'
      env: 'dev'
      project: '${ROLE_COOKBOOK}'
      category: 'default'

verifier:
  name: inspec
  inspec_tests:
       - tests/NIBR-custom
EOF_kitchen

            cat > ${ROLE_PROJECT_NAME}/nx_metadata.json <<EOF_json
{
  "update": {
    "approvers": [],
    "notifications": {
      "email": true,
      "email_addresses": [],
      "slack": true,
      "slack_channel": ""
    }
  }
}
EOF_json


              """)
                        /*       dir("${ROLE_PROJECT_NAME}/tests"){
                                        git credentialsId: BITBUCKET_CRED_ID , url: "https://ciganma1@bitbucket.org/" + BITBUCKET_SLUG  + "/" + "nx_compliance.git" ,  changelog: false , poll: false
                                }*/
                        }
                }
        }

    }// end Prepare kitchen
        /*
            Stage creates a test VM
        */
        stage('Kitchen create'){
            steps {
                    // clone repo
                    script {
                                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                                 sh (returnStdout: true, script: """
                                    export ${HTTP_PROXY}  ${HTTPS_PROXY} 
                                    cd ${ROLE_PROJECT_NAME}
                                    /usr/bin/kitchen create --no-color""")
                                }
                        }
                }
        }
        /*
            Stage applies nx_core cookbooks
        */
        stage('Kitchen converge'){
            steps {
                    // clone repo
                    script {
                                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                                 sh (returnStdout: true, script: """
                                    export ${HTTP_PROXY}  ${HTTPS_PROXY} 
                                    cd ${ROLE_PROJECT_NAME}
                                    /usr/bin/kitchen converge --no-color""")
                                }
                        }
                }
        }
        /*
            Stage verifies the VM against security baseline
        */
        stage('Kitchen verify '){
            steps {
                    // clone repo
                    script {
                                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                                 sh (script: """
                                    export ${HTTP_PROXY}  ${HTTPS_PROXY} 
                                    cd ${ROLE_PROJECT_NAME}
                                    /usr/bin/kitchen verify --no-color""")
                                }
                                currentBuild.result = 'SUCCESS'
                        }
                }
        }
        /*
            Stage removes VM
        */
        stage('Kitchen destroy'){
                when {
                        expression {
                            currentBuild.result == 'SUCCESS'
                        }
                }
                steps {
                    // clone repo
                script {
                        dir("${BUILD_WORKING_DIR}/${PACKER_DIR}/kitchen/nx_core") {
                                 sh (returnStdout: true, script: """
                                    export ${HTTP_PROXY}  ${HTTPS_PROXY} 
                                    cd ${ROLE_PROJECT_NAME}
                                    /usr/bin/kitchen destroy --no-color""")
                                }
                        }
                }
        }
        /*
            Stage uploads original image to the repository
        */
        stage('Upload to repo'){
                when {
                        expression {
                                currentBuild.result == 'SUCCESS'
                        }
                }
                steps {
                        // clone repo
                        script {
                                dir("${BUILD_WORKING_DIR}/${PACKER_DIR}") {
                                        sh (returnStdout: true, script: """rsync &>/dev/null""")
                                }
                        }
                }
        }
        /*
            Stage generates and publishes release notes on Confluence
        */
        stage('Publish release notes'){
            when {
                    expression {
                            currentBuild.result == 'SUCCESS'
                    }
            }
            steps {
                    // clone repo
                    script {
                            dir("${BUILD_WORKING_DIR}/${PACKER_DIR}") {
                                sh (returnStdout: true, script: """echo""")
                            }
                    }
            }
    }

        
            

    } //end Stages
} //end  pipeline  
