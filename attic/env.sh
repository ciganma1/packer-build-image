#!/bin/bash --
### Useful functions and environmental setup for creating/managing AMIs


## Logging function
# Just to screen for now
function log () {
  echo "$(date "+%F %T") $1" | tee -a "${build_log_dir}/env.log"
}


## Output variables and their values in a runnable format, for debugging
function echo_var () {
  declare -p $@
  #for var in $@; do
  #  echo "export $var=\"${!var}\""
  #done
}


## Get the credentials necessary to use the AWS CLI
function get_aws_credentials () {
  # Mandatory parameters: turbot_user, turbot_account
  # Also requires proxy to be correctly set
  # To-do: Check number of and validity of args
  # To-do: Method of pulling in user's Turbot credentials
  local turbot_user=$1
  local turbot_account=$2

  # If the credentials are already set, they won't expire for at least 4 hours, and they are still valid in AWS, then just keep using them
  # aws sts get-caller-identity is a good way to test whether creds are valid, without actually requiring any particular permissions
  if [[ ${aws_credentials_expire_time} && ${AWS_ACCESS_KEY_ID} && ${AWS_SECRET_ACCESS_KEY} && ${AWS_SESSION_TOKEN} ]] && [[ $(date --date="${aws_credentials_expire_time}" +"%s") -ge $(date --date="+4 hours" +"%s") ]] && aws sts get-caller-identity &> /dev/null ; then return 0; fi

  # Clear previous credentials
  unset AWS_ACCESS_KEY AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SECRET_KEY AWS_SESSION_TOKEN aws_credentials_expire_time

  # Get temporary session credentials for the account
  # Per AJ, having multiple sets around shouldn't be an issue, so we don't need to cache and re-use these
  raw_turbot_response="$(curl --request POST --insecure --silent --header "Content-Type: application/json" --url https://turbot-app.prd.nibr.novartis.net/api/v3/accounts/${turbot_account}/users/${turbot_user}/awsCredentials --user ${turbot_access_key}:${turbot_secret_key})"
  read -r AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN aws_credentials_expire_time <<< "$(echo "$raw_turbot_response" | jq -r '[.accessKeyId, ."$secretAccessKey", ."$sessionToken", .expireTime] | @tsv')"
  export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN aws_credentials_expire_time
  # To-do: Catch errors, ensure necessary privileges

}


## Wait for one or more AMIs to be available
function wait_for_amis () {
  local usage="Usage: ${FUNCNAME[0]} {number_of_tries} {region} {ami id 1} [{ami id 2} {ami id 3} ...]
Waits up to number_of_tries x 10 minutes for the specified AMIs to become available"

  [ "$1" ] || { log "ERROR: Number of tries not specified in call to ${FUNCNAME[0]}"; log "$usage"; return 1; }
  local num_tries=$1
  shift
  [ "$1" ] || { log "ERROR: Region not specified in call to ${FUNCNAME[0]}"; log "$usage"; return 1; }
  local region=$1
  shift
  [ "$1" ] || { log "ERROR: No ami ids specified in call to ${FUNCNAME[0]}"; log "$usage"; return 1; }
  local ami_ids="$@"

  # Wait for the AMIs to be available
  log "Waiting for AMI(s) ${ami_ids} in ${region} to be available"
  # Have to do this C style because sequence expressions expand before variables do
  for ((try=1; try<=num_tries; try++)); do
    [ ${try} == 1 ] || log "AMI(s) ${ami_ids} in ${region} still unavailable after $((try * 10)) minutes; waiting longer"
    aws ec2 wait image-available --region "${region}" --image-ids ${ami_ids} && break
  done
  [ $? == 0 ] && { log "AMI(s) ${ami_ids} in ${region} confirmed to be available"; return 0; } || { log "ERROR: AMI(s) ${ami_ids} in ${region} still unavailable after $((try * 10)) minutes"; return 1; }
}


## Delete AMIs
function delete_amis () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ami id 1} [{ami id 2} {ami id 3} ...]
Deletes the specified AMIs and their associated snapshots"

  [ "$1" ] || { echo "ERROR: Region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  [ "$1" ] || { echo "ERROR: No ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Loop through all the provided AMI IDs
  for ami_id in "$@"; do
    # In case the AMI is newly-created, makre sure it is actually available
    # Otheerwise, the snapshot deletion can fail
     wait_for_amis 3 "${region}" "${ami_id}" || continue

    # First grab the list of snapshots, because they aren't deleted automatically and can't be listed after the AMI is deregistered. Seriously, Amazon?
    local snapshots_to_delete=$(aws ec2 describe-images --region "${region}" --image-ids ${ami_id} | jq -r '.Images[] | .BlockDeviceMappings[] | .Ebs.SnapshotId')
    [ ${snapshots_to_delete} ] && log "Found snapshot(s) connected to ${ami_id} in ${region}: ${snapshots_to_delete}" || log "ERROR: Could not find snapshots attached to ${region}:${ami_id}"

    # Then deregister the AMI
    aws ec2 deregister-image --region "${region}" --image-id "${ami_id}" && log "${ami_id} in ${region} deregistered" || { log "ERROR: Problem deregistering ${ami_id} in ${region}"; continue; }

    # And finally delete the snapshot(s)
    for snapshot in ${snapshots_to_delete}; do
      aws ec2 delete-snapshot --region "${region}" --snapshot-id "${snapshot}" && log "Snapshot ${snapshot} (previously attached to ${ami_id}) in ${region} deleted" || log "ERROR: Problem deleting snapshot ${snapshot} (previously attached to ${ami_id}) in ${region}"
    done

  done
}


## Add AMI to CID
function add_amis_to_cid () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ami id 1} [{ami id 2} {ami id 3} ...]
Adds the specified AMIs to the list in CID"

  [ "$1" ] || { echo "ERROR: Region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  [[ "${region}" == "us-east-1" ]] || { echo "ERROR: ${region} is not a valid region in this context. Currently only the us-east-1 region is supported by CID."; echo "$usage"; return 1; }
  shift
  [ "$1" ] || { echo "ERROR: No ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Loop through provided AMI IDs
  for ami_id in "$@"; do

    # Pull the ami name
    local ami_metadata="$(aws ec2 describe-images --region ${region} --image-ids ${ami_id})"
    local ami_name="$(echo "${ami_metadata}" | jq -r '.Images[].Name')"
    local ami_name_sans_color_text="$(echo ${ami_name} | sed -r 's/^(.*)\(.*\)(.*)$/\1\2/')"

    # Add the AMI ID to CID
    local result_code_1="$(http_proxy="" https_proxy="" curl -sS -X PUT -H "Content-Type: application/json" -H "REMOTE_USER: jenkins" http://cid-backend.prd.nibr.novartis.net:5080/services/cid/v2.0/filter/ec2/image -d "[\"${ami_id}\"]" | jq -r '.message')"
    [[ "${result_code_1}" == "Updated" ]] && log "Created entry for AMI ${region}:${ami_id} in CID" || { log "ERROR: Problem creating entry for AMI ${region}:${ami_id} in CID, result code: ${result_code_1}"; continue; }

    # Add some AMI metadata to CID
    local result_code_2="$(http_proxy="" https_proxy="" curl -sS -X POST -H "Content-Type: application/json" -H "REMOTE_USER: jenkins" http://cid-backend.prd.nibr.novartis.net:5080/services/cid/v2.0/image -d "
      {
        \"provider\": \"ec2\",
        \"name\": \"${ami_name_sans_color_text}\",
        \"remote_user\": \"ec2-user\",
        \"bootstrap_template\": \"omnibus\",
        \"id\": \"${ami_id}\"
      }" | jq -r '.message')"
    [[ "${result_code_2}" == "Created" ]] && log "Added metadata for AMI ${region}:${ami_id} in CID" || { log "ERROR: Problem creating entry for AMI ${region}:${ami_id} in CID, result code: ${result_code_2}"; continue; }

  done
}


## Set AMI launch permissions
function set_ami_permissions () {
  local usage="Usage: ${FUNCNAME[0]} {region} {account list} {ami id 1} [{ami id 2} {ami id 3} ...]
Adds launch permissions to the specified AMIs
The account list should be surrounded by quotes, or use the special value 'all' for all NIBR AWS accounts"
  [ "$1" ] && [ "$2" ] && [ "$3" ] || { echo "ERROR: invalid usage for ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  local aws_account_list="$1"
  shift

  # If "all" is specified, pull the current list of AWS accounts from Turbot, and pad with zeroes if fewer than 12 characters
  # Turbot omits leading zeroes, but AWS requires them
  if [ "${aws_account_list}" == "all" ]; then
    # Grab Turbot credentials
    # . "${build_base_dir}/ssturbot.cred"
    aws_account_list=$(curl --insecure --silent --request GET --header "Content-Type: application/json" --url https://turbot-app.prd.nibr.novartis.net/api/v2/accounts --user ${turbot_access_key}:${turbot_secret_key} | jq -r '.items[] | .aws.accountId' | awk '{printf("%012d\n",$1)}')
    # To-do: make sure we actually got the account list successfully
    # Add on the static list of Turbot dev accounts
    aws_account_list+="${turbot_dev_accounts}"
  fi

  for ami_id in "$@"; do

    # Make sure AMI is available, since if it's new there can be a delay, even when copied in the same region
    wait_for_amis 3 "${region}" "${ami_id}" || continue

    # Add launch permissions
    log "Adding launch permissions to ${region}:${ami_id} - ${aws_account_list//$'\n'/ }"
    aws ec2 modify-image-attribute --region "${region}" --image-id "${ami_id}" --attribute launchPermission --operation-type add --user-ids ${aws_account_list} && log "Added launch permissions to ${region}:${ami_id}" || log "ERROR: Problem adding launch permissions to ${region}:${ami_id}"

  done
}


## Remove all launch permissions from AMIs
function remove_ami_permissions () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ami id 1} [{ami id 2} {ami id 3} ...]
Removes all launch permissions from the specified AMIs"
  [ "$1" ] || { echo "ERROR: region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  [ "$1" ] || { echo "ERROR: no ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Loop through provided AMI IDs
  for ami_id in "$@"; do

    # Retrieve the current launch permissions from the AMI
    local aws_account_list=$(aws ec2 describe-image-attribute --region "${region}" --attribute launchPermission --image-id "${ami_id}" | jq -r '.LaunchPermissions[].UserId')

    # To-do: make sure the account list isn't empty, check for and remove groups

    # Remove launch permissions
    echo "Removing the following launch permissions from ${ami_id}:"
    echo ${aws_account_list}
    aws ec2 modify-image-attribute --region "${region}" --image-id "${ami_id}" --attribute launchPermission --operation-type remove --user-ids ${aws_account_list}

  done
}


## Set AMI tags for all NIBR AWS accounts
function set_ami_tags () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ami_id_1} [ami_id_2 ami_id_3 ...]"
  [ "$1" ] || { echo "ERROR: region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  [ "$1" ] || { echo "ERROR: no ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  for ami_id in "$@"; do

    # Grab Turbot credentials
    # . "${build_base_dir}/ssturbot.cred"

    # Pull the current list of Turbot accounts
    local turbot_account_list=$(curl --insecure --silent --request GET --header "Content-Type: application/json" --url https://turbot-app.prd.nibr.novartis.net/api/v2/accounts/ --user ${turbot_access_key}:${turbot_secret_key} | jq -r '.items[].id')
    # To-do: make sure we actually got the account list successfully

    #curl -X POST -k -u ${ACCESS_KEY}:${SECRET_KEY} https://turbot-app.prd.nibr.novartis.net/api/v1/accounts/aan/aws/superUserCredentials | python -m json.tool > /Users/willijos/projects/tbot-special-bits/temp-superuser.json

    # Pull data from AMI
    local ami_metadata="$(aws ec2 describe-images --region "${region}" --image-ids ${ami_id})"
    local ami_name="$(echo "${ami_metadata}" | jq -r '.Images[].Name')"
    local ami_description="$(echo "${ami_metadata}" | jq -r '.Images[].Description')"
    [[ ${ami_description} ]] && log "Current description of ${region}:${ami_id} is \"${ami_description}\"" || { log "ERROR: Could not retrieve image description for ${region}:${ami_id}, or it is blank"; continue; }
    # Check whether specified AMI is set as latest
    [[ ${ami_description} =~ \ latest$  ]] && { local is_latest="true"; log "AMI ${region}:${ami_id} is marked as latest";  } || { local is_latest="false"; log "AMI ${region}:${ami_id} is not marked as latest"; }
    local ami_color_text="$(echo $ami_name | sed -r 's/^.*\((.*)\).*$/\1/')"
    local ami_name_sans_color_text="$(echo ${ami_name} | sed -r 's/^(.*)\(.*\)(.*)$/\1\2/')"

    local ami_company ami_basic_type ami_label ami_os ami_major_version ami_arch ami_date_and_timestamp
    read ami_company ami_basic_type ami_label ami_os ami_major_version ami_arch ami_date_and_timestamp <<< "${ami_name_sans_color_text}"

    # Impossible to do this currently, as I don't have access. Hopefully that will be corrected soon.
    aws ec2 create-tags --region "${region}" \
      --tags \
      Key=Name,Value="${ami_name}" \
      Key="nibr:technicalowner",Value="platform_eng_linux_ORG@dl.mgd.novartis.com" \
      Key="nibr:os",Value="${ami_os}${ami_major_version}" \
      Key="nibr:latest",Value="${is_latest}" \
      --resources "${ami_id}" \
      && log "Added tags to ${ami_id} in ${region}" || { log "ERROR: Problem adding tags to ${new_ami_id} in ${region}"; continue; }
    aws ec2 describe-tags --region "${region}" --filters Name=resource-id,Values="${ami_id}"

  done
}


## Add the " latest" piece of the description to AMIs, and remove it from any matching old AMIs
function change_latest_amis () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ami_id_1} [ami_id_2 ami_id_3 ...]
Label AMIs as 'latest' in the description, and unlabel any other AMIs of the same type"
  [ "$1" ] || { echo "ERROR: region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  [ "$1" ] || { echo "ERROR: no ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  for ami_id in "$@"; do

    # Get description of specified image
    local initial_image_description="$(aws ec2 describe-images --region "${region}" --image-ids ${ami_id} | jq -r '.Images[].Description')"
    [[ ${initial_image_description} ]] && log "Current description of ${region}:${ami_id} is \"${initial_image_description}\"" || { log "ERROR: Could not retrieve image description for ${region}:${ami_id}, or it is blank"; continue; }

    # Check whether specified AMI is already latest
    [[ ${initial_image_description} =~ \ latest$  ]] && { log "ERROR: AMI ${region}:${ami_id} is already marked as latest"; continue; }

    # Get image flavor (description sans date) from AMI description
    local image_flavor="$(echo "${initial_image_description}" | sed -r 's/^(.*) \(.*\) [0-9]{8}-[0-9]+( latest)?/\1/')"
    [[ "${image_flavor}" == "${initial_image_description}" ]] && { log "ERROR: Could not determine image type of ${region}:${ami_id} from its description: \"${initial_image_description}\""; continue; } || log "${region}:${ami_id} is image type \"${image_flavor}\""
    #echo "image_flavor: ${image_flavor}" # Debug code

    # Find any existing latest AMIs
    local existing_latest_images=$(aws ec2 describe-images --region ${region} --filters Name=description,Values="${image_flavor} * latest" | jq -r '.Images[].ImageId')
    #echo "${image_flavor} * latest # Debug code"
    #aws ec2 describe-images --region "${region}" --filters Name=description,Values="${image_flavor} * latest" | jq -r '.Images[].ImageId' # Debug code
    #aws ec2 describe-images --region "${region}" --filters Name=description,Values="${image_flavor} * latest" # Debug code
    #echo "existing_latest_images: ${existing_latest_images}" # Debug code

    # Add the latest label to the specified AMIs
    new_image_description="${initial_image_description} latest"
    aws ec2 modify-image-attribute --region "${region}" --description "${initial_image_description} latest" --image-id "${ami_id}" && log "Marked ${region}:${ami_id} as latest: \"${new_image_description}\"" || { log "ERROR: Problem adding \"latest\" to description of ${region}:${ami_id}"; continue; }
    # Wait for the change to take effect
    until [[ "$(aws ec2 describe-images --region "${region}" --image-ids ${ami_id} | jq -r '.Images[].Description')" == "${new_image_description}" ]]; do sleep 15s; echo "Waiting for new description of ${region}:${ami_id} to take effect"; done
    #aws ec2 describe-images --region "${region}" --image-ids ${ami_id} | jq -r '.Images[].Description' # Debug code

    # Remove latest from previous AMIs, if applicable
    if [[ ${existing_latest_images} ]]; then
      for existing_latest_image in ${existing_latest_images}; do
        local current_description="$(aws ec2 describe-images --region "${region}" --image-ids ${existing_latest_image} | jq -r '.Images[].Description')"
        #echo "current_description: ${current_description}" # Debug code
        local new_description="${current_description% latest}"
        #echo "new_description: ${new_description}" # Debug code
        aws ec2 modify-image-attribute --region "${region}" --description "${new_description}" --image-id ${existing_latest_image} && log "Removed \"latest\" from description of ${region}:${existing_latest_image}. \"${current_description}\" -> \"${new_description}\"" || { log "ERROR: Problem removing \"latest\" from description of ${region}:${existing_latest_image}."; continue; }
        until [[ "$(aws ec2 describe-images --region "${region}" --image-ids ${existing_latest_image} | jq -r '.Images[].Description')" == "${new_description}" ]]; do sleep 15s; echo "Waiting for new description of ${region}:${existing_latest_image} to take effect"; done
      done
    else
      log "No prior \"${image_flavor}\" AMIs were marked as latest"
    fi

  done
}


## Distribute AMIs to all regions, set launch permissions, and tag
# Needs more output?
function distribute_amis () {
  local usage="Usage: ${FUNCNAME[0]} {source_region} {ami_id_1} [ami_id_2 ami_id_3 ...]
Distributes the specified AMIs to all regions and sets launch permissions"

  # Get AWS credentials from Turbot
  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  # Make sure we have appropriate input
  [ "$1" ] || { echo "ERROR: source region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local source_region="$1"
  shift
  [ "$1" ] || { echo "ERROR: no ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Loop through all the provided AMIs, running the process in parallel
  # Turn off monitor mode for jobs, to avoid excessive output
  set +m
  for ami_id in "$@"; do
    {
      # Pull data from original AMI
      local ami_metadata="$(aws ec2 describe-images --region "${source_region}" --image-ids ${ami_id})"
      local ami_name="$(echo "${ami_metadata}" | jq -r '.Images[].Name')"
      local ami_description="$(echo "${ami_metadata}" | jq -r '.Images[].Description')"

      # Derive image flavor, needed for infomatics/collab permissions
      local image_flavor="$(echo "${ami_description}" | sed -r 's/^(.*) \(.*\) [0-9]{8}-[0-9]+( latest)?/\1/')"
      [[ "${image_flavor}" == "${ami_description}" ]] && { log "ERROR: Could not determine image type of ${destination_region}:${new_ami_id} from its description: \"${ami_description}\""; continue; } || log "${destination_region}:${new_ami_id} is image type \"${image_flavor}\""

      # Loop through each region
      # Should work in parallel, once we get to the point of adding a third region
      for destination_region in ${nibr_aws_regions}; do
        {
          # Skip the copy on the source region; just do permissions/tags
          if [[ "${destination_region}" == "${source_region}" ]]; then
            new_ami_id="${ami_id}"

          else
            # Copy the AMI to the destination region
            # Exclude the "latest" label, if present on the source
            local amicopyoutput="$(aws ec2 copy-image --source-image-id "${ami_id}" --source-region "${source_region}" --region "${destination_region}" --name "${ami_name}" --description "${ami_description% latest}")" || { log "ERROR: Problem copying ${source_region}:${ami_id} to ${destination_region}"; continue; }

            # Parse out the id of the new AMI
            local new_ami_id="$(echo $amicopyoutput | jq -r '.ImageId')"
            log "Copied ${source_region}:${ami_id} --> ${destination_region}:${new_ami_id}"
          fi

          # Wait for the AMI to be available
          # Not strictly necessary before setting permissions/tags, but it's better not to give the impression that everything was successful until we confirm availability
          wait_for_amis 3 "${destination_region}" "${new_ami_id}" || continue

          # Add launch permissions and create tags
          case "${ami_name}" in
            "nibr baseos nx_core centos 6 x86_64"* | "nibr baseos nx_core centos 7 x86_64"*)
              set_ami_permissions "${destination_region}" "all" "${new_ami_id}"
              set_ami_tags "${destination_region}" "${new_ami_id}"
            ;;
            "nibr stack nx_core_informatics "*" x86_64"* | "nibr stack nx_core_informatics_collaboration "*" x86_64"*)
              # 865206837818 is the Informatics account, 221763934359 is the Cloud management account
              local existing_latest_images=$(aws ec2 describe-images --region ${destination_region} --filters Name=description,Values="${image_flavor} * latest" | jq -r '.Images[].ImageId')
              local latest_ami_permissions=$(aws ec2 describe-image-attribute --region ${destination_region} --attribute launchPermission --image-id ${existing_latest_images} | jq -r '.LaunchPermissions[].UserId' | tr '\n' ' ')
              set_ami_permissions "${destination_region}" "${latest_ami_permissions}" "${new_ami_id}"
              set_ami_tags "${destination_region}" "${new_ami_id}"
            ;;
          esac

          # Add the AMIs to CID (can be removed once they are automatically pulling the latest, in the next version of CID)
          # us-east-1 only, because CID doesn't currently handle other regions
          if [[ "${destination_region}" == "us-east-1" ]]; then
            add_amis_to_cid "${destination_region}" "${new_ami_id}"
          fi

          # Set as latest
          # This may need an option flag or something, in case we ever want to distribute one without marking it as latest
          # Also, this could break if multiple AMIs of the same type are being distributed at once
          # Possibly this will need to be broken out into its own separate queue
          change_latest_amis "${destination_region}" "${new_ami_id}"

        } #&
      done

    } #&
  done

  # Wait for all of the individual jobs to finish
  wait

  # Turn monitor mode back on
  set -m
}


## From base AMIs, create new ones with the enhanced networking property enabled
# Note: This is out-of-date; it will need updating if it is to be used again
function create_enhanced_networking_amis () {
  local usage="Usage: ${FUNCNAME[0]} {region} {ova_file_1} [ova_file_2 ova_file_3 ...]
Creates a copy of each provided AMI with the enhanced networking property enabled"

  [ "$1" ] || { echo "ERROR: region not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local region="$1"
  shift
  [ "$1" ] || { echo "ERROR: no ami ids or no region specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Loop through provided AMI IDs
  for initial_ami_id in "$@"; do
    # Multitask
    set +m
    {

      # Pull data from original AMI
      local initial_ami_metadata="$(aws ec2 describe-images --region "${region}" --image-ids ${initial_ami_id})"
      local initial_ami_name="$(echo "${initial_ami_metadata}" | jq -r '.Images[].Name')"
      local initial_ami_description="$(echo "${initial_ami_metadata}" | jq -r '.Images[].Description')"
      local initial_ami_color_text="$(echo $initial_ami_name | sed -r 's/^.*\((.*)\).*$/\1/')"
      local initial_ami_name_sans_color_text="$(echo $initial_ami_name | sed -r 's/^(.*)\(.*\)(.*)$/\1\2/')"

      local initial_ami_company initial_ami_basic_type initial_ami_label initial_ami_os initial_ami_major_version initial_ami_arch initial_ami_date_and_timestamp
      read initial_ami_company initial_ami_basic_type initial_ami_label initial_ami_os initial_ami_major_version initial_ami_arch initial_ami_date_and_timestamp <<< "${initial_ami_name_sans_color_text}"

      local new_ami_name="${initial_ami_company} ${initial_ami_basic_type} ${initial_ami_label}_enhnet ${initial_ami_os} ${initial_ami_major_version} ${initial_ami_arch} (${initial_ami_color_text} with enhanced networking) ${initial_ami_date_and_timestamp}"
      local new_ami_description="${new_ami_name}"

      # Wait for the initial AMI to be available
      wait_for_amis 3 "${region}" "${initial_ami_id}" || continue

      # Launch an instance from the initial AMI
      # If they change, you can list available subnets for this with `aws ec2 describe-subnets`
      local subnet_id="subnet-a21c8fff" # new aaw VPC
      #local subnet_id="subnet-65bcf64f" # old aaw VPC
      # This must be an instance type with enhanced networking support, obviously
      local instance_type="m4.large"
      local temporary_instance_id="$(aws ec2 run-instances --region "${region}" --image-id "${initial_ami_id}" --instance-type "${instance_type}" --subnet-id "${subnet_id}" | jq -r '.Instances[].InstanceId')"
      [ "${temporary_instance_id}" ] && log "Ran instance ${temporary_instance_id} from AMI ${initial_ami_id}" || { log "ERROR: Could not run temporary instance from AMI ${initial_ami_id}"; continue; }

      # Wait for the temporary instance to be reachable
      log "Waiting for instance ${temporary_instance_id} to start..."
      # First, we need to wait for it to be running
      for tries in {1..2}; do
        [ ${tries} == 1 ] || log "${temporary_instance_id} still unavailable after $((tries * 10)) minutes; waiting longer"
        aws ec2 wait instance-status-ok --region "${region}" --instance-ids ${temporary_instance_id} && break
      done
      [ $? == 0 ] || { log "ERROR: ${temporary_instance_id} still unavailable after $((tries * 10)) minutes"; continue; }
      # Then wait a bit longer, so anything running at boot (e.g. cloud-init) can finish up
      sleep 5m
      log "Instance ${temporary_instance_id} started"

      # Stop the temporary instance
      aws ec2 stop-instances --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
      [ $? == 0 ] && { log "Stopping instance ${region}:${temporary_instance_id}"; } || { log "ERROR: Problem trying to stop instance ${region}:${temporary_instance_id}"; continue; }

      # Wait for it to actually be stopped
      aws ec2 wait instance-stopped --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
      [ $? == 0 ] && { log "Stopped instance ${region}:${temporary_instance_id}"; } || { log "ERROR: Problem stopping instance ${region}:${temporary_instance_id}"; continue; }

      # Set the enhanced networking properties
      aws ec2 modify-instance-attribute --region "${region}" --instance-id "${temporary_instance_id}" --sriov-net-support "simple" > /dev/null
      [ $? == 0 ] && log "Set the sriov-net-support enhanced networking property on instance ${region}:${temporary_instance_id}" || { log "ERROR: Problem setting the enhanced networking property on instance ${region}:${temporary_instance_id}"; continue; }
      aws ec2 modify-instance-attribute --region "${region}" --instance-id "${temporary_instance_id}" --ena-support > /dev/null
      [ $? == 0 ] && log "Set the ena-support enhanced networking property on instance ${region}:${temporary_instance_id}" || { log "ERROR: Problem setting the enhanced networking property on instance ${region}:${temporary_instance_id}"; continue; }

      # Create a new image
      enhanced_ami_id="$(aws ec2 create-image --region ${region} --instance-id ${temporary_instance_id} --name "${new_ami_name}" --description "${new_ami_description}" | jq -r '.ImageId')"
      [ "${enhanced_ami_id}" ] && log "Creating AMI ${enhanced_ami_id} from instance ${temporary_instance_id} in ${region}" || { log "ERROR: Could not create AMI from instance ${region}:${temporary_instance_id}"; continue; }

      # Wait for the new image to be available
      wait_for_amis 3 "${region}" "${enhanced_ami_id}" || continue

      # Terminate the instance
      aws ec2 terminate-instances --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
      [ $? == 0 ] && log "Terminated instance ${region}:${temporary_instance_id}" || { log "ERROR: Problem terminating instance ${region}:${temporary_instance_id}"; continue; }

      # Output final status
      log "Created ${region}:${enhanced_ami_id} with enhanced networking from base AMI ${region}:${initial_ami_id}"

    } &
  done

  # Wait for all upload jobs to complete
  wait

  # Turn monitor mode back on
  set -m
}


## Upload ova file to s3 and import to an EC2 AMI
# Adds list of ami IDs for the imported images to the array imported_ami_list
function upload_aws_images () {
  local usage="Usage: ${FUNCNAME[0]} {ova_file_1} [ova_file_2 ova_file_3 ...]
Imports ova files to EC2 and sets delete on termination/enhanced networking properties, if applicable"
  # Specify the s3 bucket and region for the upload and import
  local s3bucket="com-novartis-nibr-devops-scratch-aaw"
  local region="us-east-1"

  [ "$1" ] || { echo "ERROR: Source ova file(s) not specified in call to ${FUNCNAME[0]}"; echo "$usage"; return 1; }

  # Get AWS credentials from Turbot
  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  for ova_for_upload in "$@"; do
    # Run each upload in the background; much faster when importing multiple images
    # Turn off monitor mode for jobs, to avoid excessive output
    set +m
    {
      local ova_file="$(basename "${ova_for_upload}")"
      local ova_dir="$(dirname "${ova_for_upload}")"
      local ami_name="${ova_file%.ova}"

      # Parse out the components of the name
      # http://web.global.nibr.novartis.net/apps/confluence/display/CD/AWS+AMI+Naming+Standard
      # nibr baseos <label> <OS type> <OS version> <architecture> (<color text>) <creation date>-<version>
      local ami_color_text="$(echo $ami_name | sed -r 's/^.*\((.*)\).*$/\1/')"
      local ami_name_sans_color_text="$(echo $ami_name | sed -r 's/^(.*)\(.*\)(.*)$/\1\2/')"
      local ami_company ami_basic_type ami_label ami_os ami_major_version ami_arch ami_date_and_timestamp
      read ami_company ami_basic_type ami_label ami_os ami_major_version ami_arch ami_date_and_timestamp <<< "${ami_name_sans_color_text}"

      # Fix certain names: VirtualBox has a more restrictive character limit than AWS, so we need to do some translation
      ami_label="${ami_label//nxc/nx_core}"
      ami_color_text="${ami_color_text//nxc/nx_core}"
      if [[ "${ami_label}" == "nx_core_informatic2" ]]; then ami_label="nx_core_informatics_collaboration"; fi
      if [[ "${ami_color_text}" == "nx_core informatic2" ]]; then ami_color_text="nx_core informatics collaboration"; fi

      # Recreate AMI name with any fixes included
      ami_name="${ami_company} ${ami_basic_type} ${ami_label} ${ami_os} ${ami_major_version} ${ami_arch} (${ami_color_text}) ${ami_date_and_timestamp}"

      # Name and description are the same for now
      local ami_description="${ami_name}"

      # Create S3 bucket - only needed once
      #aws s3 mb --region "${region}" "s3://${s3bucket}"

      # Copy to S3
      aws s3 cp --sse AES256 "${ova_dir}/${ova_file}" "s3://${s3bucket}"
      [ $? == 0 ] && log "Successfully uploaded ${ova_file} to s3://${s3bucket}" || { log "ERROR: Problem uploading ${ova_file} to s3://${s3bucket}"; continue; }

      # Import from S3 to AMI
      # Capture the output, for parsing out the information we want
      log "import_task will run aws ec2 import-image --region "${region}" \
        --description \"${ami_name}\" \
        --architecture \"${ami_arch}\" \
        --license-type \"BYOL\" \
        --disk-containers \"Description=\"${ami_name}\",Format=\"ova\",UserBucket={S3Bucket=\"${s3bucket}\",S3Key=\"${ova_file}\"}"

      local import_task="$(aws ec2 import-image --region "${region}" \
	--description "${ami_name}" \
	--architecture "${ami_arch}" \
	--license-type "BYOL" \
	--disk-containers "Description=\"${ami_name}\",Format=\"ova\",UserBucket={S3Bucket=\"${s3bucket}\",S3Key=\"${ova_file}\"}")"

      # Pull the task ID
      local import_task_id="$(echo $import_task | jq -r '.ImportTaskId')"

      # Check up on it
      # aws ec2 wait image-available would work for this, but wouldn't give as detailed status updates
      while : ; do
	local currentstatus="$(aws ec2 describe-import-image-tasks --region "${region}" --import-task-ids ${import_task_id})"
	if [ "$(echo "${currentstatus}" | jq -r '.ImportImageTasks[] | .Status')" == "completed" ]; then log "Import ${import_task_id} of s3://${s3bucket}/${ova_file} to ${region} complete"; break; fi
	log "Import ${import_task_id} of s3://${s3bucket}/${ova_file} to ${region} in progress - $(echo "${currentstatus}" | jq -rj '.ImportImageTasks[] | .StatusMessage, " (", .Progress, "%)"')"
	sleep 30
	# To-do: Fail after a certain max time, killing the import task and outputting an error
      done

      # Snag the initial AMI id
      local initial_ami_id="$(echo "${currentstatus}" | jq -r '.ImportImageTasks[] | .ImageId')"
      # To-do: Check for failure and give an error, though it will be largely theoretical until we have an actual failure example to work from
      log "Imported s3://${s3bucket}/${ova_file} to ${region}:${initial_ami_id}"

      # Remove ova file from s3 bucket
      aws s3 rm --region "${region}" "s3://${s3bucket}/${ova_file}" && log "Deleted ${ova_file} from s3://${s3bucket}" || log "ERROR: Problem deleting ${ova_file} from s3://${s3bucket}"

      # Wait for the initial AMI to be available
      wait_for_amis 3 "${region}" "${initial_ami_id}" || continue

      # Pull the block device info and add delete on termination
      initial_ami_metadata="$(aws ec2 describe-images --region "${region}" --image-ids "${initial_ami_id}")"
      ami_root_device_name="$(echo "${initial_ami_metadata}" | jq -r '.Images[].RootDeviceName')"
      ami_block_device_mappings="$(echo "${initial_ami_metadata}" | jq -r '.Images[].BlockDeviceMappings[] | .Ebs.DeleteOnTermination = true | del(.Ebs.Encrypted)')"

      # Manual debug code. Don't uncomment this.
      # ami_name="$(echo "${initial_ami_metadata}" | jq -r '.Images[].Name')b"
      # ami_description="$(echo "${initial_ami_metadata}" | jq -r '.Images[].Description')b"
      # ami_arch="x86_64"
      
      # Register the new image
      # Much simpler than spinning up a temporary instance and then creating a new AMI for each enhanced networking option
      # Doesn't work for Windows AMIs, but they are running their own entirely separate process now anyway
      final_ami_id="$(aws ec2 register-image \
                        --region "${region}" \
                        --name "${ami_name}" \
                        --description "${ami_description}" \
                        --virtualization-type "hvm" \
                        --architecture "${ami_arch}" \
                        --root-device-name "${ami_root_device_name}" \
                        --block-device-mappings "${ami_block_device_mappings}" \
                        --ena-support \
                        --sriov-net-support "simple" \
                      | jq -r '.ImageId')"


      # The next several steps; launching an instance, changing the settings, creating a new image, then deleting the old one; were all to work
      # around the fact that the "delete on termination" property for the root volume can neither be specified during import nor set directly
      # on the AMI afterward. They put in a feature request for this when I contacted support, but considering that other very simple and straightforward
      # feature requests have languished for literally years, this was the best workaround for the meantime.
      # We're now using a much simpler process with register-image, since we don't need to accommodate Windows image uploads.
      # If we ever take Windows back on, we could branch on OS type and add an updated version of the code below.

#      # Launch an instance from the initial ami
#      # If they change, you can list available subnets for this with `aws ec2 describe-subnets`
#      local subnet_id="subnet-65bcf64f"
#      # Use an instance type with enhanced networking support
#      local instance_type="m4.large"
#      local temporary_instance_id="$(aws ec2 run-instances --region "${region}" --image-id "${initial_ami_id}" --instance-type "${instance_type}" --subnet-id "${subnet_id}" | jq -r '.Instances[].InstanceId')"
#      [ "${temporary_instance_id}" ] && log "Ran instance ${temporary_instance_id} from AMI ${initial_ami_id}" || { log "ERROR: Could not run temporary instance from AMI ${initial_ami_id}"; continue; }
#
#      # Wait for the temporary instance to be reachable
#      log "Waiting for instance ${temporary_instance_id} to start..."
#
#      # First, we need to wait for it to be running
#      for tries in {1..2}; do
#        [ ${tries} == 1 ] || log "${temporary_instance_id} still unavailable after $((tries * 10)) minutes; waiting longer"
#        aws ec2 wait instance-status-ok --region "${region}" --instance-ids ${temporary_instance_id} && break
#      done
#      [ $? == 0 ] || { log "ERROR: ${temporary_instance_id} still unavailable after $((tries * 10)) minutes"; continue; }
#
#      # Then wait a bit longer, so anything running at boot (e.g. cloud-init) can finish up
#      sleep 5m
#      log "Instance ${temporary_instance_id} started"
#
#      # Get root volume name
#      local root_volume="$(aws ec2 --region $region describe-instances --instance-ids ${temporary_instance_id} | jq -r '.Reservations[].Instances[].RootDeviceName')"
#      [ "${root_volume}" ] && { log "Root volume of instance ${region}:${temporary_instance_id} is ${root_volume}"; } || { log "ERROR: Could not determine root volume for instance ${region}:${temporary_instance_id}"; continue; }
#
#      # Set root volume to delete on termination
#      aws ec2 modify-instance-attribute --region "${region}" --instance-id "${temporary_instance_id}" --block-device-mappings "[{\"DeviceName\": \"${root_volume}\",\"Ebs\":{\"DeleteOnTermination\":true}}]"
#      [ $? == 0 ] && log "Set root volume ${root_volume} on instance ${region}:${temporary_instance_id} to delete on termination" || { log "ERROR: Problem setting root volume ${root_volume} on instance ${region}:${temporary_instance_id} to delete on termination"; continue; }
#
#      # Stop the temporary instance
#      aws ec2 stop-instances --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
#      [ $? == 0 ] && { log "Stopping instance ${region}:${temporary_instance_id}"; } || { log "ERROR: Problem trying to stop instance ${region}:${temporary_instance_id}"; continue; }
#
#      # Wait for it to actually be stopped
#      aws ec2 wait instance-stopped --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
#      [ $? == 0 ] && { log "Stopped instance ${region}:${temporary_instance_id}"; } || { log "ERROR: Problem stopping instance ${region}:${temporary_instance_id}"; continue; }
#
#      # Set the enhanced networking property, if this is nx_core
#      # This should be changed to an option flag at some point
#      if [[ "${ami_label}" =~ nx_core ]]; then
#        aws ec2 modify-instance-attribute --region "${region}" --instance-id "${temporary_instance_id}" --sriov-net-support "simple" > /dev/null
#        [ $? == 0 ] && log "Set the enhanced networking property on instance ${region}:${temporary_instance_id}" || { log "ERROR: Problem setting the enhanced networking property on instance ${region}:${temporary_instance_id}"; continue; }
#      fi
#
#      # Create a new image
#      final_ami_id="$(aws ec2 create-image --region ${region} --instance-id ${temporary_instance_id} --name "${ami_name}" --description "${ami_description}" | jq -r '.ImageId')"
#      [ "${final_ami_id}" ] && log "Creating AMI ${final_ami_id} from instance ${temporary_instance_id} in ${region}" || { log "ERROR: Could not create AMI from instance ${region}:${temporary_instance_id}"; continue; }
#      #sleep 15m


      # Wait for the new image to be available
      wait_for_amis 3 "${region}" "${final_ami_id}" || continue

      # Deregister initial AMI
      # We just deregister it directly in this case instead of using the delete_amis functions, since the snapshot is still used on the new image
      aws ec2 deregister-image --region "${region}" --image-id "${initial_ami_id}"

#      # Terminate the instance
#      aws ec2 terminate-instances --region "${region}" --instance-ids ${temporary_instance_id} > /dev/null
#      [ $? == 0 ] && log "Terminated instance ${region}:${temporary_instance_id}" || { log "ERROR: Problem terminating instance ${region}:${temporary_instance_id}"; continue; }

#      # Delete the old image and snapshot
#      delete_amis "${region}" "${initial_ami_id}"

      # Output final status of AMI
      touch "${build_untested_ami_dir}/${final_ami_id}"
      log "Successfully created AMI ${region}:${final_ami_id}, ${ami_name}"
    } &
  done

  # Wait for all upload jobs to complete
  wait

  # Point to status files
  log "All successfully imported AMIs listed in ${build_untested_ami_dir}/"
#  log "Any unsuccessful imports listed in ${build_status_dir}/import_broken/"
}


## Run an instance for testing
# Note that this needs a bit of work yet, to operate in any but the default manner
function run_test_instance () {
  local usage="Usage: ${FUNCNAME[0]} {turbot account} {region} {instance type} {ami id}
Run an instance with standard test settings"

  [ "$1" ] && [ "$2" ] && [ "$3" ] && [ "$4" ] || { echo "ERROR: invalid usage for ${FUNCNAME[0]}"; echo "$usage"; return 1; }
  local turbot_account="$1"; shift
  local region="$1"; shift
  local instance_type="$1"; shift
  local ami_id="$1"; shift

  key_name="snydesc1"
  key_contents='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCk1U5HEhWUemRY85YodesuCz2QCK3kChChSNHn2z60sTCoyK/Ys1SQmaM4Fic9gQF1E/RFXKcO11PIBduRx+bw/pNXXgmHLOiPPaAvwFnxRmZocPWFLr3uA49bo+izKqlQ6nrVglxSHSAGDtVLEl4KyXqETvlzJtEvCKfU9//8dR6Ia+uGnTKJ32/Ka60VGHRVZCWFiRncbTLv7mTkgGtuVlGsEZgSsdLPMoTcYJfJj+LFyPf89ijvHkhefPEGQAzxIZyScWJ73cNP70iLmK41rmIIyB467ij87mK39DBlbHPuZVne2a4vZ46QVlu/fpkTvOxEKV4Tzfb925rBW+Zr snydesc1'
  subnet_id="subnet-a21c8fff" # new aaw VPC
  #subnet_id="subnet-65bcf64f" # old aaw VPC
  #subnet_id="subnet-" # syslog test
  security_groups="sg-41c9ac35" # new aaw VPC
  #security_groups="sg-68036013" # old aaw VPC
  #security_groups="sg-" # syslog test
  #instance_type="t2.micro"
  #instance_type="r4.large" # cheapest ENA instance type
  #instance_type="m4.large" # cheapest ixgbevf instance type
  #instance_type="i3.8xlarge" # missing NVMe SSDs
  #instance_type="i3.16xlarge" # missing NVMe SSDs
  #ami_id=ami-32a37824 # enhanced networking
  #ami_id=ami-1c894b0a # missing SSD drives, CentOS 7
  #ami_id=ami-0e8a4818 # missing SSD drives, RHEL 7
  #ami_id=ami-b63769a1 # missing SSD drives, official RHEL 7
  #ami_id=ami-abae09bd # cloud-init, resize and resolv
  #ami_id=ami-8c34939a # C6 cloud-init
  #ami_id=ami-ef14f582 # official RHEL 6
  #ami_id=ami-22ef4d34 # enhanced networking base
  #ami_id=ami-eca002fa # enhanced networking baked-in
  #ami_id=ami-436fc055 # informatics CentOS 7
  #ami_id=ami-566ec140 # informatics RHEL 6
  #ami_id=ami-0e903f18 # beta CentOS 6
  #ami_id=ami-70913e66 # beta CentOS 7
  #ami_id=ami-1c894b0a # CentOS 7 latest
  #ami_id=ami-73e44f65 # CentOS 7 with older kernel
  #ami_id=ami-72e44f64 # RHEL 7 with older kernel
  #ami_id=ami-357b8a23 # potieyo1 repo issues
  #ami_id=ami-aa1fadbc # chapmma6 NETWORKWAIT
  #ami_id=ami-357b8a23 # zelenan1 crond
  #ami_id=ami-0e903f18 # CentOS 6 latest
  #ami_id=ami-2a65043c # CentOS 7 04-29
  #ami_id=ami-c86b06de # CentOS 6 05-02
  #ami_id=ami-575e6841 # CentOS 6 06-26
  #ami_id=ami-ee5066f8 # CentOS 7 06-26
  #ami_id=ami-25121533 # CentOS 7 Informatics collaboration

  # Get AWS credentials from Turbot
  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  # Check whether the key already exists, and import it if not
  aws ec2 describe-key-pairs --region "${region}" --key-names "${key_name}" &> /dev/null || aws ec2 import-key-pair --region "${region}" --key-name "${key_name}" --public-key-material "${key_contents}"

  # Run an instance, with a larger-than-default root disk so partition/vg expansion can be tested
  local RUN_TEST_CMD="aws ec2 run-instances --region ${region} --image-id ${ami_id}
                 --instance-type ${instance_type} --subnet-id ${subnet_id}
                 --key-name ${key_name} --security-group-ids ${security_groups} 
                 --instance-initiated-shutdown-behavior terminate 
                 --block-device-mapping DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true,VolumeSize=80} 
                 --tag-specifications ResourceType=instance,Tags=[{Key=Name,Value=TestInstance}]
                 --user-data file://${build_base_dir}/public_ssh_keys.sh"

  cmdoutfile=/tmp/file$RANDOM
  echo $RUN_TEST_CMD #debug
  $RUN_TEST_CMD > $cmdoutfile
  

  # Parse out the instance id and ip into variables
  read instance_id instance_ip_addr <<< $(cat $cmdoutfile | jq -r '.Instances[].InstanceId, .Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress')
  rm -f $cmdoutfile
  # Output the IP to connect to
  echo ${instance_ip_addr}

  aws ec2 wait instance-status-ok --region ${region} --instance-id ${instance_id} && echo "${instance_id} ready at ${instance_ip_addr}"



}


## List latest images
function list_latest_images () {
  local usage="Usage: ${FUNCNAME[0]}
Lists the images tagged as latest in each region"

  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  # Loop through the regions, pulling the list for each
  for region in ${nibr_aws_regions}; do
    echo "Latest images in region ${region}:"
    aws ec2 describe-images --region "${region}" --filters Name=description,Values="nibr * latest" | jq -r '.Images[] | [.ImageId, .Name]'
  done

}


## Re-share the latest standard nx_core images
function reshare_latest_images () {
  local usage="Usage: ${FUNCNAME[0]}
Share the latest base nx_core CentOS images to any NIBR AWS accounts that don't already have it"

  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  # Loop through the regions
  for region in ${nibr_aws_regions}; do

    # Pull the appropriate latest images and loop through them
    for ami_id in $(aws ec2 describe-images --region "${region}" --filters Name=description,Values="nibr baseos nx_core centos * x86_64 * latest" | jq -r '.Images[].ImageId'); do
      # Set the permissions and tags
      set_ami_permissions "${region}" "all" "${ami_id}"
      set_ami_tags "${region}" "${ami_id}"
    done

  done

}


## List AWS/Turbot accounts
function list_accounts () {
  local usage="Usage: ${FUNCNAME[0]} [{account 1} {account 2} ...]
List all or specific AWS/Turbot accounts
Specify accounts by AWS and/or Turbot ID. Defaults to all if unspecified"

  # Grab Turbot credentials
  #. "${build_base_dir}/turbot.cred"

  # Look up accounts, then filter for those specified, if any
  curl --insecure --silent --request GET --header "Content-Type: application/json" --url https://turbot-app.prd.nibr.novartis.net/api/v2/accounts --user ${turbot_access_key}:${turbot_secret_key} | jq -r '.items[] | [ .awsAccountId, .id, .description ] | @tsv' | awk -v list="$*" 'list ~ $1 || list ~ $2 || list == "" {print}'

}


## List instances in default account
function list_instances () {
  local usage="Usage: ${FUNCNAME[0]}
Lists the images tagged as latest in each region"

  get_aws_credentials "${turbot_user}" "${turbot_default_account}"

  # Loop through the regions, pulling the list for each
  for region in ${nibr_aws_regions}; do
    echo "  Instances in region ${region}:"
      aws ec2 describe-instances --region "${region}" | jq -r '["Instance ID", "AMI ID", "Key Name", "IP Address", "Status"], (.Reservations[].Instances[] | [.InstanceId, .ImageId, .KeyName, .PrivateIpAddress, .State.Name]) | @tsv' | column -t -s $'\t'
      echo
  done

}


## Build images for AWS and Vagrant
function build_images () {
  local -a argz=( "$@" )

  local -a AVAILABLE_IMAGES
  AVAILABLE_IMAGES=( $( grep -P '^\s+\"name\":\s+' ${build_json_dir}/nx-builds-commented.json |tr -d '",' |cut -d : -f 2) )

  # To-do: parse arguments for builders to run
  local usage="Usage: ${FUNCNAME[0]} [packer_builder_1 packer_builder_2 ...]\n available images are ${AVAILABLE_IMAGES[*]}"

  if [ -z "$argz" ]; then
      log "ERROR: ${FUNCNAME[0]} requires a list of images to build."; log "$usage"; return 1
  fi

  local -a images_to_build
  
  # TODO: It is nice to dream; someday packer can run all this in parallel:
  #if "$argz" == '--all'
  #  images_to_build=( "${AVAILABLE_IMAGES[@]}" )
  #else

  # verify I can type correctly on the command line:
  # meaning, make sure the given ami-name is valid, don't waste time on a bad build.
  for imagename in "${argz[@]}"; do
    local found=0
    for availablename in "${AVAILABLE_IMAGES[@]}"; do
      if [ "$imagename" == "$availablename" ]; then
	      found=1
	      images_to_build[${#images_to_build[*]}]="$imagename"
	      break
	    fi
    done
    if [ $found == 0 ]; then
      log "ERROR: ${FUNCNAME[0]} image $imagename is not available. run again with no args to see the list."
	    return 1;
    fi
  done
  images_to_build_list=$( echo "${images_to_build[@]}" |tr ' ' ',')

  log "${FUNCNAME[0]} packer asked to build $images_to_build_list"

  # Run packer
  packer_cmd build -only=$images_to_build_list -force <(jq -n -f "${build_json_dir}/nx-builds-commented.json")

  if [ $? -ne 0 ]; then
      log "ERROR: packer command failed $?, skip upload"
      return 1
  fi

  # Pull ami list from manifest into array for later commands
  # readarray (aka mapfile) is the easiest way to do this without problems due to whitespace or special characters
  #ovas_from_manifest=$(jq -r '.builds[] | select(.name | contains("-ami")) | .files[] | .name' "${build_base_dir}/packer-manifest.json")
  readarray -t ovas_from_manifest < <(jq -r '.builds[] | select(.name | contains("-ami")) | .files[] | .name' "${build_base_dir}/packer-manifest.json")

  if [ ${#ovas_from_manifest[@]} -gt 0 ]; then
    # Push the AWS images up to Amazon
    upload_aws_images "${ovas_from_manifest[@]}"

    # List the imported AMIs
    echo "New, untested AMIs: ${imported_ami_list[*]}"; echo;
  fi

  # Pull list of new Vagrant boxes from manifest
  new_vagrant_list=($(jq -r '.builds[] | select(.name | contains("-vagrant")) | .files[] | .name' packer-manifest.json | awk '{system ("basename "$0)}'))

  if [ ${#new_vagrant_list[@]} -gt 0 ]; then
    # Create flag files for new Vagrant boxes
    for vagrant_file in "${new_vagrant_list[@]}"; do touch "${build_untested_vagrant_dir}/${vagrant_file}"; done

    # List new Vagrant boxes
    echo "New, untested Vagrant boxes: ${new_vagrant_list[*]}"; echo;
  fi

}


## Deploy tested AMIs and Vagrant boxes
function deploy_images () {

  # Pull a list of tested AMIs
  readarray -t tested_ami_list < <(ls ${build_tested_ami_dir}/)
  if [[ "$?" == "0" ]]; then rm -f ${build_tested_ami_dir}/*; fi

  # Pull a list of tested Vagrant boxes
  readarray -t tested_vagrant_list < <(ls ${build_tested_vagrant_dir}/)
  if [[ "$?" == "0" ]]; then rm -f ${build_tested_vagrant_dir}/*; fi

  # Create local latest links for vagrant boxes
  pushd "${build_vagrantoutput_dir}" > /dev/null
  log "Creating latest links for vagrant boxes in ${build_vagrantoutput_dir}"
  printf '%s\n' "${tested_vagrant_list[@]}" | awk '{system ("ln --verbose --symbolic --force "$0" "gensub("-vagrant_virtualbox_[0-9]*.box","_latest.box","g"))}'
  popd > /dev/null

  # Add Vagrant boxes to Artifactory
  # To-do: automate this
  log "MANUAL STEP REQUIRED"
  log "To add the new Vagrant boxes to Artifactory:"
  log "1. Make sure you are in the group admins_nibr_rcp_pe_l3_linux_gbl_a"
  log "2. Run the following command, substituting your own adm_521 account"
  log "scp ${tested_vagrant_list[@]/#/${build_vagrantoutput_dir}/} adm_snydesc1@nrusca-slp0002:/var/www/html/vagrant/nxami/"

  # Copy the AMIs to other regions (currently just eu-central-1)
  # Needs modification to handle uploaded/fixed but not shared images
  distribute_amis "${build_region}" "${tested_ami_list[@]}"
}


## Set up common environment variables, functions, etc
export packer_bin="/usr/local/bin/packer"
function packer_cmd () {
    local logpath=$(date "+$build_log_dir/packer-%Y%m%d.%H%M%S.log")
  echo PACKER_LOG_PATH= $logpath
  PACKER_LOG_PATH=$logpath $packer_bin "$@"
}

export build_base_dir="/apps/nx_build"
export build_iso_dir="${build_base_dir}/iso"
export build_ks_dir="${build_base_dir}/ks"
export build_json_dir="${build_base_dir}/json"
export build_scripts_dir="${build_base_dir}/scripts"
export build_vagrantfile_dir="${build_base_dir}/vagrant"
export build_vagrantoutput_dir="${build_base_dir}/boxes"
export build_kitchen_dir="${build_base_dir}/kitchen"
export build_log_dir="${build_base_dir}/log"
export build_status_dir="${build_base_dir}/status"
export build_untested_ami_dir="${build_status_dir}/untested_amis"
export build_tested_ami_dir="${build_status_dir}/tested_amis"
export build_untested_vagrant_dir="${build_status_dir}/untested_vagrants"
export build_tested_vagrant_dir="${build_status_dir}/tested_vagrants"
mkdir -p "${build_untested_ami_dir}" "${build_tested_ami_dir}" "${build_untested_vagrant_dir}" "${build_tested_vagrant_dir}"
export build_region=us-east-1
export nibr_aws_regions="us-east-1 eu-central-1"
mkdir -p "${build_log_dir}"
export http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011
export https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011
export PACKER_LOG_PATH="${build_log_dir}/packer-adhoc.log"
export PACKER_LOG=1
export VAGRANT_HOME="${build_base_dir}/.vagrant.d"
export turbot_default_account="aaw"
# We don't have api access to Turbot dev, so for now we have to use a static list of accounts
export turbot_dev_accounts="
661207772925
810622225034
189702839778
922157597662
445060670821
332818835778
721358240883"
pushd "${build_base_dir}"
complete -C '/usr/bin/aws_completer' aws
# Unset any AWS access variables, then set new ones
#unset AWS_ACCESS_KEY AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SECRET_KEY AWS_SESSION_TOKEN
#. .awscli
# Grab Turbot credentials, Turbot user, and default account
# . "${build_base_dir}/turbot.cred"
# Grab the default credentials, so that ad-hoc commands will work right away
get_aws_credentials "${turbot_user}" "${turbot_default_account}"
alias vi='vim'
# Create nx_build cron jobs
cat > /etc/cron.d/nx_build << "EOF"
# Share out the latest base nx_core AMIs to any NIBR accounts that don't have them
SHELL=/bin/bash
0 0 * * * root /bin/bash -c '(echo "$(date) - Adding any new AMI permissions";. /apps/nx_build/env.sh && reshare_latest_images) &>> /var/log/acs/nx_build-ami-permissions.log'

EOF
chmod 640 /etc/cron.d/nx_build
