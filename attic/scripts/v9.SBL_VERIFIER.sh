#!/bin/bash  
# DERBY @ SBL Verification Script : 5/26/2015 Version = 8

#### GLOBAL VARS 
export myStatus=""
export isSet="is_set_properly"
export isNot="is_NOT_SET_properly"
###

### HELPER FUNCTIONS : 

# lets you keep adding the results of each check 
setStatus(){
myStatus="$myStatus@${1}"
}

# prints out the results of all checks
printFinalStatus(){ echo $myStatus |tr '@' '\n' ; } 

############

function verify_3.1.1(){
myFile=/etc/ssh/sshd_config
export num=3.1.1
#echo "ssh Protocol 2 " 

egrep ^Protocol\ 2 $myFile 
if [ $? -eq 0 ]
        then
		setStatus "$num:OK:Protocol_2_$isSet"
        else
		setStatus "$num:ERROR:Protocol_2_$isNOT"
		grep ^Protocol\ 1 $myFile
		if [ $? -eq 0 ]
			then
				setStatus "$num:ERROR:ssh_is_on_Protocol_1"
			else
				setStatus "$num:UNKNOWN_ISSUE:_verify_$myFile" 
		fi
fi
}

function verify_3.1.2(){ 
export num=3.1.2 
#pw="$1$8Z4eB$jAH7Mt1md1rdPTq/A1Hsy."
pw="$1$df48G$wWI7uQmrxNvlNdXHWam03."
grep ^PROMPT=no /etc/sysconfig/init 
	if [ $? -eq 0 ] 
		then 
			setStatus "$num:OK:PROMPT=no_in_/etc/sysconfig/init_$isSet" 
		else 
			setStatus "$num:ERROR:PROMPT=no_in_/etc/sysconfig/init_$isNot" 
	fi 

grep $pw /etc/grub.conf 
	if [ $? -eq 0 ] 
		then 
			setStatus "$num:OK:grub_password_$isSet"
		else 
			setStatus "$num:ERROR:grub_password_$isNot"
	fi

val=$( stat -c%a /boot/grub/grub.conf ) 
#etc/grub.conf ) 

	if [ $val -eq 600 ]
		then
			setStatus "$num:OK:/etc/grub.conf_permissions_are_$val_$isSet"
		else
			setStatus "$num:ERROR:/etc/grub.conf_permissions_$isNot"
	fi


grep "SINGLE=/sbin/sulogin" /etc/sysconfig/init 
	if [ $? -eq 0 ]
		then 
			setStatus "$num:OK:/etc/sysconfig/init_$isSet"
		else
			setStatus "$num:ERROR:/etc/sysconfig/init_$isNot"
	fi 

grep "~:S:wait:/sbin/sulogin" /etc/inittab
	if [ $? -eq 0 ] 
		then 
			setStatus "$num:OK:/etc/inittab_$isSet"
		else 
			setStatus "$num:ERROR:/etc/inittab_~:S:wait:/sbin/sulogin_$isNot"
	fi


}



function verify_3.1.3(){  
export num=3.1.3
isilonPrefix="isiusca"
fstab="/etc/fstab" 

export dirs="/boot:/scratch:/dev/shm:/var/tmp" 

for i in $( echo $dirs |tr ':' '\n' ) 
 	do  
		grep nodev $fstab |grep -i $i 
			if [ $? -eq 0 ] 
				then 
					setStatus "$num:OK:LocalMount_=_${i}_$isSet"
				else 
					setStatus "$num:ERROR:LocalMount=${i}_$isNot"
			fi 
 done ;


for j in $( cat /etc/fstab |grep isiusca|awk '{print $2}') 
	do 
	    grep $j /etc/fstab |grep nodev 
		if [ $? -eq 0 ] 
			 then 
				setStatus "$num:OK:Isilon_Mount_:${j}_nodev_$isSet"
			 else 
				setStatus "$num:ERROR:${j}_nodev_$isNot"
		fi 
	done

 }


function verify_3.1.4(){
output=/dev/shm/3.1.4.log
# deletes any previous logs :
rm -rf $output
export num=3.1.4
dirs="bin,boot,dev,etc,lib,lib64,local,media,misc,mnt,net,opt,root,sbin,scratch,selinux,srv,tmp,var,usr/bin,usr/etc,usr/games,usr/include,usr/lib,usr/lib64,usr/libexec,usr/local,usr/sbin,usr/share,usr/src,usr/tmp"

for i in $( echo $dirs |tr ',' '\n' )
        do
                find /$i -xdev -type d \( -perm -002 -a ! -perm -1000 \) -print  >>$output

        done

lines=$(cat $output |wc -l )
if [ $lines -eq 0 ]
        then
		setStatus "$num:OK:no_world_writeable_directories_without_sticky_bit_$isSet"
        else
		#setStatus "$num:ERROR:there_are_world_writeable_directories_without_sticky_bit_set_$isNot"
		setStatus "$num:ERROR:there_are_$(cat $output |wc -l )_world_writeable_directories_without_sticky_bit_set_$isNot_check_$(ls $output) "
		#setStatus "$num:ERROR:$(cat $output)_no_sticky_bit"
fi


# clean up the file : 
rm -rf $output


 }

function verify_3.1.5(){
output=/dev/shm/3.1.5.log
# deletes any previous logs :
rm -rf $output
export num=3.1.5
dirs="bin,boot,dev,etc,lib,lib64,local,media,misc,mnt,net,opt,root,sbin,scratch,selinux,srv,tmp,var,usr/bin,usr/etc,usr/games,usr/include,usr/lib,usr/lib64,usr/libexec,usr/local,usr/sbin,usr/share,usr/src,usr/tmp"

for i in $( echo $dirs |tr ',' '\n' )
        do
                #echo "print scanning $i " # only enabled during debugging :  
                find /$i -xdev -type d \( -perm -002 -a ! -user root \) -print  >>$output

        done

lines=$(cat $output |wc -l )
if [ $lines -eq 0 ]
        then
		setStatus "$num:OK:there_are_no_world_writeable_directories_not_owned_by_root_$isSet"
        else
		setStatus "$num:ERROR:there_are_$(cat $output |wc -l )_world_writeable_directories_not_owned_by_root_$isNot_check_$(ls $output) "
		#setStatus "$num:ERROR:$(cat $output|head -n 2)_not_owned_by_root"
fi


# clean up the file : 
rm -rf $output


 }





function verify_3.1.6(){  
export num=3.1.6

export issue="a8a35b33c273c50f840145ca0d9a4ce7"
export motd="847d7eb9aa1dc864049f3e92429e87b6"

	for i in $( echo /etc/issue /etc/issue.net /etc/ssh/banner ) 
		do              
			file=$i
			md5sum $file |grep -i $issue 
			if [ $? -eq 0 ]
				then  
					setStatus "$num:OK:${file}_${isSet}"
				else  
					setStatus "$num:ERROR:${file}_${isNot}"
	                fi     
		done   

	grep ^Banner\ \/etc\/ssh\/banner /etc/ssh/sshd_config 
		if [ $? -eq 0 ]
			then
				setStatus "$num:OK:sshd_config_banner_$isSet"
			else
				setStatus "$num:ERROR:sshd_config_banner_$isNot"
			
		fi
 }


function verify_3.1.7(){
myFile=/etc/ssh/sshd_config
export num=3.1.7
#echo "ssh IgnoreRhosts " 

egrep ^IgnoreRhosts\ yes $myFile 
if [ $? -eq 0 ]
        then
		setStatus "$num:OK:sshd_config_IgnoreRhosts_$isSet"
        else
		setStatus "$num:ERROR:sshd_config_IgnoreRhosts_$isNot"
fi

}

function verify_3.1.8(){
myFile=/etc/ssh/sshd_config
export num=3.1.8
#echo "ssh Hostbased Authentication"

egrep ^HostbasedAuthentication\ no $myFile
if [ $? -eq 0 ]
        then
		setStatus "$num:OK:No_HostBased_Authentication_$isSet"
        else
		setStatus "$num:ERROR:No_HostBased_Authentication_$isNot"
fi

}

function verify_3.1.9(){
myFile=/etc/ssh/sshd_config

export num=3.1.9.a
#echo "ssh PermitRootLogin NO"
#egrep ^PermitRootLogin\ no $myFile 
egrep ^PermitRootLogin\ without-password $myFile
if [ $? -eq 0 ]
        then
		setStatus "$num:OK:RootLogin_not_permitted_by_ssh_$isSet"
        else
		setStatus "$num:ERROR:ssh_still_allows_root_login_$isNot"
fi

export num=3.1.9.b
#echo "ssh Do not Allow login if user has a null password" 
egrep ^PermitEmptyPasswords\ no $myFile 
if [ $? -eq 0 ]
        then
		setStatus "$num:OK:Users_with_Null_passwords_not_allowed_to_log_in_$isSet"
        else
		setStatus "$num:ERROR:Users_with_Null_passwords_can_still_log_in_$isNot"
fi

}

function verify_3.1.10(){
num=3.1.10; val=$( stat -c%a /etc/ssh/sshd_config ) 
if [ $val -eq 600 ] 
	then 
		setStatus "$num:OK:/etc/ssh/sshd_config_set_to_$val\_$isSet"
	else 
		setStatus "$num:ERROR:/etc/ssh/sshd_config_set_to_$val\_$isNot"
fi
} 

function verify_3.1.11(){
num=3.1.11

	setStatus $num:QUESTION:revisit_this_one_what_should_be_the_default_snmp_string

#REVISIT
} 

function verify_3.1.12(){
num=3.1.12

	setStatus $num:QUESTION:revisit_this_one_Default_umask_permissions_for_all_users_what_do_we_decide

#REVISIT
} 

function verify_3.1.13(){  
export num="3.1.13" 
lines=$(grep -v :x: /etc/passwd |wc -l )
	if [ $lines -gt 0 ] 
		then
		   	setStatus $num:ERROR:There_are_user_accounts_without_proper_passwords_$isNot
		else            
		    	setStatus $num:OK:all_local_users_have_passwords_set_$isSet
	fi 

 } 



function verify_3.2.1(){
myFile=/etc/ssh/sshd_config
export num=3.2.1
#echo "ssh Max authentication attempts ( 4 ) " 
#REVISIT
setStatus $num:QUESTION:can_max_auth_attempts_be_10_or_does_it_really_need_to_be_4

egrep ^MaxAuthTries $myFile
if [ $? -eq 0 ]
        then
		setStatus $num:OK:Max_Auth_tries=$(grep ^MaxAuthTries $myFile)_$isSet
        else
		setStatus $num:ERROR:Max_auth_tries_in_$myFile\_$isNot
fi

}


function verify_3.2.2(){ 
export num=3.2.2 
numLines=$( awk -F: '{print $1}' /etc/passwd |wc -l ) 
sortedLines=$( awk -F: '{print $1}' /etc/passwd |sort -u |wc -l) 
echo $numLines $sortedLines
if [ $sortedLines -eq $numLines ] 
	then 
		setStatus $num:OK:Lines_match_all_users_are_distinct_in_/etc/passwd_$isSet
	else 
		setStatus $num:ERROR:mismatch_dupes_found_in_/etc/passwd_$isNot
fi 
} 

function verify_3.2.3(){
myFile=/etc/login.defs
export num=3.2.3
#echo "set max login tries : 10 " 

egrep ^LOGIN_RETRIES\ 10 $myFile 
if [ $? -eq 0 ]
        then
		setStatus $num:OK:max_login_tries\=$( grep ^LOGIN_RETRIES $myFile )_$isSet
        else
		setStatus $num:ERROR:Max_Auth_Tries_$isNot
fi

}

function verify_3.2.4(){
num=3.2.4
setStatus $num:QUESTION:revisit_all_users_have_distinct_homes_on_isilon_is_this_sufficient
#REVISIT
} 

function verify_3.2.5(){
export num=3.2.5 
value=$( awk -F: '($2 != "x" ) { print } ' /etc/passwd |wc -l ) 
if [ $value -ne 0 ] 
	then 
		setStatus $num:ERROR:some_passwords_not_using_shadow_$value\_of_them_$isNot
	else 
		setStatus $num:OK:all_passwords_using_shadow_$isSet
fi 
}


function verify_3.2.6(){
export num=3.2.6 
value=$( awk -F: '($2 != "x" ) { print } ' /etc/passwd |wc -l ) 
if [ $value -ne 0 ] 
	then 
		setStatus $num:ERROR:some_local_passwords_$value\_$isNot
	else 
		setStatus $num:OK:all_passwords_set_for_local_accounts_$isSet
fi 
}

function verify_3.2.7(){   
export num=3.2.7
file="/etc/local.defs"
fields="MAX_DAYS:MIN_DAYS:MIN_LEN:WARN_AGE"
for i in $( echo $fields |tr ':' '\n' )
	do              
		grep $i $file
		if [ $? -eq 0 ]
			then 
				setStatus $num:OK:$i\_already_set_in_$file\_$isSet
			else 
				setStatus $num:ERROR:$i\_not_found_in_$file\_$isNot
		fi 
	done  

setStatus $num:Question:revisit_is_it_ok_to_have_default_/etc/local.defs_or_are_SBL_settings_required
#REVISIT
  } 


function verify_3.2.8(){
export num=3.2.8; head -n 1 /etc/passwd |grep root:x:0:0:root:/root:/bin/bash 
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:root_is_the_first_entry_in_/etc/_passwd_$isSet
	else 
		setStatus $num:ERROR:root_is_not_the_first_line_in_/etc/passwd_$isNot
fi 
}


function verify_3.2.9(){
export num=3.2.9
head -n 1 /etc/group |grep root:x:0: 
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:root_is_the_first_entry_in_/etc/group_$isSet
	else 
		setStatus $num:ERROR:root_is_not_the_first_line_in_/etc/group_$isNot
fi 
}


function verify_3.3.1(){
num=3.3.1
setStatus $num:QUESTION:ssh_allowed_by_group_only_for_specific_administrators_revisit
} 


function verify_3.3.2(){
num=3.3.2; 
echo "$num: place holder needs to be questioned and discussed ... restrict access to priviledged commands ( su and sudo ), is our current sudo process sufficient? " 
} 



function verify_3.3.3(){
export num=3.3.3
echo "$num: this rule needs to be questioned : restricting perms of user config files...\
should this not be handled on the isilon? sciComp should not be enforcing ? Check & verify? \
recommended chmod 600 ~/.* for every user "
}

function verify_3.3.4(){
export num=3.3.4
echo " THIS RULE NEEDS TO BE VERIFIED MANUALLY" 
echo "$num: this rule needs to be questioned : would we ever enforce user PATHS?" 
echo " users should not have the current directory in their PATH variable" 
echo " echo \$PATH|tr -d "\." " 
}

function verify_3.3.5(){
export num=3.3.5
echo " THIS RULE NEEDS TO BE VERIFIED MANUALLY" 
echo " root should have no world/group writable directories in his PATH"
echo " root should not have null directories in his path " 
echo " root should not have relative paths in his path " 
echo " the output of the echo \$PATH command should be :/usr/sbin; /usr/bin;/sbin;/bin" 
echo "$num: this last bit needs to be questioned : we would never have just this in our environment PATH variable for root ( bright has needs ) "
}  

function verify_3.4.1(){
myFile=/etc/ssh/ssh_config
export num=3.4.1
ciphersInBright=$( grep -i cipher /etc/ssh/ssh_config |grep ^#|tr '#' ' '|tr '\n' ','|sed -e 's/,\ $//g' -e 's/\ \ \ \ /\ /g' -e 's/,$//g')

echo "$num: should be questioned : ARE THESE STILL THE ONLY APPROVED CIPHERS? ... Ciphers aes128-ctr,aes192-ctr,aes256-ctr... these are the ciphers available in bright  : $ciphersInBright "

egrep ^Ciphers  $myFile
if [ $? -eq 0 ]
        then
		setStatus $num:OK:Ciphers_are_set_$isSet
        else
		setStatus $num:ERROR:Ciphers_are_not_set_$isNot
fi
#REVISIT
}

function verify_3.5.1(){   
export num="3.5.1" 
egrep 'net.ipv4.ip_forward=0|net.ipv4.conf.all.send_redirects=0|net.ipv4.conf.default.send_redirects=0' /etc/sysctl.conf 
if [ $? -eq 0 ] 
	then
		setStatus $num:OK:/etc/sysctl.conf_host_not_acting_as_a_router_$isSet
	else            
		setStatus $num:ERROR:/etc/sysctl.conf_host_is_not_acting_as_a_router_$isNot
fi
 }

function verify_3.6.1(){ 
export num="3.6.1"
cat /etc/security/limits.conf |grep "* hard core 0" 
if [ $? -eq 0 ] 
	then  
		setStatus $num\-A:OK:/etc/security/limits.conf_has_core_dumps_$isSet
	else 
		setStatus $num\-A:OK:/etc/security/limits.conf_does_not_have__core_dumps_$isNot
fi 
cat /etc/sysctl.conf |grep "fs.suid.dumpable=0"
if [ $? -eq 0 ] 
	then 
		setStatus $num\-B:OK:/etc/sysctl.conf_fs.suid.dumpable\=0_$isSet
	else 
		setStatus $num\-B:ERROR:/etc/sysctl.conf_fs.suid.dumpable\=0_$isNot
fi 
   }


function verify_3.6.2(){ export num="3.6.2"; 
grep "^LogLevel INFO" /etc/ssh/sshd_config
	if [ $? -eq 0 ]
		then 
			setStatus $num:OK:logLevel_INFO_in_sshd_config_$isSet
		else
			grep "^#LogLevel INFO" /etc/ssh/sshd_config
			if [ $? -eq 0 ]
				then
					setStatus $num:ERROR:Loglevel_INFO_is_commented_out_$isNot
				else
					setStatus $num:ERROR:LoglevelINFO_not_in_/etc/ssh/sshd_config_$isNot
			fi
	fi

} 


function verify_3.6.3(){ 
export num="3.6.3"
service ntpd status |grep running
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:ntpd_running_$isSet
	else 
		setStatus $num:ERROR:ntpd_running_$isNot
fi 
 }

# if it is not equal to 4 then it has too many things...
function verify_3.6.4(){ 
export num="3.6.4"
var=$(ls -la /var/log/* |grep -v :$|grep -v total|grep -v ^$|tr " " ":" |tr -s ":"|grep -v :root:root:|grep -v :gdm:|wc -l ) 
if [ $var -eq 4 ]
	then 
		setStatus $num:OK:/var/log_permissions_are_set_properly
	else 
		setStatus $num:ERROR:permissions_on_/var/log_$isNot\_number_dirs_with_non-root_perms\=$var
fi
 }


function verify_3.6.5(){ 
export num="3.6.5" 
service auditd status |grep running 
if [ $? -eq 0 ]
	then 
		setStatus $num:OK:auditd_running_$isSet
	else 
		setStatus $num:ERROR:auditd_not_running_$isNot
fi 
 }



function verify_3.6.6(){
export num=3.6.6
file="/etc/audit/audit.rules"
sum="a88930367992663f7db9bbefd08bb33a"
md5sum $file |grep $sum 
if [ $? -eq 0 ]
	then
		setStatus $num:ERROR_auditing_$isNot
	else
		setStatus $num:OK:Auditing_Rules_found\=$(( $( wc -l $file |awk '{print $1}') - 14 ))_$isSet 
fi

#cat /etc/audit/audit.rules |tail -n 2 |head -n 1 |grep "See auditctl man page"
#if [ $? -eq 0 ]
#        then
#                echo "$num:ERROR: Auditing does not seem to be setup properly"
#        else
#                echo "$num:OK: Auditing seems to be setup properly... Rules found=$(( $( wc -l /etc/audit/audit.rules|awk '{print $1}')-14))"
#fi

}

function verify_3.6.7(){ 
export num="3.6.7"

egrep =500\ auid!=4294967295\ -k\ mounts /etc/audit/audit.rules |egrep ^-a\ always,exit\ -F\ arch=b64\ -S\ mount\ -F\ auid
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:collection_of_successful_system_mounts_configured_$isSet
	else 
		setStatus $num:ERROR:collection_of_successful_system_mounts_not_configured_$isNot
fi 
} 

function verify_3.6.8(){ 
export num="3.6.8"
cat /etc/audit/audit.rules |egrep ^-w\ /etc/sudoers\ -p\ wa\ -k\ scope
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:monitoring_sys_admin_scope_configured_$isSet
	else 
		setStatus $num:OK:monitoring_sys_admin_scope_not_configure_$isNot
fi 
} 


function verify_3.6.9(){ 
export num="3.6.9"
cat /etc/audit/audit.rules |egrep ^-w\ /var/log/sudo.log\ -p\ wa\ -k\ actions
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:monitoring_of_sudo_actions_configure_$isSet
	else 
		setStatus $num:ERROR:monitoring_of_sudo_actions_not_configured_$isNot
fi 
} 

function verify_3.6.10(){ 
export num="3.6.10"
cat /etc/audit/audit.rules |egrep ^-e\ 2
if [ $? -eq 0 ] 
	then 
		setStatus $num:OK:audit_configuration_is_immutable_$isSet
	else 
		setStatus $num:ERROR:auditing_configuration_is_not_immutable_$isNot
fi 
} 


function verify_3.6.11(){ 
export num="3.6.11"
#STATUS=OK
STATUS=ERROR
CONFIG=INCOMPLETE

cat /etc/audit/audit.rules |egrep ^-w\ /var/log/faillog\ -p\ wa\ -k\ logins
if [ $? -eq 0 ] 
	then 
		cat /etc/audit/audit.rules |egrep ^-w\ /var/log/lastlog\ -p\ wa\ -k\ logins
		if [ $? -eq 0 ] 
			then 
				cat /etc/audit/audit.rules |egrep ^-w\ /var/log/tallylog\ -p\ wa\ -k\ logins
				if [ $? -eq 0 ] 
					then
						cat /etc/audit/audit.rules |egrep ^-w\ /var/log/btmp\ -p\ wa\ -k\ session
						if [ $? -eq 0 ] 
							then
								STATUS=OK 
								CONFIG=COMPLETE
						fi
				fi
				
		fi
fi
		
	setStatus $num:$STATUS:auditing_of_login_logout_events_$CONFIG
} 

# MAIN


verify_3.1.1
verify_3.1.2
verify_3.1.3
verify_3.1.4
verify_3.1.5
verify_3.1.6
verify_3.1.7
verify_3.1.8
verify_3.1.9
verify_3.1.10
verify_3.1.11
verify_3.1.12
verify_3.1.13


verify_3.2.1
verify_3.2.2
verify_3.2.3
verify_3.2.4
verify_3.2.5
verify_3.2.6
verify_3.2.7
verify_3.2.8
verify_3.2.9


verify_3.3.1
verify_3.3.2


verify_3.5.1

verify_3.3.3
verify_3.3.4
verify_3.3.5
verify_3.4.1

verify_3.6.1
verify_3.6.2
verify_3.6.3
verify_3.6.4
verify_3.6.5
verify_3.6.6
verify_3.6.7
verify_3.6.8
verify_3.6.9
verify_3.6.10
verify_3.6.11

printFinalStatus



# TODO THINGS : 
# make a set of wrapper functions for this script 


# go through and make sure we add the stuff for umask for 3.1.12 : 
# ------------- details for 3.1.12 below ----------------------
# By default, we want umask to get set. This sets it for login shell
# Current threshold for system reserved uid/gids is 200
# You could check uidgid reservation validity in
# /usr/share/doc/setup-*/uidgid file
# SBL CHANGE : DERBY / may 2015 ... 077 umask 
#if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
#    umask 002
#else
#    umask 022
#fi

#if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
#    umask 077
#else
#    umask 022
#fi

