#!/bin/bash --

# Environment variable specifically used to initialize a new build server

#export current_packer_installer="https://releases.hashicorp.com/packer/0.11.0/packer_0.11.0_linux_amd64.zip"
#export current_packer_installer="https://releases.hashicorp.com/packer/0.12.1/packer_0.12.1_linux_amd64.zip"
#export current_packer_installer="https://releases.hashicorp.com/packer/1.0.2/packer_1.0.2_linux_amd64.zip"
export current_packer_installer="https://releases.hashicorp.com/packer/1.1.1/packer_1.1.1_linux_amd64.zip"
#export current_vagrant_installer="https://releases.hashicorp.com/vagrant/1.8.6/vagrant_1.8.6_x86_64.rpm"
#export current_vagrant_installer="https://releases.hashicorp.com/vagrant/1.9.7/vagrant_1.9.7_x86_64.rpm"
export current_vagrant_installer="https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.rpm"
#export current_virtualbox_installer="http://download.virtualbox.org/virtualbox/5.1.10/VirtualBox-5.1-5.1.10_112026_el7-1.x86_64.rpm"
#export current_virtualbox_installer="http://download.virtualbox.org/virtualbox/5.1.22/VirtualBox-5.1-5.1.22_115126_el7-1.x86_64.rpm"
export current_virtualbox_installer="http://download.virtualbox.org/virtualbox/5.1.30/VirtualBox-5.1-5.1.30_118389_el7-1.x86_64.rpm"
# Leaving this on older version, because 5.2.0 has potential issues with the included guest additions
#export current_virtualbox_installer="http://download.virtualbox.org/virtualbox/5.2.0/VirtualBox-5.2-5.2.0_118431_el7-1.x86_64.rpm"
#export current_virtualbox_extpack_installer="http://download.virtualbox.org/virtualbox/5.1.10/Oracle_VM_VirtualBox_Extension_Pack-5.1.10-112026.vbox-extpack"
#export current_virtualbox_extpack_installer="http://download.virtualbox.org/virtualbox/5.1.22/Oracle_VM_VirtualBox_Extension_Pack-5.1.22-115126.vbox-extpack"
export current_virtualbox_extpack_installer="http://download.virtualbox.org/virtualbox/5.1.30/Oracle_VM_VirtualBox_Extension_Pack-5.1.30-118389.vbox-extpack"
#export current_virtualbox_extpack_installer="http://download.virtualbox.org/virtualbox/5.2.0/Oracle_VM_VirtualBox_Extension_Pack-5.2.0-118431.vbox-extpack"

# For RHEL/CentOS 7
export current_chefdk_installer="https://packages.chef.io/files/stable/chefdk/1.0.3/el/7/chefdk-1.0.3-1.el7.x86_64.rpm"
# or for RHEL/CentOS 6
#export current_chefdk_installer="https://packages.chef.io/files/stable/chefdk/1.0.3/el/6/chefdk-1.0.3-1.el6.x86_64.rpm"
