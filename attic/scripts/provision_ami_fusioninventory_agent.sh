#!/bin/bash --

# we install the fusioninventory agent
yum -y install nibr_fusioninventory-agent nibr_fusioninventory-agent-task-collect nibr_fusioninventory-agent-task-inventory dbus

# we make sure the required services are started at boot (list is os-major-specific)
major_release=$(/usr/local/bin/acs-nxfacts get core os_release_major)
if [ $major_release -eq 6 ]; then
  chkconfig haldaemon on
  chkconfig messagebus on
  chkconfig atd on
else
  systemctl enable messagebus.service
  systemctl enable atd.service
fi

# we set the URL of the fusion server to contact, as well as a pre-defined TAG
sed -i -e '/^server/d' -e '/^tag/d' /etc/fusioninventory/agent.cfg
cat >> /etc/fusioninventory/agent.cfg << 'EOF'
server = http://fusioninventory.global.nibr.novartis.net/fusion
tag = AWS_SRV
EOF

cat > /usr/local/libexec/acs/acs-fusioninventory-agent-run << 'EOF'
#!/bin/bash

if [ -d '/tmp/kitchen' ] || [ -d '/var/tmp/kitchen' ]; then
  logger acs-fusioninventory-agent-run "This is a kithen machine, we do not want it in the inventory"
  sed -i -e '/^server/d' -e '/^tag/d' /etc/fusioninventory/agent.cfg
else
  /usr/bin/fusioninventory-agent --force --delaytime=1 --wait=1
fi
EOF
chmod 755 /usr/local/libexec/acs/acs-fusioninventory-agent-run

# we set a fusioninventory agent run at boot, so that new isntances appear in the inventory short after they're created
echo '@reboot root echo /usr/local/libexec/acs/acs-fusioninventory-agent-run | at now + 3 minutes' > /etc/cron.d/nx_fusioninventory_at_boot
chmod 640 /etc/cron.d/nx_fusioninventory_at_boot
