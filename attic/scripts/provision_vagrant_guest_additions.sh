#!/bin/bash --


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Install VirtualBox guest additions
echo "Creating /tmp/vboxguestadditions..."
mkdir -p /tmp/vboxguestadditions
echo "Mounting /root/VBoxGuestAdditions.iso to /tmp/vboxguestadditions..."
mount -t iso9660 -o loop /root/VBoxGuestAdditions.iso /tmp/vboxguestadditions
echo "Installing guest additions..."
/tmp/vboxguestadditions/VBoxLinuxAdditions.run
echo "Unmounting /tmp/vboxguestadditions"
umount /tmp/vboxguestadditions
echo "Deleting /root/VBoxGuestAdditions.iso..."
rm -rf /tmp/vboxguestadditions /root/VBoxGuestAdditions.iso
# Start them, so there's not an error before they run on the next boot
echo "Starting guest additions..."
/sbin/service vboxadd start
