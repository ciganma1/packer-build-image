#!/bin/bash --


### Do configuration common to all builds


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Add yum repos
# Main OS repo(s), EPEL for python-pip and cloud-init, optional for python-pygments on RHEL7 (a dependency of a dependency of cloud-init), supplementary for zhongyi-song-fonts on RHEL6 (dependency of scicomp-all), and the acs repos (acs-update, scicomp-all, etc)
echo "Adding yum repos"

# Set up yum variables
mkdir -p /etc/yum/vars/
echo "dev" > /etc/yum/vars/tag_name
echo "yum.global.nibr.novartis.net" > /etc/yum/vars/yum_server

# Common stuff
cat > /etc/yum.conf << "EOF"
[main]
cachedir=/var/cache/yum
keepcache=0
debuglevel=2
logfile=/var/log/yum.log
exactbasearch=1
obsoletes=1
gpgcheck=1
plugins=1
installonly_limit=3
proxy=http://nibr-proxy.global.nibr.novartis.net:2011
EOF
# Then OS-specific
# Uses the ;;& operator from Bash 4 to execute multiple blocks
case "$PACKER_BUILD_NAME" in
  centos*) cat >> /etc/yum.conf << "EOF"
distroverpkg=centos-release

EOF
  ;;&
  rhel*) cat >> /etc/yum.conf << "EOF"
distroverpkg=redhat-release

EOF
  ;;&
  *) cat >> /etc/yum.conf << "EOF"
[acs-local]
name = 'ACS self-built packages'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/acs-local
gpgcheck = 0
proxy = _none_
priority = 1
enabled = 1

[acs-thirdparty]
name = 'ACS third-party packages'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/acs-thirdparty
gpgcheck = 0
proxy = _none_
priority = 1
enabled = 1

[rsyslog-8]
name = 'ACS adiscon rsyslog v8 mirror'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rsyslog-8
gpgcheck = 0
proxy = _none_
priority = 1
enabled = 1

[epel]
name = 'Extra Packages for Enterprise Linux (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/epel
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-$os_release_major
proxy = _none_
priority = 1
enabled = 1

EOF
  ;;&
  centos6*|rhel6*) 
    # Add yum tags
    echo "6" > /etc/yum/vars/os_release_major
    echo "6Server" > /etc/yum/vars/releasever
    # Add the key for EPEL
    cat > /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6 << "EOF"
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.5 (GNU/Linux)

mQINBEvSKUIBEADLGnUj24ZVKW7liFN/JA5CgtzlNnKs7sBg7fVbNWryiE3URbn1
JXvrdwHtkKyY96/ifZ1Ld3lE2gOF61bGZ2CWwJNee76Sp9Z+isP8RQXbG5jwj/4B
M9HK7phktqFVJ8VbY2jfTjcfxRvGM8YBwXF8hx0CDZURAjvf1xRSQJ7iAo58qcHn
XtxOAvQmAbR9z6Q/h/D+Y/PhoIJp1OV4VNHCbCs9M7HUVBpgC53PDcTUQuwcgeY6
pQgo9eT1eLNSZVrJ5Bctivl1UcD6P6CIGkkeT2gNhqindRPngUXGXW7Qzoefe+fV
QqJSm7Tq2q9oqVZ46J964waCRItRySpuW5dxZO34WM6wsw2BP2MlACbH4l3luqtp
Xo3Bvfnk+HAFH3HcMuwdaulxv7zYKXCfNoSfgrpEfo2Ex4Im/I3WdtwME/Gbnwdq
3VJzgAxLVFhczDHwNkjmIdPAlNJ9/ixRjip4dgZtW8VcBCrNoL+LhDrIfjvnLdRu
vBHy9P3sCF7FZycaHlMWP6RiLtHnEMGcbZ8QpQHi2dReU1wyr9QgguGU+jqSXYar
1yEcsdRGasppNIZ8+Qawbm/a4doT10TEtPArhSoHlwbvqTDYjtfV92lC/2iwgO6g
YgG9XrO4V8dV39Ffm7oLFfvTbg5mv4Q/E6AWo/gkjmtxkculbyAvjFtYAQARAQAB
tCFFUEVMICg2KSA8ZXBlbEBmZWRvcmFwcm9qZWN0Lm9yZz6JAjYEEwECACAFAkvS
KUICGw8GCwkIBwMCBBUCCAMEFgIDAQIeAQIXgAAKCRA7Sd8qBgi4lR/GD/wLGPv9
qO39eyb9NlrwfKdUEo1tHxKdrhNz+XYrO4yVDTBZRPSuvL2yaoeSIhQOKhNPfEgT
9mdsbsgcfmoHxmGVcn+lbheWsSvcgrXuz0gLt8TGGKGGROAoLXpuUsb1HNtKEOwP
Q4z1uQ2nOz5hLRyDOV0I2LwYV8BjGIjBKUMFEUxFTsL7XOZkrAg/WbTH2PW3hrfS
WtcRA7EYonI3B80d39ffws7SmyKbS5PmZjqOPuTvV2F0tMhKIhncBwoojWZPExft
HpKhzKVh8fdDO/3P1y1Fk3Cin8UbCO9MWMFNR27fVzCANlEPljsHA+3Ez4F7uboF
p0OOEov4Yyi4BEbgqZnthTG4ub9nyiupIZ3ckPHr3nVcDUGcL6lQD/nkmNVIeLYP
x1uHPOSlWfuojAYgzRH6LL7Idg4FHHBA0to7FW8dQXFIOyNiJFAOT2j8P5+tVdq8
wB0PDSH8yRpn4HdJ9RYquau4OkjluxOWf0uRaS//SUcCZh+1/KBEOmcvBHYRZA5J
l/nakCgxGb2paQOzqqpOcHKvlyLuzO5uybMXaipLExTGJXBlXrbbASfXa/yGYSAG
iVrGz9CE6676dMlm8F+s3XXE13QZrXmjloc6jwOljnfAkjTGXjiB7OULESed96MR
XtfLk0W5Ab9pd7tKDR6QHI7rgHXfCopRnZ2VVQ==
=V/6I
-----END PGP PUBLIC KEY BLOCK-----
EOF
  ;;&
  centos7*|rhel7*)
    # Add yum tags
    echo "7" > /etc/yum/vars/os_release_major
    echo "7Server" > /etc/yum/vars/releasever
    # Add the key for EPEL
    cat > /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 << "EOF"
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.4.11 (GNU/Linux)

mQINBFKuaIQBEAC1UphXwMqCAarPUH/ZsOFslabeTVO2pDk5YnO96f+rgZB7xArB
OSeQk7B90iqSJ85/c72OAn4OXYvT63gfCeXpJs5M7emXkPsNQWWSju99lW+AqSNm
jYWhmRlLRGl0OO7gIwj776dIXvcMNFlzSPj00N2xAqjMbjlnV2n2abAE5gq6VpqP
vFXVyfrVa/ualogDVmf6h2t4Rdpifq8qTHsHFU3xpCz+T6/dGWKGQ42ZQfTaLnDM
jToAsmY0AyevkIbX6iZVtzGvanYpPcWW4X0RDPcpqfFNZk643xI4lsZ+Y2Er9Yu5
S/8x0ly+tmmIokaE0wwbdUu740YTZjCesroYWiRg5zuQ2xfKxJoV5E+Eh+tYwGDJ
n6HfWhRgnudRRwvuJ45ztYVtKulKw8QQpd2STWrcQQDJaRWmnMooX/PATTjCBExB
9dkz38Druvk7IkHMtsIqlkAOQMdsX1d3Tov6BE2XDjIG0zFxLduJGbVwc/6rIc95
T055j36Ez0HrjxdpTGOOHxRqMK5m9flFbaxxtDnS7w77WqzW7HjFrD0VeTx2vnjj
GqchHEQpfDpFOzb8LTFhgYidyRNUflQY35WLOzLNV+pV3eQ3Jg11UFwelSNLqfQf
uFRGc+zcwkNjHh5yPvm9odR1BIfqJ6sKGPGbtPNXo7ERMRypWyRz0zi0twARAQAB
tChGZWRvcmEgRVBFTCAoNykgPGVwZWxAZmVkb3JhcHJvamVjdC5vcmc+iQI4BBMB
AgAiBQJSrmiEAhsPBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAKCRBqL66iNSxk
5cfGD/4spqpsTjtDM7qpytKLHKruZtvuWiqt5RfvT9ww9GUUFMZ4ZZGX4nUXg49q
ixDLayWR8ddG/s5kyOi3C0uX/6inzaYyRg+Bh70brqKUK14F1BrrPi29eaKfG+Gu
MFtXdBG2a7OtPmw3yuKmq9Epv6B0mP6E5KSdvSRSqJWtGcA6wRS/wDzXJENHp5re
9Ism3CYydpy0GLRA5wo4fPB5uLdUhLEUDvh2KK//fMjja3o0L+SNz8N0aDZyn5Ax
CU9RB3EHcTecFgoy5umRj99BZrebR1NO+4gBrivIfdvD4fJNfNBHXwhSH9ACGCNv
HnXVjHQF9iHWApKkRIeh8Fr2n5dtfJEF7SEX8GbX7FbsWo29kXMrVgNqHNyDnfAB
VoPubgQdtJZJkVZAkaHrMu8AytwT62Q4eNqmJI1aWbZQNI5jWYqc6RKuCK6/F99q
thFT9gJO17+yRuL6Uv2/vgzVR1RGdwVLKwlUjGPAjYflpCQwWMAASxiv9uPyYPHc
ErSrbRG0wjIfAR3vus1OSOx3xZHZpXFfmQTsDP7zVROLzV98R3JwFAxJ4/xqeON4
vCPFU6OsT3lWQ8w7il5ohY95wmujfr6lk89kEzJdOTzcn7DBbUru33CQMGKZ3Evt
RjsC7FDbL017qxS+ZVA/HGkyfiu4cpgV8VUnbql5eAZ+1Ll6Dw==
=hdPa
-----END PGP PUBLIC KEY BLOCK-----
EOF
  ;;&
  centos*) cat >> /etc/yum.conf << "EOF"
[centos-os]
name = 'CentOS os (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-os
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-$os_release_major
proxy = _none_
priority = 1
enabled = 1

[centos-updates]
name = 'CentOS updates (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-updates
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-$os_release_major
proxy = _none_
priority = 1
enabled = 1

[centos-debuginfo]
name = 'CentOS Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-$os_release_major
proxy = _none_
priority = 1
enabled = 0

[centos-fasttrack]
name = 'CentOS FastTrack (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-fasttrack
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-$os_release_major
proxy = _none_
priority = 1
enabled = 0

[centos-extras]
name = 'CentOS Extras (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-extras
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
proxy = _none_
priority = 1
enabled = 0

[centos-sclo-sclo]
name = 'CentOS SCLo sclo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-sclo-sclo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
proxy = _none_
priority = 1
enabled = 0

[centos-sclo-rh]
name = 'CentOS SCLo rh (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/centos-sclo-rh
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo
proxy = _none_
priority = 1
enabled = 0

EOF
    # Add the key for SCL
    cat > /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-SCLo << "EOF"
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)

mQENBFYM/AoBCADR9Q5cb+H5ndx+QkzNBQ88wcD+g112yvnHNlSiBMOnNEGHuKPJ
tujZ+eWXP3K6ucJckT91WxfQ2fxPr9jQ0xpZytcHcZdTfn3vKL9+OwR0npp+qmcz
rK8/EzVz/SWSgBQ5xT/HUvaeoVAbzBHSng0r2njnBAqABKAoTxgyRGKSCWduKD32
7PF2ZpqeDFFhd99Ykt6ar8SlV8ToqH6F7An0ILeejINVbHUxd6+wsbpcOwQ4mGAa
/CPXeqqLGj62ASBv36xQr34hlN/9zQMViaKkacl8zkuvwhuHf4b4VlGVCe6VILpQ
8ytKMV/lcg7YpMfRq4KVWBjCwkvk6zg6KxaHABEBAAG0aENlbnRPUyBTb2Z0d2Fy
ZUNvbGxlY3Rpb25zIFNJRyAoaHR0cHM6Ly93aWtpLmNlbnRvcy5vcmcvU3BlY2lh
bEludGVyZXN0R3JvdXAvU0NMbykgPHNlY3VyaXR5QGNlbnRvcy5vcmc+iQE5BBMB
AgAjBQJWDPwKAhsDBwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AACgkQTrhOcfLu
nVXNewgAg7RVclomjTY4w80XiztUuUaFlCHyR76KazdaGfx/8XckWH2GdQtwii+3
Tg7+PT2H0Xyuj1aod+jVTPXTPVUr+rEHAjuNDY+xyAJrNljoOHiz111zs9pk7PLX
CPwKWQLnmrcKIi8v/51L79FFsUMvhClTBdLUQ51lkCwbcXQi+bOhPvZTVbRhjoB/
a9z0d8t65X16zEzE7fBhnVoj4xye/MPMbTH41Mv+FWVciBTuAepOLmgJ9oxODliv
rgZa28IEWkvHQ8m9GLJ0y9mI6olh0cGFybnd5y4Ss1cMttlRGR4qthLhN2gHZpO9
2y4WgkeVXCj1BK1fzVrDMLPbuNNCZQ==
=UtPD
-----END PGP PUBLIC KEY BLOCK-----
EOF
  ;;&
  rhel6*) cat >> /etc/yum.conf << "EOF"
[updates]
name = 'Red Hat Enterprise Linux Server (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/updates
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[debuginfo]
name = 'Red Hat Enterprise Linux Server Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[fastrack]
name = 'Red Hat Enterprise Linux Server Optional FasTrack Software (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/fastrack
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[fastrack-debuginfo]
name = 'Red Hat Enterprise Linux Server Optional FasTrack Software Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/fastrack-debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[optional]
name = 'Red Hat Enterprise Linux Server Optional Software (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/optional
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[optional-debuginfo]
name = 'Red Hat Enterprise Linux Server Optional Software Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/optional-debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[optional-fastrack]
name = 'Red Hat Enterprise Linux Server Optional FasTrack Software (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/optional-fastrack
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[optional-fastrack-debuginfo]
name = 'Red Hat Enterprise Linux Server Optional FasTrack Software Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/optional-fastrack-debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[supplementary]
name = 'Red Hat Enterprise Linux Server Supplementary Software (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/supplementary
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[supplementary-debuginfo]
name = 'Red Hat Enterprise Linux Server Supplementary Software Debuginfo (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/supplementary-debuginfo
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhn-tools]
name = 'Red Hat Network Tools for RHEL Server (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhn-tools
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[thirdparty-oracle-java]
name = 'Oracle Repository for Java 1.6, 1.7 and 1.8 (v. $os_release_major for $basearch)'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/thirdparty-oracle-java
gpgcheck = 0
proxy = _none_
priority = 1
enabled = 1

EOF
  ;;&
  rhel7*) cat >> /etc/yum.conf << "EOF"
[rhel-7-server-rpms]
name = 'rhel-7-server-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-debug-rpms]
name = 'rhel-7-server-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-extras-rpms]
name = 'rhel-7-server-extras-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-extras-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-extras-debug-rpms]
name = 'rhel-7-server-extras-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-extras-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-fastrack-rpms]
name = 'rhel-7-server-fastrack-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-fastrack-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-fastrack-debug-rpms]
name = 'rhel-7-server-fastrack-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-fastrack-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-optional-rpms]
name = 'rhel-7-server-optional-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-optional-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-optional-debug-rpms]
name = 'rhel-7-server-optional-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-optional-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-optional-fastrack-rpms]
name = 'rhel-7-server-optional-fastrack-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-optional-fastrack-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-optional-fastrack-debug-rpms]
name = 'rhel-7-server-optional-fastrack-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-optional-fastrack-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-rh-common-rpms]
name = 'rhel-7-server-rh-common-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-rh-common-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-rh-common-debug-rpms]
name = 'rhel-7-server-rh-common-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-rh-common-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-rhn-tools-rpms]
name = 'rhel-7-server-rhn-tools-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-rhn-tools-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-rhn-tools-debug-rpms]
name = 'rhel-7-server-rhn-tools-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-rhn-tools-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

[rhel-7-server-supplementary-rpms]
name = 'rhel-7-server-supplementary-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-supplementary-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 1

[rhel-7-server-supplementary-debug-rpms]
name = 'rhel-7-server-supplementary-debug-rpms'
baseurl = http://$yum_server/yum/tags/$tag_name/$releasever-$basearch/rhel-7-server-supplementary-debug-rpms
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
proxy = _none_
priority = 1
enabled = 0

EOF
  ;;&
esac

# On CentOS, disable default repos, in hopes that they will not be re-enabled by update/reinstall of centos-release
if grep --quiet --ignore-case "centos" /etc/system-release; then
  echo "Disabling CentOS default external yum repos"
  yum-config-manager --disable base updates extras
fi


## Update
yum -y update


