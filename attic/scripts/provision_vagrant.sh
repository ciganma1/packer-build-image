#!/bin/bash --


### Implement some of the recommendations from https://www.vagrantup.com/docs/boxes/base.html


## Fail immediately upon error
# Worth noting that there are situations where this works imperfectly, see http://mywiki.wooledge.org/BashFAQ/105 and http://fvue.nl/wiki/Bash:_Error_handling
set -e -o pipefail


## Create vagrant user and add sudo
echo "Creating vagrant user and adding to sudoers"
useradd --uid 1000 --gid 100 --no-user-group --home /var/lib/vagrant --password $(openssl passwd -1 vagrant) vagrant
echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers


## Add standard vagrant key
echo "Adding standard vagrant key"
mkdir --mode=700 /var/lib/vagrant/.ssh
chown vagrant:users /var/lib/vagrant/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key' > /var/lib/vagrant/.ssh/authorized_keys
chmod 600 /var/lib/vagrant/.ssh/authorized_keys
chown vagrant:users /var/lib/vagrant/.ssh/authorized_keys


## Add information to motd
echo "Adding info to motd"
#AMI ID:
#AMI date:
#Major changes:
#Full release notes in /etc/nx-core_release_notes
#
cat >> /etc/motd << 'EOF'
This box should meet the pre-CIS security baseline, similar to what is
implemented for on-premise servers. However it is still recommended to
apply Chef nx_core to ensure you have the most up-to-date security
settings. For more information, see
http://web.global.nibr.novartis.net/apps/confluence/display/CD/AMI+Creation+and+Deployment

WARNING: Vagrant boxes are for experimental use only, and
must NOT be used in production or with any real data.

EOF


## Build facts
# We store some basic facts about the build
echo "Adding build facts"
mkdir -p /etc/acs/nxfacts
cat > /etc/acs/nxfacts/build.json << EOF
{
  "cmdline": "$(cat /proc/cmdline)",
  "method": "packer",
  "target": "virtualbox",  
  "time": $(date +%s),
  "version": 3
}
EOF

