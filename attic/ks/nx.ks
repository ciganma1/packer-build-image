# System authorization information
auth --enableshadow --passalgo=sha512
#poweroff
reboot
autostep

# Use CDROM installation media
cdrom

# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard us
# System language
lang en_US.UTF-8
# Don't install X11
text

# Security
auth --enableshadow --enablemd5
firewall --disabled
selinux --permissive

# Network information
network  --bootproto=dhcp --device=eth0 --onboot=on --noipv6 --activate --hostname=localhost
# Root password
rootpw packer


## System timezone
timezone UTC


## Disk configuration
zerombr
clearpart --all
bootloader --location=mbr --driveorder=sda
part /boot --fstype ext4 --size=512 --asprimary --ondisk=sda
part pv.root --size=1 --grow
volgroup vg_root pv.root
logvol /              --fstype ext4 --name=lv_root          --vgname=vg_root --size=10240
logvol /var           --fstype ext4 --name=lv_var           --vgname=vg_root --size=3272
#logvol /var/log       --fstype ext4 --name=lv_var_log       --vgname=vg_root --size=1536
#logvol /var/log/audit --fstype ext4 --name=lv_var_log_audit --vgname=vg_root --size=256
# CIS Level 1 specifies that nosuid and noexec should be set on /tmp
# Not implementing it yet, as it will require a lot of changes by other groups
# Plus, we're not yet sure whether it will be on the subset of CIS requirements which NIBR uses
#logvol /tmp           --fstype ext4 --name=lv_tmp           --vgname=vg_root --size=2048 --fsoptions="nodev,nosuid,noexec"
logvol /tmp           --fstype ext4 --name=lv_tmp           --vgname=vg_root --size=2048
logvol /admhome       --fstype ext4 --name=lv_admhome       --vgname=vg_root --size=1024
logvol swap           --fstype swap --name=lv_swap          --vgname=vg_root --size=2048


## Install base packages
%packages
@core
@base
%end


%post
exec >& /root/nx-kick-post.log

## UID/GID change in RHEL7
# Rationale: the default uid/gid range has changed in rhel7. We keep the old ranges
major_release=$(cat /etc/system-release | sed -r 's/(Red Hat Enterprise Linux Server|CentOS Linux|CentOS) release ([67]).*/\2/')
case "$major_release" in
  7) # RHEL/CentOS 7
     echo 'UID/GID change'

     NEW_UID_MIN=500
     NEW_GID_MIN=500
     NEW_SYS_UID_MAX=499
     NEW_SYS_GID_MAX=499

     # Some system users and groups are created during the installation, when rpms are installed.
     # Those users/groups have the wrong uids/gids. We change them.
     CUR_UID=$NEW_SYS_UID_MAX
     CUR_GID=$NEW_SYS_GID_MAX

     for LINE in $(cat /etc/group|awk -v NEW_SYS_GID_MAX=$NEW_SYS_GID_MAX -F: 'BEGIN { OFS = ":" } ($3>NEW_SYS_GID_MAX) { print $1,$3 }'); do
       GROUPNAME=`echo $LINE|cut -d: -f1`
       OLDGID=`echo $LINE|cut -d: -f2`
       groupmod -g $CUR_GID $GROUPNAME
       find / -path /proc -prune -o -group $OLDGID -exec chown :$CUR_GID {} \;
       CUR_GID=$[CUR_GID-1]
     done

     for LINE in $(cat /etc/passwd|awk -v NEW_SYS_UID_MAX=$NEW_SYS_UID_MAX -F: 'BEGIN { OFS = ":" } ($3>NEW_SYS_UID_MAX) { print $1,$3 }'); do
       USERNAME=`echo $LINE|cut -d: -f1`
       OLDUID=`echo $LINE|cut -d: -f2`
       usermod -u $CUR_UID -g $USERNAME $USERNAME
       find / -path /proc -prune -o -user $OLDUID -exec chown $CUR_UID {} \;
       CUR_UID=$[CUR_UID-1]
     done

     # /etc/login.defs needs to be changed, for the old ranges to apply for future users/groups
     cat > /etc/login.defs << EOF
MAIL_DIR        /var/spool/mail
PASS_MAX_DAYS   99999
PASS_MIN_DAYS   0
PASS_MIN_LEN    5
PASS_WARN_AGE   7
UID_MIN         $NEW_UID_MIN
UID_MAX         60000
SYS_UID_MIN     201
SYS_UID_MAX     $NEW_SYS_UID_MAX
GID_MIN         $NEW_GID_MIN
GID_MAX         60000
SYS_GID_MIN     201
SYS_GID_MAX     $NEW_SYS_GID_MAX
CREATE_HOME     yes
UMASK           077
USERGROUPS_ENAB yes
ENCRYPT_METHOD MD5
MD5_CRYPT_ENAB yes
EOF
  ;;
esac

%end
