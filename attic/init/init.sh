#!/bin/bash --

### Initial build environment setup


# Set up environment
. $(dirname $0)/../env.sh
. $(dirname $0)/env.sh


## Install Packer
  wget --directory-prefix="${TMPDIR:-/tmp}" "${current_packer_installer}"
  unzip -o "${TMPDIR:-/tmp}/$(basename "${current_packer_installer}")" -d $(dirname ${packer_bin})/
  ${packer_bin} --version


## Install Vagrant
  wget --directory-prefix="${TMPDIR:-/tmp}" "${current_vagrant_installer}"
  yum -y install "${TMPDIR:-/tmp}/$(basename "${current_vagrant_installer}")"
  vagrant version


## Install VirtualBox
  yum -y install gcc make kernel-devel
  yum --nogpgcheck -y install "${current_virtualbox_installer}"
  vboxmanage --version
  # Manual step: VM Settings, VM Hardware, CPU, enable Hardware virtualization (https://egustafson.github.io/esxi-nested-virtualbox.html)
  # Not 100% sure whether this is necessary to run it, but regardless performance should be better with this on
  # Need the extpack for RDP access during install
  wget --directory-prefix="${TMPDIR:-/tmp}" "${current_virtualbox_extpack_installer}"
  # Pipe in a "y" to accept the license agreement; necessary since v5.1.20
  echo y | vboxmanage extpack install "${TMPDIR:-/tmp}/$(basename "${current_virtualbox_extpack_installer}")" --replace
  vboxmanage list extpacks


## Install ChefDK
  yum -y install "${current_chefdk_installer}"


## Check out chef-repo-demo for kitchen testing
  # Just do this once and leave it; as pointed out by Matt, "This is to test the build and not the nxcore"
  # Which is also why it doesn't matter whose credentials are used
  # .netrc can be used to avoid the dozens of username and password prompts. Just be sure to delete it afterward.
  yum -y install git
  git clone --recurse-submodules https://snydesc1@bitbucket.org/novartisnibr/chef-repo-demo.git "${build_kitchen_dir}"
  # Run the next bit in a subshell, so we don't keep the altered path
  bash
  export PATH=/opt/chefdk/bin:/opt/chefdk/embedded/bin:$PATH
  knife ssl fetch https://10.147.137.206
  knife ssl fetch https://10.147.137.202
  cat ~/.chef/trusted_certs/*.crt >> /opt/chefdk/embedded/ssl/certs/cacert.pem
  cd "${build_kitchen_dir}"
  git submodule update --remote
  cd nx-core/role_nxcore_example
  berks
  exit


## Install awscli
  yum -y install python-pip
  pip install awscli


## Install jq
  # To deal with all the JSON necessary when communicating with AWS (and Packer)
  yum -y install jq


## Pull in sources
  mkdir -p "${build_iso_dir}"
  # Download ISOs
  wget --directory-prefix="${build_iso_dir}/" --timestamping "http://yum.global.nibr.novartis.net/iso/rhel-server-6.7-x86_64-dvd.iso" "http://yum.global.nibr.novartis.net/iso/rhel-server-7.2-x86_64-dvd.iso" "http://yum.global.nibr.novartis.net/iso/centos-6.5-x86_64.iso" "http://yum.global.nibr.novartis.net/iso/rhel-server-7.1-x86_64-dvd.iso" "http://yum.global.nibr.novartis.net/iso/centos-7.0-x86_64.iso"
  wget --directory-prefix="${build_iso_dir}/" --timestamping "http://mirror.net.cen.ct.gov/centos/7/isos/x86_64/CentOS-7-x86_64-DVD-1511.iso"
  # Create checksum files
  for isofile in ${build_iso_dir}/*.iso; do checksumfile="$(dirname $isofile)/$(basename -s .iso $isofile).md5"; [ ! -e "$checksumfile" ] && md5sum "$isofile" > "$checksumfile"; done
  cat ${build_iso_dir}/*.md5

# Log commands run by sys_nxbuild
cat > /etc/profile.d/acs_log_nxbuild.sh << "EOF"
if [ $(whoami) == 'sys_nxbuild' ] && [ -f /var/log/syslog ]; then
  export HISTTIMEFORMAT="%F %T  "
  unset HISTCONTROL
  declare -r PROMPT_COMMAND='RETRN_VAL=$?; history -a; LAST_COMMAND_DATE=$(history 1 | awk "{print \$2,\$3}");LAST_COMMAND=$(history 1 | awk "{print substr(\$0, index(\$0,\$4))}"); LOGMSG="run-as-nxbuild process=$$,when=$LAST_COMMAND_DATE,what=$LAST_COMMAND"; if [ ! $nxbuild_logging_begun ]; then logger -p local6.debug "run-as-nxbuild process=$$,when=$LAST_COMMAND_DATE,what=SESSION_STARTED"; nxbuild_logging_begun=true; elif [ "$nxbuild_last_log" != "$LOGMSG" ]; then logger -p local6.debug "$LOGMSG,return=$RETRN_VAL"; nxbuild_last_log="$LOGMSG"; fi'
fi
EOF

