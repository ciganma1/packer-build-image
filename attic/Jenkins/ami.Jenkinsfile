 /*

Description:
-------------

Jenkins declarative pipeline uploads AMI image on S3 and run import. After successful import the pipeline generates kitchen skeleton files
runs kitchen create and spins two ec2 instances , converge ( with no cookbooks ), verify ( no inspec profiles runs , not ready yet ) and destroys ec2 instances.
If the build is succesful it sends email notification.

Parameters:
-------------

AMI_IMAGE:

- path to an existing AMI image

AMI_NAME:

- name of AMI image

Todo:
-------------


- add check AWS_SUBNET_ID
    aws ec2 describe-subnets --region us-east-1 | jq ".Subnets[].SubnetId"

- add check AWS_SECURITY_GROUP_ID
    aws ec2 describe-security-groups  --region us-east-1 | jq ".SecurityGroups[].GroupId"


*/

/*/////////////////////////////////////////////////////////////////
   Settings
*//////////////////////////////////////////////////////////////////


DEFAULT_TIMEOUT = 90

// default bitbucket settings
BITBUCKET_CRED_ID = 'BITBUCKET_AUTOMATION_ID'
BITBUCKET_USER_ID = 'nibr_linux_servers'
BITBUCKET_SLUG    = 'novartisnibr'


// default environment settings
env.OS_BUILD_TYPE = 'AMI'
env.NODE = 'packer-bm'
env.CUSTOM_WORKSPACE = '/apps/nx_build'


// default proxy settings
env.HTTP_PROXY="http_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"
env.HTTPS_PROXY="https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011"


// default AWS settings
//env.AWS_SUBNET_ID = 'subnet-0b7e1034'
env.AWS_SUBNET_ID = 'subnet-a21c8fff'
env.AWS_SECURITY_GROUP_ID = 'sg-41c9ac35'
env.AWS_INSTANCE_TYPE = ''
env.AWS_USER = "ec2-user"
env.AWS_REGION = 'us-east-1'
env.AWS_SSH_KEY_ID = 'sys_nxbuild'
env.AMI_IMAGE_ID = ''

// sys_nxbuild credentials
SYS_NXBUILD_SSH_KEY='SYS_NXBUILD_SSH_KEY'
SYS_NXBUILD_SSH_PUB = 'SYS_NXBUILD_SSH_PUB'

// default kitchen role
ROLE_COOKBOOK = 'nxcore_kitchen_run'
ROLE_PROJECT_NAME = 'role_ec2_kitchen_run'

//GIT_NX_CORE_PROJECTS = ["nx_compliance","nxbd_smoketest"]
def GIT_NX_CORE_PROJECTS = ["augeas","nx_concat","nx_core","nx_core_yum","nx_core_ulimits","nx_core_sysctl","nx_core_sudo","nx_core_ssh","nx_core_snmp","nx_core_rsyslog","nx_core_resolv","nx_core_patching","nx_core_ntp","nx_core_nscd","nx_core_network","nx_core_mountfs","nx_core_mail","nx_core_local_accounts","nx_core_kdump","nx_core_fusioninventory","nx_core_data","nx_core_boot","nx_core_backup","nx_core_auth","nx_core_auditd","nx_core_sysctl","nx_core_ulimits","nx_core_yum","nx_compliance"]
//,"nxbd_smoketest"]


// default build info answer
ENV_CONFLUENCE_ANSWER = ["yes","no"]


// fallback email recipient in case of failed build
env.INFO_EMAIL = 'marek.ciganek@novartis.com'

// set if upload to repository is enabled
env.PUBLISH_TO_ALL_ACCOUNTS = 'no'

// turn on/off debugging
def DEBUG = true



/*/////////////////////////////////////////////////////////////////
  Functions
*//////////////////////////////////////////////////////////////////

def send_email_notification(rcp,stat){
    def recipient       = rcp
    def build_status    = stat
    def mail_body       = ""

        mail_body       = """
Build details:\n\n\n
JOB NAME: ${env.JOB_NAME}
BUILD ID: ${env.BUILD_ID}
NODE NAME: ${env.NODE_NAME}
URL: ${env.BUILD_URL}
RUN DISPLAY: ${env.RUN_DISPLAY_URL}
"""
if (  AMI_IMAGE != null &&  AMI_IMAGE != null && AMI_IMAGE_ID != null ){
        mail_body   += """


AMI details:\n\n
AMI ID:    ${env.AMI_IMAGE_ID}
AMI IMAGE: ${env.AMI_IMAGE}
AMI NAME:  ${env.AMI_NAME}
"""
}

    if ( build_status == 'Successful') { mail_body = "${mail_body}" }

        if ( recipient.size() == 0 ) { return }

        mail(to: recipient,
            subject: "${env.JOB_NAME} BUILD: ${env.BUILD_ID} ${build_status} IMAGE: ${env.AMI_NAME}",
            body: mail_body
            )
}


/*/////////////////////////////////////////////////////////////////
  Pipeline section
*//////////////////////////////////////////////////////////////////


pipeline {
    agent {
            node {
                label NODE
                customWorkspace CUSTOM_WORKSPACE
            }
        }
    // we terminate the build after 90 minutes
    options {
        timeout(time: 90, unit: 'MINUTES')
        timestamps()
    }

    parameters {
                string(
                        name: "AMI_IMAGE",
                        description: "AMI path to image e.g. /apps/nx_build/output-centos7-ami/nibr baseos nxc centos 7 x86_64 (nxc) 20181123-1542960424.ova")
                string(
                        name: "AMI_NAME",
                        description: "AMI name e.g. centos7-ami-20181123-1542960424")
    }
    stages {
        /*
            Stage prepare for the build
        */
        stage('Preparation') {
            steps {

                script {

                        // validate input parameter
                        if ( params.AMI_NAME != '' ){
                            AMI_NAME = params.AMI_NAME
                        }else{
                            error("AMI_NAME parameter can't be empty")
                        }

                        // validate input parameter
                        if ( params.AMI_IMAGE != '' ){
                            AMI_IMAGE = params.AMI_IMAGE
                        }else{
                            error("AMI_IMAGE parameter can't be empty")
                        }

                        // Check if image really exists
                        if ( !fileExists(AMI_IMAGE) ){
                            error("Unable to locate: [ ${AMI_IMAGE} ]")
                        }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage upload to S3 and import into EC2
        */

        stage('Run upload and import'){
            when {
                expression { AMI_IMAGE != null }
            }
            steps {
                script{
                    try{

                        dir(".ssh")
                        withCredentials([file(credentialsId: SYS_NXBUILD_SSH_KEY, variable: 'SYS_NXBUILD_SSH_KEY'),(credentialsId: SYS_NXBUILD_SSH_PUB, variable: 'SYS_NXBUILD_SSH_PUB')]) {
                            def output_git_commands = sh(returnStdout: true,script: """
                                set +x
                                mv '${SYS_NXBUILD_SSH_KEY}' id_rsa
                                chmod 400 id_rsa
                                mv '${SYS_NXBUILD_SSH_PUB}' id_rsa.pub
cat > config << EOF_ssh_config
Host *
 AddKeysToAgent yes
 IdentityFile ~/.ssh/id_rsa
EOF_ssh_config
                        
                            """)
                        }
                        withCredentials([file(credentialsId: 'SYS_NXBUILD_TURBOT', variable: 'TURBOT')]) {
                            sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "source ${TURBOT}\n" +
                                        ". ${CUSTOM_WORKSPACE}/env.sh\n" +
                                        "get_aws_credentials\n" +
                                        "upload_aws_images '${AMI_IMAGE}' | tee ${AMI_NAME}-aws-import.log"
                            ])
                        }

                        if ( ! fileExists("${CUSTOM_WORKSPACE}/${AMI_NAME}-aws-import.log") ){
                            error("Unable to upload and import: Unable to find [ ${AMI_NAME}-aws-import.log ]")
                        }else{

                            AMI_IMAGE_ID = sh  ( returnStdout: true, script: "perl -ne 'print \$1 if /Successfully created AMI ${AWS_REGION}:(.*),/' ${AMI_NAME}-aws-import.log" )

                            if ( DEBUG == true ) {  print "DEBUG: AMI IMAGE ID: [${AMI_IMAGE_ID}]" }
                            currentBuild.result = 'SUCCESS'
                        }

                        if (AMI_IMAGE_ID =~ /Can't open/){
                            error("Run upload and import: Unable to find build name")
                        }else{
                            if ( DEBUG == true ) {  print "DEBUG: AMI IMAGE ID: [${AMI_IMAGE_ID}]" }
                            currentBuild.result = 'SUCCESS'
                        }
                    }catch(Exception e){
                       error("Unable to run upload [ ${AMI_IMAGE} ] ]")
                    }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage clones nx_core repos
        */

        stage('Clone repository'){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {

                // clone repo
                    script {
                            def output_git_commands  = ''
                            try {
                                // clone all necessary projects from git repository
                                GIT_NX_CORE_PROJECTS.each { project ->

                                    dir("kitchen/${AMI_NAME}/nx_core" ){

                                            withCredentials([usernamePassword(credentialsId: BITBUCKET_CRED_ID , passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                                                output_git_commands = sh(returnStdout: true,script: """
                                                    if [[ ! -d ${project} ]];then
                                                        git  clone https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${BITBUCKET_SLUG}/${project}.git
                                                    else
                                                        cd  ${project}
                                                        git pull
                                                        cd ..
                                                    fi
                                                """)
                                                }

                                            if ( output_git_commands =~ /Error|ERROR|Failed|Not found|No such file or directory/ ){
                                                error("Packer build failed")
                                            }else{
                                                currentBuild.result = 'SUCCESS'
                                            }
                                    }
                                }

                            }catch(Exception ex){
                                error("ERROR! Unable to clone repositories")
                            }
              } // end script
            } // end steps
        } // end stage


        /*
            Stage prepares kitchen evironment
        */

        stage('Prepare kitchen environment'){

            when {
                allOf {
                        expression { currentBuild.result == 'SUCCESS' }
                        expression { AMI_IMAGE_ID != null  }
                }
            }

            steps {

                script {

                    def sshd_output_command = ''
                    dir("kitchen/${AMI_NAME}") {
                        ssshd_output_command = sh (returnStdout: true, script: """[[ ! -d databags ]] && mkdir databags || exit 0""")
                    }

                    dir("kitchen/${AMI_NAME}/nx_core") {

                        sshd_output_command = sh (returnStdout: true, script: """
                            set -x -v

                            [[ ! -d ${ROLE_PROJECT_NAME} ]] && mkdir  ${ROLE_PROJECT_NAME} || { rm -rf  ${ROLE_PROJECT_NAME} ; mkdir  ${ROLE_PROJECT_NAME} ; }
                            [[ ! -d ${ROLE_PROJECT_NAME}/recipes ]]  && mkdir ${ROLE_PROJECT_NAME}/recipes || exit 0

                            touch ${ROLE_PROJECT_NAME}/README.md

cat > ${ROLE_PROJECT_NAME}/Berksfile << EOF_berksfile
source 'https://10.147.137.206'
source 'https://supermarket.chef.io'
#source 'https://uch-supermarket.prd.nibr.novartis.net'
#source 'https://supermarket.chef.io'

metadata

group :core do
  def dependencies(path)
    full_entry = File.join(File.dirname(__FILE__), path)
    File.directory?(full_entry) and File.exist?(File.join(full_entry, 'metadata.rb'))
  end
  Dir.glob('../*').each do |path|
    dependencies path
    cookbook File.basename(path),
             :path => path
  end
end
EOF_berksfile

######################################################################################################

cat > ${ROLE_PROJECT_NAME}/metadata.rb << EOF_metadata
name             '${ROLE_PROJECT_NAME}'
maintainer       'NX'
maintainer_email 'NX'
license          'All rights reserved'
description      'Installs/Configures ${ROLE_PROJECT_NAME}, a role-cookbook for testing.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'nx_core'
EOF_metadata

######################################################################################################

cat > ${ROLE_PROJECT_NAME}/recipes/default.rb << EOF_recipe
#
# Cookbook Name:: ${ROLE_PROJECT_NAME}
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nx_core'
EOF_recipe

######################################################################################################


cat > ${ROLE_PROJECT_NAME}/.kitchen.yml <<EOF_kitchen
---
driver:
  name: ec2
  region: ${AWS_REGION}
  security_group_ids: ["${AWS_SECURITY_GROUP_ID}"]
  require_chef_omnibus: true
  instance_type: ${AWS_INSTANCE_TYPE}
  associate_public_ip: false
  interface: private
  subnet_id: ${AWS_SUBNET_ID}
  aws_ssh_key_id: ${AWS_SSH_KEY_ID}

transport:
    username: ${AWS_USER}

provisioner:
  name: chef_zero
  client_rb:
     'ohai.disabled_plugins = [:Passwd] #':
  http_proxy:  'https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011'
  https_proxy: 'https_proxy=http://nibr-proxy.global.nibr.novartis.net:2011'

platforms:
  - name: ${AMI_NAME}
    driver:
      image_id: ${AMI_IMAGE_ID}
      region: us-east-1

suites:
  - name: t2-micro
    attributes:
    driver:
        name: ec2
        region: ${AWS_REGION}
        security_group_ids: ["${AWS_SECURITY_GROUP_ID}"]
        require_chef_omnibus: true
        instance_type:  t2.micro
        associate_public_ip: false
        interface: private
        subnet_id: ${AWS_SUBNET_ID}
        aws_ssh_key_id: ${AWS_SSH_KEY_ID}

  - name: t2-medium
    attributes:
    driver:
        name: ec2
        region: ${AWS_REGION}
        security_group_ids: ["${AWS_SECURITY_GROUP_ID}"]
        require_chef_omnibus: true
        instance_type: t2.medium
        associate_public_ip: false
        interface: private
        subnet_id: ${AWS_SUBNET_ID}
#        aws_ssh_key_id: ${AWS_SSH_KEY_ID}

  - name: m5-large
    attributes:
    driver:
        name: ec2
        region: ${AWS_REGION}
        security_group_ids: ["${AWS_SECURITY_GROUP_ID}"]
        require_chef_omnibus: true
        instance_type: m5.large
        associate_public_ip: false
        interface: private
        subnet_id: ${AWS_SUBNET_ID}
        aws_ssh_key_id: ${AWS_SSH_KEY_ID}


#verifier:
#  name: inspec
#  inspec_tests:
#        - tests/NIBR-custom
#        - tests/nx-ami-smoketest
#       - tests/nx_compliance
EOF_kitchen

######################################################################################################

cat > ${ROLE_PROJECT_NAME}/nx_metadata.json <<EOF_json
{
  "update": {
    "approvers": [],
    "notifications": {
      "email": true,
      "email_addresses": [],
      "slack": true,
      "slack_channel": ""
    }
  }
}
EOF_json

[[ -d  nx_compliance ]] && mv nx_compliance   ${ROLE_PROJECT_NAME}/tests
#[[ -d  nxbd_smoketest ]] && mv nxbd_smoketest  ${ROLE_PROJECT_NAME}/tests
""")
                        if ( sshd_output_command =~ /Error|ERROR|Failed|Not found|No such file or directory/ ){
                            error("Packer build failed")
                        }else{
                            currentBuild.result = 'SUCCESS'
                        }

                    } //end  dir
                } // end script
            } // end steps
        } // end stage

        /*
            Stage runs Kitchen to spinup ec2 instance
        */

        stage('Kitchen create'){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {
                // clone repo
                script {

                    try {
                            dir("kitchen/${AMI_NAME}/nx_core") {
                                withCredentials([file(credentialsId: 'SYS_NXBUILD_TURBOT', variable: 'TURBOT')]) {
                                    sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "source ${TURBOT}\n" +
                                        ". ${CUSTOM_WORKSPACE}/env.sh\n" +
                                        "get_aws_credentials\n" +
                                        "cd ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}/nx_core/${ROLE_PROJECT_NAME}\n" +
                                        "/usr/bin/kitchen create --no-color"
                                    ])
                                }
                            } //end  dir

                            currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                       error("ERROR! Failed stage [ Kitchen create ]")
                    }
                } // end script
            } // end steps
        } // end stage

        /*
            Stage runs Kitchen convegre
        */

        stage('Kitchen converge '){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {
                    // clone repo
                script {

                    try {

                        dir("kitchen/${AMI_NAME}/nx_core") {
                            withCredentials([file(credentialsId: 'SYS_NXBUILD_TURBOT', variable: 'TURBOT')]) {
                                sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "source ${TURBOT}\n" +
                                        ". ${CUSTOM_WORKSPACE}/env.sh\n" +
                                        "get_aws_credentials\n" +
                                        "cd ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}/nx_core/${ROLE_PROJECT_NAME}\n" +
                                        "/usr/bin/kitchen converge --no-color"
                                ])
                            }
                        }

                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen converge ]")
                    }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage runs Kitchen verify to run inspec profiles with security baseling
        */

        stage('Kitchen verify'){

            when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {
                // clone repo
                script {

                    try {

                        dir("kitchen/${AMI_NAME}/nx_core") {

                            sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "cd ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}/nx_core/${ROLE_PROJECT_NAME}\n" +
                                        "/usr/bin/kitchen verify --no-color"
                            ])
                        }
                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen converge ]")
                    }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage removes VMs created by Kitchen create step
        */

        stage('Kitchen destroy'){

            when {
                    expression { currentBuild.result == 'SUCCESS' ||  currentBuild.result == 'FAILED ' }
            }

            steps {
                // clone repo
                script {

                    try {

                        dir("kitchen/${AMI_NAME}/nx_core") {
                            withCredentials([file(credentialsId: 'SYS_NXBUILD_TURBOT', variable: 'TURBOT')]) {
                                sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "source ${TURBOT}\n" +
                                        ". ${CUSTOM_WORKSPACE}/env.sh\n" +
                                        "get_aws_credentials\n" +
                                        "cd ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}/nx_core/${ROLE_PROJECT_NAME}\n" +
                                        "/usr/bin/kitchen destroy --no-color"
                                ])
                            }
                        }
                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen converge ]")
                    }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage uploads original image to the repository
        */

        stage('Publish to all accounts'){
            when {
                    expression { PUBLISH_TO_ALL_ACCOUNTS == 'yes'}
            }
            steps {

                script {
                   try{
                       sh (returnStdout: true, script: """echo UPLOAD""")

                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen create ]")
                    }

                } // end script
            } // end steps
        } // end stage

        /*
            Stage generates and publishes release notes on Confluence
        */

        stage('Publish release notes'){

         when {
                    expression { currentBuild.result == 'SUCCESS' }
            }

            steps {
                    // clone repo
                script {
                    def PUBLISH_RELEASE_NOTES = 'no'
                    def sshd_output_command  = ''

                    try {
                        // We setup default timeout prompt for user input we run with preset variables otherwise
                        timeout(time: 180, unit: 'SECONDS'){

                        //user can choose on input which build combination to run
                         def userInput = input message: 'Do you want to publish release notes?', submitterParameter: 'submit_by', ok: 'Continue',
                            parameters: [
                                            choice(choices:ENV_CONFLUENCE_ANSWER.join('\n') , description: 'Do you want to publish Release Notes to confluence?', name: 'publish_release_notes')
                                        ]

                            PUBLISH_RELEASE_NOTES = userInput['publish_release_notes']

                                  // try to publish release information on Confluence
                                if ( PUBLISH_RELEASE_NOTES == 'yes' ){

                                    if ( fileExists("publish_release.sh") ){
                                        sshd_output_command =  sh (returnStdout: true, script: "./publish_release.sh -n ${AMI_NAME} -C 125118621")
                                    }else{
                                        error("ERROR: Unable to find [ ${env.CUSTOM_WORKSPACE}/publish_release.sh ]")
                                    }
                                }

                                print "DEBUG: ${sshd_output_command}"
                            }

                        }catch(Exception e){
                                //Do nothing
                                //error("${e}")
                        }finally{

                                // Default parameters to run the pipeline with
                                if ( PUBLISH_RELEASE_NOTES == null  ) { PUBLISH_RELEASE_NOTES = 'no' }

                                //
                                //
                                // we record the name of the Jenkins user who submits this request, and the time
                                SUBMIT_BY = 'Autorun'
                                SUBMIT_TIME = new Date().toString()

                        } // end finally
                } // end script
            } // end steps
        } // end stage

        /*
            Stage generates and publishes release notes on Confluence
        */

        stage('Cleanup'){

          //  when {
          //          expression { currentBuild.result == 'SUCCESS' }
          //  }

            steps {
                    
                script {
                    try{
                        //remove kitchen
                        sh (returnStdout: false, script:
                                    "#!/bin/bash\n" +
                                    "rm -rf ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}*"
                            )
                        
                        if(fileExists(".ssh/id_rsa")){
                          println "INFO: Removing .ssh directory"
                          sh (returnStdout: false, script:
                            "#!/bin/bash\n" +
                            "rm -rf .ssh"
                          )
                        }

                        currentBuild.result = 'SUCCESS'

                    }catch(Exception ex){
                        error("ERROR! Failed stage [ Kitchen create ]")
                    }
              } // end script
            } // end steps
        } // end stage

    } //end Stages

    /*
      Post actions
    */

     post('Post actions') {

        //Disabled
        /*   always {
            deleteDir()
        }*/

        success{
            send_email_notification(INFO_EMAIL,'Successful')
        }

        unstable{
           send_email_notification(INFO_EMAIL,'Unstable')
        }

        // we destroy kitchen
        failure {

            send_email_notification(INFO_EMAIL,'Failed')

            script{
                try{
                    dir("kitchen/${AMI_NAME}/nx_core") {
                        if ( fileExists("${ROLE_PROJECT_NAME}/.kitchen.yml")){
                            withCredentials([file(credentialsId: 'SYS_NXBUILD_TURBOT', variable: 'TURBOT')]) {
                                sh ([returnStdout: false, script:
                                        "#!/bin/bash\n" +
                                        "source ${TURBOT}\n" +
                                        ". ${CUSTOM_WORKSPACE}/env.sh\n" +
                                        "get_aws_credentials\n" +
                                        "cd ${CUSTOM_WORKSPACE}/kitchen/${AMI_NAME}/nx_core/${ROLE_PROJECT_NAME}\n" +
                                        "/usr/bin/kitchen destroy --no-color"
                                ])
                            }
                        }
                    }

                    dir("kitchen"){
                        sh (returnStdout: false, script:
                            '#!/bin/bash\n' //+
                            //'rm -rf ${AMI_NAME}'
                        )
                    }

                    if(fileExists(".ssh/id_rsa")){
                          println "INFO: Removing .ssh directory"
                          sh (returnStdout: false, script:
                            "#!/bin/bash\n" +
                            "rm -rf .ssh"
                          )
                    }
                }catch(Exception ex){
                            error("ERROR! Failed post action [ Kitchen destroy ]")
                }
            }
        }
    }

} //end  pipeline