#!/usr/bin/env bash
set -x -v
PACKER_LOG=1 /usr/local/bin/packer build  -color=false -only=$1  -force <(jq -n -f json/nx-builds-commented-mnc-proto1.json) > $2
